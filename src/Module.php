<?php

namespace dvizh\order;

use dvizh\order\behaviors\ShippingCost;
use yii;

/**
 * Class Module
 * @package dvizh\order
 */
class Module extends \yii\base\Module
{
    /**
     *
     */
    const EVENT_ORDER_CREATE = 'create';
    /**
     *
     */
    const EVENT_ORDER_DELETE = 'delete';
    /**
     *
     */
    const EVENT_ELEMENT_DELETE = 'delete_element';
    /**
     *
     */
    const EVENT_ORDER_UPDATE_STATUS = 'update_status';

    /**
     * @var string
     */
    public $countryCode = 'RU';

    /**
     * @var string[]
     */
    public $adminRoles = ['admin', 'superadmin'];
    /**
     * @var string[]
     */
    public $operatorRoles = ['manager', 'admin', 'superadmin'];
    /**
     * @var string
     */
    public $operatorOpenStatus = 'process';

    /**
     * @var string[]
     */
    public $orderStatuses = ['new' => 'Новый', 'approve' => 'Подтвержден', 'cancel' => 'Отменен', 'process' => 'В обработке', 'done' => 'Выполнен'];
    /**
     * @var string
     */
    public $defaultStatus = 'new';

    /**
     * @var string
     */
    public $successUrl = '/order/info/thanks/';
    /**
     * @var string
     */
    public $orderCreateRedirect = 'order/view';

    /**
     * @var string
     */
    public $robotEmail = "no-reply@localhost";
    /**
     * @var string
     */
    public $dateFormat = 'd.m.Y H:i:s';
    /**
     * @var string
     */
    public $robotName = 'Robot';
    /**
     * @var bool
     */
    public $adminNotificationEmail = false;
    /**
     * @var bool
     */
    public $clientEmailNotification = true;

    /**
     * @var string
     */
    public $currency = ' р.';
    /**
     * @var string
     */
    public $currencyPosition = 'after';
    /**
     * @var array
     */
    public $priceFormat = [2, '.', ''];

    /**
     * @var string[]
     */
    public $cartCustomFields = ['Остаток' => 'amount'];

    /**
     * @var bool
     */
    public $paymentFormAction = false;
    /**
     * @var bool
     */
    public $paymentFreeTypeIds = false;

    /**
     * @var string
     */
    public $superadminRole = 'superadmin';

    /**
     * @var bool
     */
    public $createOrderUrl = false;

    /**
     * @var string
     */
    public $userModel = '\dvizh\client\models\Client';
    /**
     * @var string
     */
    public $userSearchModel = '\dvizh\client\models\client\ClientSearch';

    /**
     * @var array
     */
    public $userModelCustomFields = [];

    /**
     * @var string
     */
    public $productModel = 'dvizh\shop\models\Product';
    /**
     * @var string
     */
    public $productSearchModel = 'dvizh\shop\models\product\ProductSearch';
    /**
     * @var null
     */
    public $productCategories = null;

    /**
     * @var string[]
     */
    public $orderColumns = ['client_name', 'phone', 'email', 'payment_type_id', 'shipping_type_id'];

    /**
     * @var array
     */
    public $elementModels = []; //depricated

    /**
     * @var null
     */
    public $sellers = null; //collable, return seller list

    /**
     * @var string
     */
    public $sellerModel = '\common\models\User';

    /**
     * @var array
     */
    public $workers = [];

    /**
     * @var bool
     */
    public $elementToOrderUrl = false;

    /**
     * @var bool
     */
    public $showPaymentColumn = false;
    /**
     * @var bool
     */
    public $showCountColumn = true;

    /**
     * @var
     */
    private $mail;

    /**
     * @var string
     */
    public $discountDescriptionCallback = '';

    /**
     * @var null
     */
    public $searchByElementNameArray = null;

    /**
     * @return mixed
     */
    public function init()
    {
        if (yii::$app->has('cart') && $orderShippingType = yii::$app->session->get('orderShippingType')) {
            if ($orderShippingType > 0) {
                yii::$app->cart->attachBehavior('ShippingCost', new ShippingCost);
            }
        }

        return parent::init();
    }

    /**
     * @return mixed
     */
    public function getMail()
    {
        if ($this->mail === null) {
            $this->mail = yii::$app->getMailer();
            $this->mail->viewPath = __DIR__ . '/mails';

            if ($this->robotEmail !== null) {
                $this->mail->messageConfig['from'] = $this->robotName === null ? $this->robotEmail : [$this->robotEmail => $this->robotName];
            }
        }

        return $this->mail;
    }

    /**
     * @return array
     */
    public function getWorkersList()
    {
        if (is_callable($this->workers)) {
            $values = $this->workers;

            return $values();
        } else {
            return $this->workers;
        }

        return [];
    }

    /**
     * @return array
     */
    public function getProductCategoriesList()
    {
        if (is_callable($this->productCategories)) {
            $values = $this->productCategories;

            return $values();
        }

        return [];
    }

    /**
     * @return array
     */
    public function getSellerList()
    {
        if (is_callable($this->sellers)) {
            $values = $this->sellers;

            return $values();
        }

        return [];
    }
}
