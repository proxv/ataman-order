<?php

namespace dvizh\order\assets;

use yii\web\AssetBundle;

/**
 * Class OneClickAsset
 * @package dvizh\order\assets
 */
class OneClickAsset extends AssetBundle
{
    /**
     * @var string[]
     */
    public $depends = [
        'dvizh\order\assets\Asset'
    ];

    /**
     * @var string[]
     */
    public $js = [
        'js/oneclick.js',
    ];

    /**
     * @var string[]
     */
    public $css = [
        'css/oneclick.css',
    ];

    /**
     * @inheritdoc}
     */
    public function init()
    {
        $this->sourcePath = __DIR__ . '/../web';
        parent::init();
    }

}
