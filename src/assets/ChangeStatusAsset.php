<?php

namespace dvizh\order\assets;

use yii\web\AssetBundle;

/**
 * Class ChangeStatusAsset
 * @package dvizh\order\assets
 */
class ChangeStatusAsset extends AssetBundle
{
    /**
     * @var string[]
     */
    public $depends = [
        'dvizh\order\assets\Asset'
    ];

    /**
     * @var string[]
     */
    public $js = [
        'js/changestatus.js',
    ];

    /**
     * @var string[]
     */
    public $css = [

    ];

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->sourcePath = __DIR__ . '/../web';
        parent::init();
    }

}
