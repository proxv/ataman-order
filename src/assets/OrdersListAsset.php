<?php

namespace dvizh\order\assets;

use yii\web\AssetBundle;

/**
 * Class OrdersListAsset
 * @package dvizh\order\assets
 */
class OrdersListAsset extends AssetBundle
{
    /**
     * @var string[]
     */
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapPluginAsset',
    ];

    /**
     * @var string[]
     */
    public $js = [
        'js/orders-list.js',
    ];

    /**
     * @var string[]
     */
    public $css = [
        'css/orders-list.css',
    ];

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->sourcePath = dirname(__DIR__) . '/web';
        parent::init();
    }
}
