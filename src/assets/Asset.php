<?php

namespace dvizh\order\assets;

use yii\web\AssetBundle;

/**
 * Class Asset
 * @package dvizh\order\assets
 *
 * @inheritdoc
 *
 */
class Asset extends AssetBundle
{
    /**
     * @inheritdoc
     */
    public $depends = [
        'yii\web\JqueryAsset',
//        'yii\bootstrap\BootstrapPluginAsset',
    ];

    /**
     * @inheritdoc
     */
    public $js = [
        //'js/scripts.js',
    ];

    /**
     * @inheritdoc
     */
    public $css = [
        'css/styles.css',
    ];

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->sourcePath = dirname(__DIR__) . '/web';
        parent::init();
    }
}
