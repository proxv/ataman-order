<?php

namespace dvizh\order\assets;

use yii\web\AssetBundle;

/**
 * Class AssigmentToOrder
 * @package dvizh\order\assets
 */
class AssigmentToOrder extends AssetBundle
{
    /**
     * @var string[]
     */
    public $depends = [
        'dvizh\order\assets\Asset'
    ];

    /**
     * @var string[]
     */
    public $js = [
        'js/assigment-to-order.js',
    ];

    /**
     * @var string[]
     */
    public $css = [

    ];

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->sourcePath = __DIR__ . '/../web';
        parent::init();
    }

}
