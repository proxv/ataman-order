<?php

namespace dvizh\order\assets;

use yii\web\AssetBundle;

/**
 * Class OrderFormAsset
 * @package dvizh\order\assets
 */
class OrderFormAsset extends AssetBundle
{
    /**
     * @var string[]
     */
    public $depends = [
        'dvizh\order\assets\Asset'
    ];
    /**
     * @var string[]
     */
    public $js = [
        'js/order-form.js',
    ];

    /**
     * {@inheritdoc}
     */
    public function init()
    {
        $this->sourcePath = __DIR__ . '/../web';
        parent::init();
    }
}