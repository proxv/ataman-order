<?php
return [
    'Order' => 'Чек',
    'ID' => 'ID',
    'Name' => 'Назва',
    'Total cost' => 'Загальна вартість',
    'In total' => 'Всього',
    'Count' => 'Кількість',
    'Cnt' => 'Кіл-ть',
    'Client name' => 'ПІБ',
    'Delivery' => 'Доставка',
    'Comment' => 'Коментар',
    'Phone' => 'Телефон',
    'Date' => 'Дата',
    'Time' => 'Час',
    'Sort' => 'Сортування',
    'Email' => 'E-mail',
    'Pay type' => 'Спосіб оплати',
    'Paid' => 'Оплачено',
    'Status' => 'Статус',
    'Order info' => 'Інформація про замовлення',
    'User ID' => 'ID користувача',
    'Price' => 'Ціна',
    'Base price' => 'Вихідна ціна',
    'Description' => 'Опис',
    'Model name' => 'Назва моделі',
    'Order ID' => 'Замовлення',
    'Item ID' => 'Елемент',
    'Update' => 'Редагувати',
    'Create' => 'Створити',
    'Delete' => 'Видалити',
    'Realy?' => 'Дійсно?',
    'Orders' => 'Надходження та чеки',
    'Promocode' => 'Промокод',
    'Create order' => 'Створити замовлення',
    'Update order' => 'Редагувати замовлення',
    'yes' => 'Так',
    'no' => 'Ні',
    'Unknow product' => 'Невідомий товар',
    'Order list' => 'Складова чеку',
    'Field' => 'Поле',
    'Fields' => 'Поля',
    'Create field' => 'Додати поле',
    'Update field' => 'Редагувати поле',
    'Payment type' => 'Тип оплаты',
    'Payment types' => 'Типи оплати',
    'Create payment type' => 'Додати вид оплати',
    'Update payment type' => 'Редагувати види оплати',
    'Create shipping type' => 'Додати тип доставки',
    'Update shipping type' => 'Редагувати тип доставки',
    'Shipping type' => 'Тип доставки',
    'Shipping types' => 'Типи доставки',
    'Product' => 'Продукт',
    'Type' => 'Тип',
    'Widget' => 'Віджет',
    'Value' => 'Значення',
    'Required' => 'Обов\'язково',
    'Error (check required fields)' => 'Помилка (перевірте правильність обов\'язкових полів)',
    'Continue shopping' => 'Продовжити покупки',
    'New order' => 'Нове замовлення',
    'User Id' => 'Користувач',
    'Look' => 'Дивитися',
    'View' => 'Дивитися',
    'Variants' => 'Ааріанти',
    'New variant' => 'новий варіант',
    'Choose' => 'Выбір',
    'Users' => 'Користувачі',
    'Products' => 'Товари',
    'To order' => 'В замовлення',
    'Close' => 'Закрити',
    'Code' => 'Артикул',
    'Amount' => 'Сума',
    'Not found' => 'Не знайдено',
    'Search' => 'Пошук',
    'Seller' => 'Продавець',
    'Cost' => 'Вартість',
    'Base cost' => 'Початкова ціна',
    'Buyer' => 'Покупець',
    'Today' => 'Сьогодні',
    'By month' => 'За місяць',
    'In month' => 'З 1 числа',
    'Average check' => 'Середній Чек',
    'Turnover' => 'Оборот',
    'Buyers' => 'Покупців',
    'Profit' => 'Прибуток',

    'Orders count' => 'Кількість Чеків',
    'Delivery time' => 'Час доставки',
    'Delivery type' => 'Тип доставки',
    'Delivery date' => 'Дата доставки',
    'Delivery hour' => 'Година доставки',
    'Delivery minute' => 'Хвилина доставки',
    'on' => 'на',
    'Free cost from' => 'Безкоштовно від',
    'Choose shipping type' => 'Виберіть тип доставки',
    'Choose payment type' => 'Виберіть тип оплати',
    'Total' => 'Разом',
    'Date from' => 'С',
    'Yesterday' => 'Вчора',
    'Tomorrow' => 'Завтра',
    'Some value' => 'Інше',
    'Date to' => 'По',
    'Difference' => 'Різниця',
    'Month' => 'Місяць',
    'Day' => 'День',
    'Order statistics' => 'Статистика замовлень',
    'Order statistics per month' => 'Статистика замовлень за місяць',
    'Previous' => 'Попередній',
    'Next' => 'Наступний',
    'month_01' => 'Січень',
    'month_02' => 'Лютий',
    'month_03' => 'Березень',
    'month_04' => 'Квітень',
    'month_05' => 'Травень',
    'month_06' => 'Червень',
    'month_07' => 'Липень',
    'month_08' => 'Серпень',
    'month_09' => 'Вересень',
    'month_10' => 'Жовтень',
    'month_11' => 'Листопад',
    'month_12' => 'Грудень',
    'dayname_0' => 'Неділя',
    'dayname_1' => 'Понеділок',
    'dayname_2' => 'Вівторок',
    'dayname_3' => 'Середа',
    'dayname_4' => 'Четвер',
    'dayname_5' => 'П\'ятниця',
    'dayname_6' => 'Субота',
    'Payments' => 'Платежі',
    'Widget call automacly' => 'Виклик віджета в момент оформлення замовлення',
    'Order #' => 'Чек №',
    'Example' => 'Приклад',
    'Operator area' => 'Операторка',
    'Print' => 'Друк',
    'Clients' => 'Клієнти',
    'Statistics' => 'Статистика',
    'Delivery to time' => 'Доставити до часу',
    'Fast order' => 'Швидкий заказ',
    'Discount' => 'Знижка',
    'How call you?' => 'Як до Вас звертатися?',
    'Element types' => 'Типи елементів',
    'With elements' => 'З елементами',
    'Elements count' => 'Кількість позіцій',
    'Write-off' => 'Списання',
    'Address' => 'Адреса',
    'organization' => 'Організація',
    'Assigment' => 'Наряд',
    'Assigments' => 'Наряди',
    'Reset' => 'Скинути',
    'Add to order' => 'Додати до замовлення',
    'Certificate' => 'Сертифікат',
    'Deleted' => 'Видалено',
    'Phone or email is required' => 'Телефон чи e-mail обов`язковий',
    'Search by date' => 'Пошук по даті',
    'Element name' => 'Елемент',
    'Cash Boxes' => 'Каси',
    'Add Cash Box' => 'Додати касу',
    'Work Shift' => 'Робоча зміна',
    'Return' => 'Повернення',
    'Cash withdrawal' => 'Видача готівки',
    'Removed goods from the order' => 'Вилучений товар із замовлення',
    'Sold goods per day' => 'Продані товари за день',
    'Cash on cash box' => 'Готівка в касі',
    'Lack' => 'Недостача',
    'Update the cash box data' => 'Оновити данні каси',
    'Information about the cash box' => 'Інформація про касу',
    'Default user' => 'Користувач за замовчуванням',
    'Open work shift' => 'Відкрити робочу зміну',
    'Close the work shift' => 'Закрити зміну',
    'Open change' => 'Відкрита зміна',
    'No open changes' => 'Відсутні відкриті зміни',
    'Closed changes' => 'Закриті зміни',
    'Change is closed by' => 'Зміна закрита',
    'User' => 'Користувач',
    'Cash Box' => 'Каса',
    'Created' => 'Створена',
    'Updated' => 'Оновлена',
    'Unique key' => 'Унікальний ключ',
    'Cash withdrawal amount' => 'Сума видачі готівки',
    'Amount of returns' => 'Сума повернень',
    'Closed by user' => 'Закрито користувачем',
    'Lack of cash' => 'Недостача в касі',
    'The amount in cash box' => 'Кількість готівки в касі',
    'Is the work shift closed' => 'Чи закрита зміна',
    'Cash on cash box when closing a work shift' => 'Готівка в касі при закритті зміни',
    'Cash on cash box when opening a change' => 'Готівка в касі при відкритті зміни',
    'Paid by card' => 'Оплачено карткою',
    'Paid by cash' => 'Оплачено готівкою',
    'Closed at' => 'Закрито о',
    'Update at' => 'Оновлено о',
    'Open at' => 'Відкрито о',
    'User Statistics' => 'Статистика по користувачах',
    'Total' => 'Загальна',
    'By week' => 'За тиждень',
    'By day' => 'За день',
    'Card turnover' => 'Оборот Картою',
    'Cash turnover' => 'Оборот готівкою',
    'Cash box turnover' => 'Оборот по касі',
    'Number of open changes' => 'Кількість відкритих змін',
    'Cashbox order key' => 'Номер чеку з каси',
    'Cash deposit amount' => 'Сума внесених коштів',
    'Deposit cash' => 'Внесення готівки',
];
