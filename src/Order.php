<?php

namespace dvizh\order;

use dvizh\order\models\Order as OrderModel;
use yii\base\Component;
use yii\db\Query;
use yii;

/**
 * Class Order
 * @package dvizh\order
 *
 * @property int $organization_id
 * @property boolean $is_assigment
 *
 */
class Order extends Component
{
    /**
     * @var int|null
     */
    private $organization_id;
    /**
     * @var bool
     */
    private $is_assigment = false;

    /**
     * @inheritDoc
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @param mixed $query
     * @return mixed
     */
    private function buildQuery($query)
    {
        if ($this->organization_id) {
            $query->andWhere('({{%order}}.organization_id IS NULL OR {{%order}}.organization_id = :organization_id)', [':organization_id' => $this->organization_id]);
        }

        $query->andWhere('({{%order}}.is_deleted IS NULL OR {{%order}}.is_deleted != 1)');

        if ($this->is_assigment) {
            $query->andWhere('{{%order}}.is_assigment = 1');
        } else {
            $query->andWhere('({{%order}}.is_assigment IS NULL OR {{%order}}.is_assigment != 1)');
        }

        return $query;
    }

    /**
     * @return mixed
     */
    private function orderQuery()
    {
        $return = (new Query())->from('{{%order}}');

        return $this->buildQuery($return);
    }

    /**
     * @return mixed
     */
    private function orderFinder()
    {
        $return = OrderModel::find();

        return $this->buildQuery($return);
    }

    /**
     * @return $this
     */
    public function resetConditions()
    {
        $this->organization_id = null;
        $this->is_assigment = false;

        return $this;
    }

    /**
     * @param int $organization_id
     * @return $this
     */
    public function setOrganization($organization_id)
    {
        $this->organization_id = $organization_id;

        return $this;
    }

    /**
     * @return $this
     */
    public function assigment()
    {
        $this->is_assigment = true;

        return $this;
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function get($id)
    {
        return OrderModel::findOne($id);
    }

    /**
     * @param string $dateStart
     * @param string $dateStop
     * @param null|mixed $where
     * @return mixed
     */
    public function getOrdersByDatePeriod($dateStart, $dateStop, $where = null)
    {
        if ($dateStop == '0000-00-00 00:00:00' | empty($dateStop)) {
            $dateStop = date('Y-m-d H:i:s');
        }

        $query = $this->orderFinder()
            ->andWhere('DATE_FORMAT(date,\'%Y-%m-%d\') >= :dateStart AND DATE_FORMAT(date,\'%Y-%m-%d\') <= :dateStop', [':dateStart' => $dateStart, ':dateStop' => $dateStop])
            ->orderBy('timestamp ASC');

        if ($where) {
            $query->andWhere($where);
        }

        $this->resetConditions();

        return $query->all();
    }

    /**
     * @param null|mixed $month
     * @param null|mixed $where
     * @return array
     */
    public function getStatInMoth($month = null, $where = null)
    {
        if (!$month) {
            $month = date('Y-m');
        }

        $query = $this->orderQuery();
        $query->addSelect(['sum(cost) as total, sum(count) as count_elements, COUNT(DISTINCT id) as count_orders'])
            ->from(['{{%order}}'])
            ->andWhere('DATE_FORMAT(date, "%Y-%m") = :date', [':date' => $month]);

        if ($where) {
            $query->andWhere($where);
        }

        $result = $query->one();

        $this->resetConditions();

        return array_map('intval', $result);
    }

    /**
     * @param string $date
     * @param null|mixed $where
     * @return array
     */
    public function getStatByDate($date, $where = null)
    {
        $query = $this->orderQuery();

        $query->addSelect(['sum(cost) as total, sum(count) as count_elements, COUNT(DISTINCT id) as count_orders, sum(cost - base_cost) as profit'])
            ->from(['{{%order}}'])
            ->andWhere('DATE_FORMAT(date, "%Y-%m-%d") = :date', [':date' => $date]);

        if ($where) {
            $query->andWhere($where);
        }

        $result = $query->one();

        $this->resetConditions();

        return array_map('intval', $result);
    }

    /**
     * @param string $dateStart
     * @param null|string $dateStop
     * @param null|mixed $where
     * @return array
     */
    public function getStatByDatePeriod($dateStart, $dateStop = null, $where = null)
    {
        if ($dateStop == '0000-00-00 00:00:00' | empty($dateStop)) {
            $dateStop = date('Y-m-d H:i:s');
        }

        $query = $this->orderQuery();
        $query->addSelect(['sum(cost) as total, sum(count) as count_elements, COUNT(DISTINCT id) as count_orders, sum(cost - base_cost) as profit'])
            ->from(['{{%order}}']);

        if ($dateStart == $dateStop) {
            $query->andWhere('DATE_FORMAT(date,\'%Y-%m-%d\') = :date', [':date' => $dateStart]);
        } else {
            $query->andWhere('DATE_FORMAT(date,\'%Y-%m-%d\') >= :dateStart AND DATE_FORMAT(date,\'%Y-%m-%d\') <= :dateStop', [':dateStart' => $dateStart, ':dateStop' => $dateStop]);
        }

        if ($where) {
            $query->andWhere($where);
        }

        $result = $query->one();

        $this->resetConditions();

        return array_map('intval', $result);
    }

    /**
     * @param mixed $model
     * @param string $dateStart
     * @param string $dateStop
     * @param null|mixed $where
     * @return array|int[]
     */
    public function getStatByModelAndDatePeriod($model, $dateStart, $dateStop, $where = null)
    {
        if ($dateStop == '0000-00-00 00:00:00' | empty($dateStop)) {
            $dateStop = date('Y-m-d H:i:s');
        }

        $query = $this->orderQuery();
        $query->addSelect(['sum(e.count*e.price) as total, sum(e.count) as count_elements, COUNT(DISTINCT order_id) as count_orders, sum(cost - base_cost) as profit'])
            ->from(['order_element e'])
            ->leftJoin('{{%order}}', '{{%order}}.id = e.order_id')
            //->groupBy('order.id')
            ->andWhere('{{%order}}.is_assigment != 1')
            ->andWhere('{{%order}}.date >= :dateStart AND {{%order}}.date <= :dateStop', [':dateStart' => $dateStart, ':dateStop' => $dateStop])
            ->andWhere(['e.model' => $model]);

        if ($where) {
            $query->andWhere($where);
        }

        $result = $query->one();

        $this->resetConditions();

        if (!$result) {
            return ['total' => 0, 'count_elements' => 0, 'count_orders' => 0];
        }

        return array_map('intval', $result);
    }

    // get Order profit and total

    /**
     * @param null|int[] $workShitIds
     * @return mixed
     */
    public function getStatByMonth($workShitIds = null)
    {
        $query = (new Query())->from('{{%order}}');
        $query->leftJoin('{{%order_element}}', '{{%order_element}}.order_id = {{%order}}.id');
//        $query->leftJoin('{{%product_price}}', '{{%product_price}}.product_id = {{%order_element}}.item_id');

//        $query->addSelect('
//        sum({{%product_price}}.price * {{%order_element}}.count) as total,
//        sum(({{%product_price}}.price - {{%product_price}}.income_price) * {{%order_element}}.count) as profit
//        ');
        $query->addSelect('sum({{%order_element}}.price * {{%order_element}}.count) as total');
        $query->addSelect('sum(({{%order_element}}.price - {{%order_element}}.base_price) * {{%order_element}}.count) as profit');

        $query->addSelect('CAST({{%order}}.date as date) as order_date, DAY({{%order}}.date) as day_of_month');
        $query->addSelect('COUNT(DISTINCT {{%order}}.id) as count');

        $query->andWhere('MONTH({{%order}}.date) = MONTH(NOW())');
        $query->groupBy('day_of_month');
        $query->orderBy('day_of_month');
        $query->indexBy('day_of_month');


        if (is_array($workShitIds)) {
            if (empty($workShitIds)) {
                $query->andWhere(['{{%order}}.workshift_id' => 0]);
            } else {
                $query->andWhere(['{{%order}}.workshift_id' => $workShitIds]);
            }
        } else {
            $query->andWhere(['not', ['{{%order}}.workshift_id' => null]]);
        }

        $result = $query->all();
        return $result;
    }

    /**
     * @param null|int[] $workShitIds
     * @return mixed
     */
    public function getStatByWeek($workShitIds = null)
    {
        $query = (new Query())->from('{{%order}}');
        $query->leftJoin('{{%order_element}}', '{{%order_element}}.order_id = {{%order}}.id');
//        $query->leftJoin('{{%product_price}}', '{{%product_price}}.product_id = {{%order_element}}.item_id');

//        $query->addSelect('sum({{%product_price}}.price * {{%order_element}}.count) as total, sum(({{%product_price}}.price - {{%product_price}}.income_price) * {{%order_element}}.count) as profit');
        $query->addSelect('sum({{%order_element}}.price * {{%order_element}}.count) as total');
        $query->addSelect('sum(({{%order_element}}.price - {{%order_element}}.base_price) * {{%order_element}}.count) as profit');

        $query->addSelect('CAST({{%order}}.date as date) as order_date, DAYNAME({{%order}}.date) as day_of_week');

        $query->andWhere('WEEK({{%order}}.date, 1) = WEEK(NOW(), 1)');
        $query->groupBy('day_of_week');
        $query->orderBy('order_date');
        $query->indexBy('day_of_week');


        if (is_array($workShitIds)) {
            if (empty($workShitIds)) {
                $query->andWhere(['{{%order}}.workshift_id' => 0]);
            } else {
                $query->andWhere(['{{%order}}.workshift_id' => $workShitIds]);
            }
        } else {
            $query->andWhere(['not', ['{{%order}}.workshift_id' => null]]);
        }

        $result = $query->all();
        return $result;
    }

    /**
     * @param null|int[] $workShitIds
     * @param null|string|int $hours
     * @return mixed
     */
    public function getStatByDay($workShitIds = null, $hours = null)
    {
        $query = (new Query())->from('{{%order}}');

        $query->leftJoin('{{%order_element}}', '{{%order_element}}.order_id = {{%order}}.id');
//        $query->leftJoin('{{%product_price}}', '{{%product_price}}.product_id = {{%order_element}}.item_id');

        $query->addSelect('COUNT(DISTINCT {{%order}}.id) as count');

//        $query->addSelect('sum({{%product_price}}.price * {{%order_element}}.count) as total');
//        $query->addSelect('sum(({{%product_price}}.price - {{%product_price}}.income_price)  * {{%order_element}}.count) as profit');
        $query->addSelect('sum({{%order_element}}.price * {{%order_element}}.count) as total');
        $query->addSelect('sum(({{%order_element}}.price - {{%order_element}}.base_price)  * {{%order_element}}.count) as profit');

        // uncoment on finish
        $query->andWhere('WEEKDAY({{%order}}.date) = WEEKDAY(NOW())');

        $query->addSelect('CAST({{%order}}.date as date) as order_date');

        $query->addSelect('DAYNAME({{%order}}.date) as day_of_week');

        $query->addSelect(["TIME_FORMAT(TIME({{%order}}.time), \"%H\") as order_time"]);

        $query->groupBy('order_time');

        $query->indexBy('order_time');

        if (is_array($workShitIds)) {
            if (empty($workShitIds)) {
                $query->andWhere(['{{%order}}.workshift_id' => 0]);
            } else {
                $query->andWhere(['{{%order}}.workshift_id' => $workShitIds]);
            }
        } else {
            $query->andWhere(['not', ['{{%order}}.workshift_id' => null]]);
        }

        if ($hours) {
            $query->andWhere(['TIME_FORMAT(TIME({{%order}}.time), "%H")' => $hours]);
        }

        $result = $query->all();
        return $result;
    }

}
