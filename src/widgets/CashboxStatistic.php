<?php

namespace dvizh\order\widgets;

use \yii\base\Widget;
use dvizh\order\models\CashboxWorkshift;
use yii;
use yii\helpers\Html;
use yii\helpers\Url;
use dvizh\order\models\Order;
use dvizh\order\models\Element;
use yii\helpers\ArrayHelper;

class CashboxStatistic extends Widget
{

    public $view = 'cashbox-statistic';
    public $cashbox = null;
    public $workshiftIds = null;
    public $users = null;

    public function init()
    {
        if (array_key_exists('statisticForUser', Yii::$app->request->queryParams)) {
            $this->users = (Yii::$app->request->queryParams)['statisticForUser'];
        }

        if ($this->cashbox) {
            $query = $this->cashbox->getWorkshift();
        } else {
            $query = CashboxWorkshift::find();
        }

        if ($this->users) {
            $query->where(['user_id' => $this->users]);
        }
        $this->workshiftIds = $query->select('id')->column();

        parent::init();
        return true;
    }

    public function run()
    {
        $workShitIds = $this->workshiftIds;

        $statHours = json_decode($this->getStatHours());
        $thisMonthDays = range(1, date("t"));

        $statByMonth = yii::$app->order->getStatByMonth($workShitIds);
        $statByWeek = yii::$app->order->getStatByWeek($workShitIds);
        $statBtDay = yii::$app->order->getStatByDay($workShitIds, $statHours);

        // Total --------------------
        $selectFromWorkShift = ['COUNT(DISTINCT id) as count_workshifts, 
        sum(card_amount) as card_total, sum(cash_amount) as cash_total, 
        sum(cash_amount + card_amount) as turnover_workshifts'];

        //        Today
        $today_order = yii::$app->order->getStatByDate(
            date('Y-m-d'),
            ['workshift_id' => $this->workshiftIds]
        );

        $today_workshift = $this->getWorkshiftModel()
            ->select($selectFromWorkShift)
            ->andWhere('DATE_FORMAT(crete_at, "%Y-%m-%d") = :date', [':date' => date('Y-m-d')]);
        if ($this->users) {
            $today_workshift->andWhere(['user_id' => $this->users]);
        }
        
        $today = array_merge($today_order, array_map('intval', $today_workshift->asArray()->one()));

//        In Month
        $inMonth_order = yii::$app->order->getStatInMoth(null, ['workshift_id' => $this->workshiftIds]);

        $inMonth_workshift = $this->getWorkshiftModel()
            ->select($selectFromWorkShift)
            ->andWhere('DATE_FORMAT(crete_at, "%Y-%m") = :date', [':date' => date('Y-m')]);
        if ($this->users) {
            $inMonth_workshift->andWhere(['user_id' => $this->users]);
        }
        $inMonth = array_merge($inMonth_order, array_map('intval', $inMonth_workshift->asArray()->one()));

//        By Month
        $byOldMonth_start = date('Y-m-d H:i:s', time() - (86400 * 30));
        $byMonth_stop = date('Y-m-d H:i:s');
        $byMonth_order = yii::$app->order->getStatByDatePeriod(
            $byOldMonth_start,
            $byMonth_stop,
            ['workshift_id' => $this->workshiftIds]
        );

        $byMonth_workshift = $this->getWorkshiftModel()
            ->select($selectFromWorkShift)
            ->andWhere('DATE_FORMAT(crete_at,\'%Y-%m-%d\') >= :dateStart', [':dateStart' => $byOldMonth_start])
            ->andWhere('DATE_FORMAT(crete_at,\'%Y-%m-%d\') <= :dateStop', [':dateStop' => $byMonth_stop]);
        if ($this->users) {
            $byMonth_workshift->andWhere(['user_id' => $this->users]);
        }
        $byMonth = array_merge($byMonth_order, array_map('intval', $byMonth_workshift->asArray()->one()));

//        By Old Month
        $byOldMonth_start = date('Y-m-d H:i:s', time() - (86400 * 60));
        $byOldMonth_stop = date('Y-m-d H:i:s', time() - (86400 * 30));
        $byOldMonth_order = yii::$app->order->getStatByDatePeriod(
            $byOldMonth_start,
            $byOldMonth_stop,
            ['workshift_id' => $this->workshiftIds]
        );

        $byOldMonth_workshifts = $this->getWorkshiftModel()
            ->select($selectFromWorkShift)
            ->andWhere('DATE_FORMAT(crete_at,\'%Y-%m-%d\') >= :dateStart', [':dateStart' => $byOldMonth_start])
            ->andWhere('DATE_FORMAT(crete_at,\'%Y-%m-%d\') <= :dateStop', [':dateStop' => $byOldMonth_stop]);
        if ($this->users) {
            $byOldMonth_workshifts->andWhere(['user_id' => $this->users]);
        }

        $byOldMonth = array_merge($byOldMonth_order, array_map('intval', $byOldMonth_workshifts->asArray()->one()));

        return $this->render($this->view, [
            'statistic' => [
                'total' => [
                    'today' => $today,
                    'inMonth' => $inMonth,
                    'byMonth' => $byMonth,
                    'byOldMonth' => $byOldMonth,
                ],
                'month' => [
                    'labels' => $thisMonthDays,
                    'data' => $this->fillData($thisMonthDays, $statByMonth, ['total', 'profit', 'count'])
                ],
                'week' => [
                    'labels' => ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'],
                    'data' => $this->fillData(['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'], $statByWeek, ['total', 'profit'])
                ],
                'day' => [
                    'labels' => $statHours,
                    'data' => $this->fillData($statHours, $statBtDay, ['total', 'profit', 'count'])
                ]
            ]
        ]);
    }

    private function getStatHours()
    {
        $keyStorage = new \common\components\keyStorage\KeyStorage();
        $key = 'backend.order-statistic-hours';

        if (!$keyStorage->has($key)) {
            return null;
        }

        $hours = $keyStorage->get($key);

        if ($hours) {
            return $hours;
        } else {
            return null;
        }
    }

    private function fillData($keys, $datas, $fields = ['total', 'profit'])
    {
        $result = [];

        foreach ($keys as $keyIndex => $keyValue) {
            foreach ($fields as $f) {
                $result[$f][$keyIndex] = $datas[$keyValue][$f] ? (integer)$datas[$keyValue][$f] : 0;
            }
        }
        return $result;
    }

    private function getWorkshiftModel()
    {
        return ($this->cashbox ? $this->cashbox->getWorkshift() : CashboxWorkshift::find());
    }

}
