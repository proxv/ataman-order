<?php

use Yii;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use yii\web\JsExpression;
use kartik\select2\Select2;


$currency = yii::$app->getModule('order')->currency;

/** @var $statistic */
$today = $statistic['total']['today'];
$inMonth = $statistic['total']['inMonth'];
$byMonth = $statistic['total']['byMonth'];
$byOldMonth = $statistic['total']['byOldMonth'];

$js = <<<JS
function setUser(e) {
    $('.form-inline').submit()
}
JS;

$this->registerJs($js, View::POS_END);

$formUrl = '/' . Yii::$app->request->getPathInfo();
$formParam = [$formUrl];

if (array_key_exists('id', Yii::$app->request->queryParams)) {
    $formParam['id'] = Yii::$app->request->queryParams['id'];
}

?>
<div class="panel-group cashbox-statistic" role="tablist" aria-multiselectable="false">
    <!--    --><?php //Pjax::begin(); ?>
    <div class="panel panel-primary">
        <div class="panel-heading" role="tab" id="headingOne">
            <div class="row">
                <div class="col-xs-12 col-md-8">
                    <h4 class="panel-title" style="line-height: 2em;">
                        <a role="button" data-toggle="collapse" data-parent=".cashbox-statistic" href="#collapseOne"
                           aria-expanded="false" aria-controls="collapseOne">
                            <?= yii::t('order', 'Statistics'); ?> ...
                        </a>
                    </h4>
                </div>
                <div class="col-xs-12 col-md-4">
                    <?= Html::beginForm($formParam, 'get', ['data-pjax' => '', 'class' => 'form-inline']); ?>
                    <?= Select2::widget([
                        'data' => ArrayHelper::map(\common\models\User::find()->all(), 'id', 'username'),
                        'name' => 'statisticForUser',
                        'language' => 'ru',
                        'value' => (Yii::$app->request->queryParams)['statisticForUser'],
                        'size' => 'sm',
//                        'readonly' => true,
                        'options' => [
                            'placeholder' => yii::t('order', 'User Statistics') . ' ...',
                            'multiple' => true
                        ],
                        'pluginOptions' => [
                            'allowClear' => true,
                        ],
                        'pluginEvents' => [
                            'change' => "setUser",
                        ]
                    ]); ?>
                    <?= Html::endForm() ?>
                </div>
            </div>
        </div>
        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
            <div class="panel-body">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a href="#statistic-total" aria-controls="statistic-total" role="tab"
                           data-toggle="tab"><?= yii::t('order', 'Total');?></a>
                    </li>
                    <li role="presentation">
                        <a href="#statistic-chart-month" aria-controls="statistic-chart-month" role="tab"
                           data-toggle="tab"><?= yii::t('order', 'By month');?></a>
                    </li>
                    <li role="presentation">
                        <a href="#statistic-chart-week" aria-controls="statistic-chart-week" role="tab"
                           data-toggle="tab"><?= yii::t('order', 'By week');?></a>
                    </li>
                    <li role="presentation">
                        <a href="#statistic-chart-day" aria-controls="statistic-chart-day" role="tab"
                           data-toggle="tab"><?= yii::t('order', 'By day');?></a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="statistic-total">
                        <table class="table table-hover table-responsive">
                            <thead>
                            <tr>
                                <th>&nbsp;</th>
                                <th><?= yii::t('order', 'Today'); ?></th>
                                <th><?= yii::t('order', 'In month'); ?></th>
                                <th><?= yii::t('order', 'By month'); ?></th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td><?= yii::t('order', 'Turnover'); ?></td>
                                <td><?= round($today['total']); ?><?= $currency; ?></td>
                                <td><?= round($inMonth['total'], 2); ?><?= $currency; ?></td>
                                <td>
                                    <?= round($byMonth['total'], 2); ?><?= $currency; ?>
                                    <?php
                                    if ($byOldMonth['total']) {
                                        $cssClass = '';
                                        $sum = '+0';
                                        if ($byOldMonth['total'] < $inMonth['total']) {
                                            $cssClass = 'good-result';
                                            $sum = '+' . ($inMonth['total'] - $byOldMonth['total']);
                                        } elseif ($byOldMonth['total'] > $inMonth['total']) {
                                            $cssClass = 'bad-result';
                                            $sum = '-' . ($byOldMonth['total'] - $inMonth['total']);
                                        }
                                        ?>
                                        <span class="result <?= $cssClass; ?>"><?= $sum; ?></span>
                                        <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td><?= yii::t('order', 'Profit'); ?></td>
                                <td><?= round($today['profit']); ?><?= $currency; ?></td>
                                <td><?= round($inMonth['profit'], 2); ?><?= $currency; ?></td>
                                <td>
                                    <?= round($byMonth['profit'], 2); ?><?= $currency; ?>
                                    <?php
                                    if ($byOldMonth['profit']) {
                                        $cssClass = '';
                                        $sum = '+0';
                                        if ($byOldMonth['profit'] < $inMonth['profit']) {
                                            $cssClass = 'good-result';
                                            $sum = '+' . ($inMonth['profit'] - $byOldMonth['profit']);
                                        } elseif ($byOldMonth['profit'] > $inMonth['profit']) {
                                            $cssClass = 'bad-result';
                                            $sum = '-' . ($byOldMonth['profit'] - $inMonth['profit']);
                                        }
                                        ?>
                                        <span class="result <?= $cssClass; ?>"><?= $sum; ?></span>
                                        <?php
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr>
                                <td><?= yii::t('order', 'Orders count'); ?></td>
                                <td><?= round($today['count_orders'], 2); ?></td>
                                <td><?= round($inMonth['count_orders'], 2); ?></td>
                                <td><?= round($byMonth['count_orders'], 2); ?></td>
                            </tr>
                            <tr>
                                <td><?= yii::t('order', 'Elements count'); ?></td>
                                <td><?= round($today['count_elements'], 2); ?></td>
                                <td><?= round($inMonth['count_elements'], 2); ?></td>
                                <td><?= round($byMonth['count_elements'], 2); ?></td>
                            </tr>
                            <tr>
                                <td><?= yii::t('order', 'Average check'); ?></td>
                                <td><?php if ($today['count_orders']) { ?><?= round($today['total'] / $today['count_orders'], 2); ?><?= $currency; ?><?php } else echo 0; ?></td>
                                <td><?php if ($inMonth['count_orders']) { ?><?= round($inMonth['total'] / $inMonth['count_orders'], 2); ?><?= $currency; ?><?php } else echo 0; ?></td>
                                <td><?php if ($byMonth['count_orders']) { ?><?= round($byMonth['total'] / $byMonth['count_orders'], 2); ?><?= $currency; ?><?php } else echo 0; ?></td>
                            </tr>
                            <tr>
                                <td><?= yii::t('order', 'Number of open changes'); ?></td>
                                <td><?= round($today['count_workshifts'], 2); ?></td>
                                <td><?= round($inMonth['count_workshifts'], 2); ?></td>
                                <td><?= round($byMonth['count_workshifts'], 2); ?></td>
                            </tr>
                            <tr>
                                <td><?= yii::t('order', 'Cash box turnover'); ?></td>
                                <td><?= round($today['turnover_workshifts'], 2); ?><?= $currency; ?></td>
                                <td><?= round($inMonth['turnover_workshifts'], 2); ?><?= $currency; ?></td>
                                <td><?= round($byMonth['turnover_workshifts'], 2); ?><?= $currency; ?></td>
                            </tr>
                            <tr>
                                <td><?= yii::t('order', 'Cash turnover'); ?></td>
                                <td><?= round($today['cash_total'], 2); ?><?= $currency; ?></td>
                                <td><?= round($inMonth['cash_total'], 2); ?><?= $currency; ?></td>
                                <td><?= round($byMonth['cash_total'], 2); ?><?= $currency; ?></td>
                            </tr>
                            <tr>
                                <td><?= yii::t('order', 'Card turnover'); ?></td>
                                <td><?= round($today['card_total'], 2); ?><?= $currency; ?></td>
                                <td><?= round($inMonth['card_total'], 2); ?><?= $currency; ?></td>
                                <td><?= round($byMonth['card_total'], 2); ?><?= $currency; ?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="statistic-chart-month">
                        <?= dosamigos\chartjs\ChartJs::widget([
                            'type' => 'line',
                            'clientOptions' => ['responsive' => true],
                            'options' => [
                                'height' => '100'
                            ],
                            'data' => [
                                'labels' => $statistic['month']['labels'],
                                'datasets' => [
                                    [
                                        'label' => "Кількість продаж",
                                        'backgroundColor' => "rgba(179,181,198,0.2)",
                                        'borderColor' => "rgba(179,181,198,1)",
                                        'pointBackgroundColor' => "rgba(179,181,198,1)",
                                        'pointBorderColor' => "#fff",
                                        'pointHoverBackgroundColor' => "#fff",
                                        'pointHoverBorderColor' => "rgba(179,181,198,1)",
                                        'data' => $statistic['month']['data']['count']
                                    ],
                                    [
                                        'label' => "Оборот",
                                        'backgroundColor' => "rgba(255,99,132,0.2)",
                                        'borderColor' => "rgba(255,99,132,1)",
                                        'pointBackgroundColor' => "rgba(255,99,132,1)",
                                        'pointBorderColor' => "#fff",
                                        'pointHoverBackgroundColor' => "#fff",
                                        'pointHoverBorderColor' => "rgba(255,99,132,1)",
                                        'data' => $statistic['month']['data']['total']
                                    ],
                                    [
                                        'label' => "Прибуток",
                                        'backgroundColor' => "rgba(92, 99, 255,0.2)",
                                        'borderColor' => "rgba(192, 99, 255, 1)",
                                        'pointBackgroundColor' => "rgba(192, 99, 255, 1)",
                                        'pointBorderColor' => "#fff",
                                        'pointHoverBackgroundColor' => "#fff",
                                        'pointHoverBorderColor' => "rgba(192, 99, 255, 1)",
                                        'data' => $statistic['month']['data']['profit']
                                    ]
                                ]
                            ]
                        ]); ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="statistic-chart-week">
                        <?= dosamigos\chartjs\ChartJs::widget([
                            'type' => 'bar',
                            'clientOptions' => ['responsive' => true],
                            'options' => [
                                'height' => '100'
                            ],
                            'data' => [
                                'labels' => $statistic['week']['labels'],
                                'datasets' => [
                                    [
                                        'label' => "Оборот",
                                        'backgroundColor' => "rgba(179,181,198,0.8)",
                                        'borderColor' => "rgba(179,181,198,1)",
                                        'pointBackgroundColor' => "rgba(179,181,198,1)",
                                        'pointBorderColor' => "#fff",
                                        'pointHoverBackgroundColor' => "#fff",
                                        'pointHoverBorderColor' => "rgba(179,181,198,1)",
                                        'data' => $statistic['week']['data']['total']
                                    ],

                                    [
                                        'label' => "Прибуток",
                                        'backgroundColor' => "rgba(179,81,98,0.8)",
                                        'borderColor' => "rgba(179,181,198,1)",
                                        'pointBackgroundColor' => "rgba(179,181,198,1)",
                                        'pointBorderColor' => "#fff",
                                        'pointHoverBackgroundColor' => "#fff",
                                        'pointHoverBorderColor' => "rgba(179,181,198,1)",
                                        'data' => $statistic['week']['data']['profit']
                                    ]
                                ]
                            ]
                        ]); ?>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="statistic-chart-day">
                        <?= dosamigos\chartjs\ChartJs::widget([
                            'type' => 'line',
                            'clientOptions' => ['responsive' => true],
                            'options' => [
                                'height' => '100'
                            ],
                            'data' => [
                                'labels' => $statistic['day']['labels'],
                                'datasets' => [
                                    [
                                        'label' => "Кількість продаж",
                                        'backgroundColor' => "rgba(179,181,198,0.2)",
                                        'borderColor' => "rgba(179,181,198,1)",
                                        'pointBackgroundColor' => "rgba(179,181,198,1)",
                                        'pointBorderColor' => "#fff",
                                        'pointHoverBackgroundColor' => "#fff",
                                        'pointHoverBorderColor' => "rgba(179,181,198,1)",
                                        'data' => $statistic['day']['data']['count']
                                    ],
                                    [
                                        'label' => "Оборот",
                                        'backgroundColor' => "rgba(255,99,132,0.2)",
                                        'borderColor' => "rgba(255,99,132,1)",
                                        'pointBackgroundColor' => "rgba(255,99,132,1)",
                                        'pointBorderColor' => "#fff",
                                        'pointHoverBackgroundColor' => "#fff",
                                        'pointHoverBorderColor' => "rgba(255,99,132,1)",
                                        'data' => $statistic['day']['data']['total']
                                    ],
                                    [
                                        'label' => "Прибуток",
                                        'backgroundColor' => "rgba(92, 99, 255,0.2)",
                                        'borderColor' => "rgba(192, 99, 255, 1)",
                                        'pointBackgroundColor' => "rgba(192, 99, 255, 1)",
                                        'pointBorderColor' => "#fff",
                                        'pointHoverBackgroundColor' => "#fff",
                                        'pointHoverBorderColor' => "rgba(192, 99, 255, 1)",
                                        'data' => $statistic['day']['data']['profit']
                                    ]
                                ]
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--    --><?php //Pjax::end(); ?>
</div>