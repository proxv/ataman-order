<?php

namespace dvizh\order\models;

use Yii;
use dvizh\order\models\tools\OrderReturnQuery;

/**
 * This is the model class for table "order_return".
 *
 * @property int $id
 * @property int $order_id
 * @property int $user_id
 * @property string $date
 * @property string|null $description
 * @property int $workshift_id
 */
class OrderReturn extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_return';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'user_id', 'date', 'workshift_id'], 'required'],
            [['order_id', 'user_id', 'workshift_id'], 'integer'],
            [['date'], 'safe'],
            [['description'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => yii::t('order', 'Order') . ' №',
            'user_id' => yii::t('order', 'Seller'),
            'date' => yii::t('order', 'Date'),
            'description' => yii::t('order', 'Description'),
            'workshift_id' => yii::t('order', 'Work Shift') . ' №',
            'cashbox_id' => yii::t('order', 'Cash Box'),
            'totalPrice' => yii::t('order', 'Total cost'),
        ];
    }

    /**
     * @return int
     */
    public function getElementCount()
    {
        return $this->getElementsRelation()->count();
    }

    /**
     * @return $this
     */
    public static function find()
    {
        $query = new OrderReturnQuery(get_called_class());

        return $query->with('elementsRelation');
    }

    /**
     * Get Order by order id
     * @return mixed
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }
    /**
     * Get Order by order id
     * @return mixed
     */
//    public function getOrderByCashboxOrderKey()
//    {
//        return $this->hasOne(Order::className(), ['cashbox_order_key' => 'cashbox_order_key']);
//    }

    /**
     * Get User by user id
     * @return mixed|null
     */
    public function getUser()
    {
        $userModel = yii::$app->getModule('order')->sellerModel;
        if ($userModel && class_exists($userModel)) {
            return $this->hasOne($userModel::className(), ['id' => 'user_id'])->one();
        }
        return null;
    }

    /**
     * @return CashboxWorkshift
     */
    public function getWorkshift()
    {
        return $this->hasOne(CashboxWorkshift::className(), ['id' => 'workshift_id'])->one();
    }

    /**
     * @return mixed
     */
    public function getCashbox()
    {
        $workShift = $this->getWorkshift();

        return $workShift->getCashbox();
    }

    /**
     * Get Order Return Elements
     * @return mixed
     */
    public function getElementsRelation()
    {
        return $this->hasMany(OrderReturnElement::className(), ['return_id' => 'id']);
    }

    /**
     * @return float
     */
    public function getTotalPrice()
    {
        $totalPrice = 0;
        $returnElements = $this->getElementsRelation()->all();

        foreach ($returnElements as $returnElement) {
            $totalPrice += $returnElement->getTotalPrice();
        }

        return $totalPrice;
    }

    public function getProduct()
    {
        return $this->hasOne(\dvizh\shop\models\Product::className(), ['id' => 'item_id']);

    }

    public function sendToApi()
    {
        if(!$this->workshift->cashbox->is_rro_active){
            return true;
        }
        $departmentId = $this->order->department_id;

        $orderReturnElements = $this->getElementsRelation()->all();
//        var_dump($this->order->fiscalized->tax_id);
//        return true;

        $check = new \common\modules\openprro\models\Check();
        $check->department_id = $departmentId;
        $check->tax_id = $this->order->fiscalized->tax_id;

        $taxeSum = [];
        foreach ($orderReturnElements as $orderReturnElement) {
            $product = $orderReturnElement->product;
            $orderElement = Element::find()
                ->where(['order_id' => $this->order_id, 'item_id' => $orderReturnElement->item_id])
                ->one();

            $realization = new \common\modules\openprro\models\Realization();
            $realization->barcode = (integer)$product->barcode;
            $realization->name = $product->name;
            $realization->unit_num = $product->short_text;

            $realization->amount = (float)$orderReturnElement->count;
            $realization->price = (float)$orderElement->price;
            $realization->cost = round((float)($realization->amount * $realization->price), 2);

            $realization->letters = 'А';
            $taxeSum['А'] += $realization->cost;

            if ($product->is_excise) {
                $realization->letters .= 'М';
                $taxeSum['М'] += $realization->cost;

                $excise = new \common\modules\openprro\models\Excise($orderElement->excise_barcode);
                $realization->excise_labels[] = $excise->create();
            }

            $check->reals[] = $realization->create();
        }

        foreach ($taxeSum as $key => $value) {
            $taxe = new \common\modules\openprro\models\Taxse($key, $value);
            $check->taxes[] = $taxe->create();
        }

        $pay = new  \common\modules\openprro\models\Pay();
        $pay->sum = round((float)$taxeSum['А'], 2);

        $check->pays[] = $pay->getCashPay();


        $data = $check->reimbursement();

        $response = \Yii::$app->getModule('openprro')->sentToAPI($check->getMethod(), $data);
        if ($response['status'] === 'success') {
            $prroOrderReturn = new \common\modules\openprro\models\PrroOrderReturn();
            $prroOrderReturn->order_return_id = $this->id;
            $prroOrderReturn->online = gettype($response['tax_id']) === 'string' ? 0 : 1;
            $prroOrderReturn->tax_id = $response['tax_id'];
            $prroOrderReturn->qr = $response['qr'];

            return $prroOrderReturn->save();
        } else {
            return false;
        }
    }

    public function getFiscalized(){
        return $this->hasOne(\common\modules\openprro\models\PrroOrderReturn::className(), ['order_return_id' => 'id']);
    }

}
