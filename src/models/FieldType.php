<?php

namespace dvizh\order\models;

use yii;

/**
 * Class FieldType
 * @package dvizh\order\models
 *
 * @property int $id
 * @property string $name
 * @property string $widget
 * @property string $have_variants
 */
class FieldType extends \yii\db\ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_field_type}}';
    }

    /**
     * @return array[]
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['widget'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => yii::t('order', 'ID'),
            'name' => yii::t('order', 'Name'),
            'widget' => yii::t('order', 'Widget'),
        ];
    }
}
