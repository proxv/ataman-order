<?php

namespace dvizh\order\models;

use yii;
use dvizh\order\models\Field;

/**
 * Class FieldValue
 * @package dvizh\order\models
 *
 * @property int $id
 * @property int $order_id
 * @property int $field_id
 * @property string $value
 */
class FieldValue extends \yii\db\ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_field_value}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'field_id'], 'required'],
            [['order_id', 'field_id'], 'unique', 'targetAttribute' => ['order_id', 'field_id']],
            [['value'], 'string'],
            [['value'], 'safe'],
        ];
    }

    /**
     * @return mixed
     */
    public function getField()
    {
        return $this->hasOne(Field::className(), ['id' => 'field_id'])->one();
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => yii::t('order', 'ID'),
            'order_id' => yii::t('order', 'Order'),
            'field_id' => yii::t('order', 'Field'),
            'value' => yii::t('order', 'Value'),
        ];
    }
}
