<?php

namespace dvizh\order\models;

use Yii;

/**
 * This is the model class for table "order_element_removed".
 *
 * @property int $id
 * @property string $date
 * @property int $workshift_id
 * @property int $user_id
 * @property int $item_id
 * @property float $count
 */
class ElementRemoved extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_element_removed';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'workshift_id', 'user_id', 'item_id', 'date', 'count'], 'required'],
            [['date'], 'safe'],
            [['workshift_id', 'user_id', 'item_id'], 'integer'],
            [['count'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => yii::t('order', 'Date'),
            'workshift_id' => yii::t('order', 'Work Shift'),
            'user_id' => yii::t('order', 'Seller'),
            'item_id' => yii::t('order', 'Product'),
            'count' => yii::t('order', 'Count'),
        ];
    }

    /**
     * @return mixed
     */
    public function getWorkshift()
    {
        return $this->hasOne(CashboxWorkshift::className(), ['id' => 'workshift_id']);
    }

    /**
     * @return mixed
     */
    public function getCashbox()
    {
        $workshift = $this->getWorkshift()->one();
        return $workshift->getCashbox();
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        $workshift = $this->getWorkshift()->one();
        return $workshift->getUser();
    }

    /**
     * @return null|mixed
     */
    public function getProduct()
    {
        $productModel = yii::$app->getModule('order')->productModel;
        if ($productModel && class_exists($productModel)) {
            return $this->hasOne($productModel::className(), ['id' => 'item_id'])->one();
        }
        return null;
    }
}
