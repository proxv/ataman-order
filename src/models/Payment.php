<?php

namespace dvizh\order\models;

use yii;

/**
 * Class Payment
 * @package dvizh\order\models
 *
 * @property int $id
 * @property int $order_id
 * @property int $payment_type_id
 * @property int $user_id
 * @property string $description
 * @property string $ip
 * @property float $amount
 * @property string $date
 *
 * @property Order $order
 * @property Payment $payment
 */
class Payment extends \yii\db\ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_payment}}';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['order_id', 'amount', 'description', 'date', 'payment_type_id', 'ip'], 'required'],
            [['order_id', 'user_id', 'payment_type_id'], 'integer'],
            [['amount'], 'number'],
            [['date'], 'safe'],
            [['description'], 'string', 'max' => 255],
            [['ip'], 'string', 'max' => 55],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => yii::t('order', 'Order'),
            'amount' => yii::t('order', 'Amount'),
            'description' => yii::t('order', 'Description'),
            'user_id' => yii::t('order', 'User'),
            'date' => yii::t('order', 'Date'),
            'payment_type_id' => yii::t('order', 'Payment type'),
            'ip' => yii::t('order', 'IP'),
        ];
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return mixed
     */
    public function getPayment()
    {
        return $this->hasOne(PaymentType::className(), ['id' => 'payment_type_id']);
    }
}
