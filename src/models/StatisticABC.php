<?php

namespace dvizh\order\models;

use yii;


/**
 * Class StatisticABC
 * @package dvizh\order\models
 *
 * @property string $date_range
 * @property int[] $categories
 * @property float $range_ab
 * @property float $range_bc
 * @property string $parameter
 * @property boolean $with_returned
 */
class StatisticABC extends Element
{
    const PARAMETER_TURNOVER = 'turnover';
    const PARAMETER_PROFIT = 'profit';

    public $date_range;
    public $categories;
    public $range_ab;
    public $range_bc;
    public $parameter;
    public $with_returned;

    public $totalPrice;
    public $abc_temp = 0;

    public function init()
    {
        $this->range_ab = 85;
        $this->range_bc = 15;
        $this->parameter = $this::PARAMETER_TURNOVER;
        $this->with_returned = true;
        $this->categories = null;
    }

    public function rules()
    {
        return [
            [['range_ab', 'range_bc', 'parameter', 'with_returned'], 'required'],
            [['range_ab', 'range_bc'], 'number'],
            ['range_ab', 'default', 'value' => '80'],
            ['range_bc', 'default', 'value' => '10'],
            ['parameter', 'string'],
            [['date_range'], 'safe'],
            [['date_range'], 'trim'],
            ['with_returned', 'integer'],
            ['categories', 'safe']
        ];
    }


    /**
     * @return array
     */
    public function getAllCategories()
    {
        $productCategories['null'] = 'Без категорії';

        $elements = Element::find()->select('item_id')->groupBy('item_id')->all();

        foreach ($elements as $element) {
            $category = $element->product->category;
            if($category){
                $productCategories[$category->id] = $category->name;
            }
        }

        return $productCategories;
    }

    public function getAllParameters()
    {
        return [
            $this::PARAMETER_TURNOVER => 'Оборот',
            $this::PARAMETER_PROFIT => 'Прибуток'
        ];
    }

    public function getTotalPrice()
    {
        $totalPrice = $this->abcProductsQuery()
            ->groupBy(['shop_product.name'])
            ->orderBy('totalPrice DESC')
            ->sum('totalPrice');
        return $totalPrice;
    }

    private function abcProductsQuery()
    {
        $count = (!$this->with_returned)
            ? 'SUM(order_element.count)'
            : '(SUM(order_element.count) - (COALESCE(SUM(order_return_element.count), 0)))';

        $price = ($this->parameter === 'turnover')
            ? 'AVG(order_element.price)'
            : '(AVG(order_element.price) - AVG(order_element.base_price))';

        $query = (new \yii\db\Query())->from('order_element')->select([
            'shop_product.name AS productName',
//            'AVG(order_element.price) AS productPriceAvg',
//            'SUM(order_element.count) AS productCount',
//            'COALESCE(SUM(order_return_element.count),0) AS productReturned',
            '('.$price.' * '.$count.') AS totalPrice'
        ]);

        $query->join('LEFT JOIN', 'order', 'order.id = order_element.order_id');
        $query->join('LEFT JOIN', 'shop_product', 'shop_product.id = order_element.item_id');
        $query->join('LEFT JOIN', 'order_return', 'order_return.order_id = order.id');
        $query->join('LEFT JOIN', 'order_return_element', 'order_return_element.return_id = order_return.id AND order_return_element.item_id=order_element.item_id');

        if (isset($this->categories) && $this->categories[0] !== '') {
            $categories = $this->categories;
            if(in_array('null', $categories)){
                $categories[array_search('null', $categories)] = null;
            }
            if(!empty($categories)){
                $query->andWhere(['in', 'shop_product.category_id', $categories]);
            }
        }

        if (isset($this->date_range) && $this->date_range !== '') {
            $date_explode = explode(" - ", $this->date_range);

            $query->andWhere('DATE_FORMAT(order.date,\'%Y-%m-%d\') >= :dateStart AND DATE_FORMAT(order.date,\'%Y-%m-%d\') <= :dateStop',
                [':dateStart' => trim($date_explode[0]), ':dateStop' => trim($date_explode[1])]
            );
        }

        return $query;
    }

    public function getAbcProductsQuery()
    {
        return $this->abcProductsQuery()
            ->groupBy(['order_element.item_id'])
            ->orderBy('totalPrice DESC');
    }

}
