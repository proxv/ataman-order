<?php

namespace dvizh\order\models;

use Yii;

/**
 * This is the model class for table "cashbox_workshift".
 *
 * @property int $id
 * @property int $cashbox_id
 * @property int $user_id
 * @property string $crete_at
 * @property string|null $update_at
 * @property string|null $closed_at
 * @property float $open_amount
 * @property float $cash_amount
 * @property float $card_amount
 * @property float $close_amount
 * @property boolean $is_closed
 * @property boolean $closed_by_user_id
 * @property float $return_amount
 * @property float $withdrawal_amount
 * @property string $key
 * @property float $deposit_amount
 *
 * @property boolean $is_send_advance
 */
class CashboxWorkshift extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cashbox_workshift';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cashbox_id', 'user_id', 'crete_at', 'open_amount', 'cash_amount', 'card_amount', 'key'], 'required'],
            [['cashbox_id', 'user_id', 'closed_by_user_id'], 'integer'],
            [['crete_at', 'update_at', 'closed_at', 'key'], 'safe'],
            [[
                'open_amount', 'cash_amount', 'card_amount', 'close_amount',
                'return_amount', 'withdrawal_amount', 'deposit_amount'
            ], 'number'],
            ['key', 'unique']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cashbox_id' => yii::t('order', 'Cash Box'),
            'user_id' => yii::t('order', 'Seller'),
            'crete_at' => yii::t('order', 'Open at'),
            'update_at' => yii::t('order', 'Update at'),
            'closed_at' => yii::t('order', 'Closed at'),
            'open_amount' => yii::t('order', 'Cash on cash box when opening a change'),
            'cash_amount' => yii::t('order', 'Paid by cash'),
            'card_amount' => yii::t('order', 'Paid by card'),
            'close_amount' => yii::t('order', 'Cash on cash box when closing a work shift'),
            'is_closed' => yii::t('order', 'Is the work shift closed'),
            'cash' => yii::t('order', 'The amount in cash box'),
            'cashLack' => yii::t('order', 'Lack of cash'),
            'closed_by_user_id' => yii::t('order', 'Closed by user'),
            'return_amount' => yii::t('order', 'Amount of returns'),
            'withdrawal_amount' => yii::t('order', 'Cash withdrawal amount'),
            'key' => yii::t('order', 'Unique key'),
            'deposit_amount' => yii::t('order', 'Cash deposit amount'),
        ];
    }

    /**
     * @return mixed
     */
    public function getCashbox()
    {
        return $this->hasOne(Cashbox::className(), ['id' => 'cashbox_id'])->one();
    }

    /**
     * @return null|mixed
     */
    public function getUser()
    {
        $userModel = yii::$app->getModule('order')->sellerModel;
        if ($userModel && class_exists($userModel)) {
            return $this->hasOne($userModel::className(), ['id' => 'user_id'])->one();
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function hasOpenWorkShift()
    {
        return CashboxWorkshift::find()
            ->where(['cashbox_id' => $this->cashbox_id])
            ->andWhere(['is_closed' => 0])
            ->one();
    }

    /**
     * @param integer $user_id
     * @return bool
     */
    public function canOpenWorkshift(int $user_id)
    {
        $ws = CashboxWorkshift::find()
            ->where(['user_id' => $user_id, 'is_closed' => 0])
            ->one();
        return is_null($ws);
    }

    /**
     * @param $amount
     * @return mixed
     */
    public function plusReturnAmount($amount)
    {
        $this->update_at = date("Y-m-d H:m:s");
        $this->return_amount += $amount;
        return $this->save();
    }

    /**
     * Updates an existing Cashbox WorkShift model.
     *
     * @param int|float $amount
     * @param integer $user
     * @return mixed
     */
    public function setCloseWorkShift($amount = 0, $user, $dateTime = null)
    {
        $this->update_at = $this->closed_at = ($dateTime) ? $dateTime : date("Y-m-d H:m:s");
        $this->is_closed = 1;
        $this->close_amount = $amount;
        $this->closed_by_user_id = $user;

        if($saved = $this->save()){
            Yii::$app->commandBus->handle(new \common\commands\AddToTimelineCommand([
                'category' => 'workshift',
                'event' => 'close',
                'data' => [
                    'id' => $this->id,
                    'cashbox_name' => $this->cashbox->name
                ]
            ]));
        }
        return $saved;
    }

    /**
     * Updates an existing Cashbox WorkShift model.
     * @param string $to card|cash
     * @param integer|float $amount
     * @return boolean
     */
    public function plusAmountTo($to, $amount)
    {
        if ($amount > 0 && !$this->is_closed) {
            $this->update_at = date("Y-m-d H:m:s");
            switch ($to) {
                case 'cash':
                    $this->cash_amount += $amount;
                    break;
                case 'card':
                    $this->card_amount += $amount;
                    break;
                default:
                    return false;
            }

            return $this->save();
        }
        return false;
    }

    /**
     * @param string $to
     * @param integer|float $amount
     * @return false
     */
    public function minusAmountTo($to, $amount)
    {
        if ($amount > 0) {
            $this->update_at = date("Y-m-d H:m:s");
            switch ($to) {
                case 'cash':
                    $this->cash_amount -= $amount;
                    break;
                case 'card':
                    $this->card_amount -= $amount;
                    break;
                default:
                    return false;
            }

            return $this->save();
        }
        return false;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->hasMany(Order::className(), ['workshift_id' => 'id']);
    }

    // Statistic

    /**
     * Getter. Return sum of cash what mus be in cashBox
     * @return float
     */
    public function getCash()
    {
        $totalCashAmount = ($this->open_amount + $this->cash_amount + $this->deposit_amount);
        $value = $totalCashAmount - ($this->return_amount + $this->withdrawal_amount);
        return number_format($value, 2, '.', '');
    }

    /**
     * Getter. Return difference of cash what mus be in cashBox and real
     * @return float
     */
    public function getCashLack()
    {
        $value = $this->close_amount - $this->getCash();
        return number_format($value, 2, '.', '');
    }

    /**
     * @param integer|float $amount
     * @return bool
     */
    public function canMinusAmount($amount)
    {
        $result = $this->getCash() - $amount;
        return $result >= 0;
    }

    /**
     * @param integer|float $amount
     * @return mixed
     */
    public function plusWithdrawalAmount($amount)
    {
        $this->update_at = date("Y-m-d H:m:s");
        $this->withdrawal_amount += $amount;
        return $this->save();
    }

    /**
     * @param integer|float $amount
     * @return mixed
     */
    public function plusDepositAmount($amount)
    {
        $this->update_at = date("Y-m-d H:m:s");
        $this->deposit_amount += $amount;
        return $this->save();
    }

    /**
     * @return CashboxWorkshift
     */
    public function getLastClosedWorkshift()
    {
        return CashboxWorkshift::find()
            ->where(['cashbox_id' => $this->cashbox_id])
            ->andWhere(['is_closed' => 1])
            ->orderBy('update_at DESC')
            ->one();
    }

    public function sendToApiOpenAmount()
    {
        if(!$this->cashbox->is_rro_active){
            return true;
        }

        $department = $this->cashbox->getPrroCashboxDepartment()->andWhere(['is_main' => 1])->one();

        $shift = new \common\modules\openprro\models\WorkShift();
        $shift->department_id = $department->department_id;
        $shift->sum = round((float)$this->open_amount, 2);
        $data = $shift->registerOpenCash();

        $response = \Yii::$app->getModule('openprro')->sentToAPI($shift->getMethod(), $data);
// var_dump($response);
        if ($response['status'] === 'success') {
            $prroWorkshift = new \common\modules\openprro\models\PrroWorkshift();

            $prroWorkshift->workshift_id = $this->id;
            $prroWorkshift->department_id = $department->department_id;
            $prroWorkshift->type = $shift->getMethod();
            $prroWorkshift->online = gettype($response['tax_id']) === 'string' ? 0 : 1;
            $prroWorkshift->tax_id = $response['tax_id'];
            $prroWorkshift->shift_tax_id = $response['shift_tax_id'];
            $prroWorkshift->qr = $response['qr'];

            $this->is_send_advance = 1;
            $this->save();

            return $prroWorkshift->save();
        } else {
            $this->addError('openprro', $response['message']);
            return false;
        }

    }

    public function sendToApiCloseShift($department_id)
    {
        if(!$this->cashbox->is_rro_active){
            return true;
        }

        $prroWorkshifts = $this->getPrroWorkshift()->andWhere(['department_id' => $department_id])->count();
        $departmentOrders = $this->getOrder()->andWhere(['department_id' => $department_id, 'is_fiscalized' => 1])->count();

        if (!$prroWorkshifts && !$departmentOrders) {
            return true;
        }

        if ($this->getPrroWorkshift()->andWhere(['department_id' => $department_id, 'type' => 'close_shift'])->count()) {
            return true;
        }

        $shift = new \common\modules\openprro\models\WorkShift();
        $shift->department_id = $department_id;
        $data = $shift->close();

        $response = \Yii::$app->getModule('openprro')->sentToAPI($shift->getMethod(), $data);
//var_dump($response);
        if ($response['status'] === 'success') {
            $prroWorkshift = new \common\modules\openprro\models\PrroWorkshift();

            $prroWorkshift->workshift_id = $this->id;
            $prroWorkshift->department_id = $department_id;
            $prroWorkshift->type = $shift->getMethod();
            $prroWorkshift->online = gettype($response['close_shift_tax_id']) === 'string' ? 0 : 1;
            $prroWorkshift->tax_id = $response['close_shift_tax_id'];
            $prroWorkshift->shift_tax_id = $response['z_report_tax_id'];;
            $prroWorkshift->qr = '';

            return $prroWorkshift->save(false);
        } else {
            $this->addError('openprro', $response['message']);
            return false;
        }
    }

    public function sendToApiCloseAllShifts()
    {
        $departments = $this->cashbox->prroCashboxDepartment;

        $status = [];

        foreach ($departments as $department) {
            $status[] = $this->sendToApiCloseShift($department->department_id);
        }
        return !in_array(false, $status);
    }

    public function getPrroWorkshift()
    {
        return $this->hasMany(\common\modules\openprro\models\PrroWorkshift::className(), ['workshift_id' => 'id']);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            Yii::$app->commandBus->handle(new \common\commands\AddToTimelineCommand([
                'category' => 'workshift',
                'event' => 'open',
                'data' => [
                    'id' => $this->id,
                    'cashbox_name' => $this->cashbox->name
                ]
            ]));
        } else {

        }
        parent::afterSave($insert, $changedAttributes);
    }

}
