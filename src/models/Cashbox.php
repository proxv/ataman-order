<?php

namespace dvizh\order\models;

use http\Client\Curl\User;
use Yii;

use common\components\keyStorage\KeyStorage;

/**
 * This is the model class for table "cashbox".
 *
 * @property int $id
 * @property string $name
 * @property string|null $description
 * @property string $created_at
 * @property string $updated_at
 * @property integer|null $user_id
 * @property boolean $is_rro_active
 */
class Cashbox extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cashbox';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'created_at', 'updated_at'], 'required'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'description'], 'string', 'max' => 255],
            [['user_id'], 'integer'],
            ['is_rro_active', 'boolean']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => yii::t('order', 'Name'),
            'description' => yii::t('order', 'Description'),
            'created_at' => yii::t('order', 'Created'),
            'updated_at' => yii::t('order', 'Updated'),
            'user_id' => yii::t('order', 'User'),
            'is_rro_active' => 'Активне РРО',
        ];
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        $userModel = yii::$app->getModule('order')->sellerModel;
        if ($userModel && class_exists($userModel)) {
            return $this->hasOne($userModel::className(), ['id' => 'user_id'])->one();
        }
        return null;
    }

    /**
     * @return mixed
     */
    public function getWorkshift()
    {
        return $this->hasMany(CashboxWorkshift::className(), ['cashbox_id' => 'id']);
    }

    /**
     * @return int
     */
    public function getMaxCashboxes()
    {
        $keyStorage = new KeyStorage();
        $key = 'backend.order-max-cashbox';

        if (!$keyStorage->has($key)) {
            return 1;
        }

        return (integer)$keyStorage->get($key);
    }

    /**
     * @return int
     */
    public function getCashboxesCount()
    {
        return (integer)(Cashbox::find()->count());
    }

    /**
     * @return string|float
     */
    public function getCurrentCashAmount()
    {
        $workShift = new CashboxWorkshift();
        $workShift->cashbox_id = $this->id;

        if ($openWorkShift = $workShift->hasOpenWorkShift()) {
            return $openWorkShift->cash;
        }

        return $workShift->lastClosedWorkshift->close_amount;
    }

    /**
     * @return string|float
     */
    public function getCurrentCashAmountLuck()
    {
        $workShift = new CashboxWorkshift();
        $workShift->cashbox_id = $this->id;

        if ($openWorkShift = $workShift->hasOpenWorkShift()) {
            return 0;
        }

        if (!$workShift->lastClosedWorkshift) {
            return 0;
        }

        return $workShift->lastClosedWorkshift->getCashLack();
    }

    /**
     * @return CashboxWorkshift
     */
    public function getOpenWorkshift()
    {
        return $this->getWorkshift()->where(['is_closed' => 0])->one();
    }

    /**
     * @return CashboxWorkshift
     */
    public function getLastClosedWorkshift()
    {
        return $this->getWorkshift()->orderBy('closed_at DESC')->limit(1)->one();
    }

    public function getPrroCashboxDepartment(){
        return $this->hasMany(\common\modules\openprro\models\PrroCashboxDepartment::className(), ['cashbox_id' => 'id']);
    }

    public function beforeSave($insert)
    {
        if ($insert) {

        }else{
            $hasOpenWorkShift = isset($this->openWorkshift);
            $isChangedAttribute_is_rro_active = $this->getOldAttribute('is_rro_active') != $this->is_rro_active;

            if($hasOpenWorkShift && $isChangedAttribute_is_rro_active){
                return false;
//                var_dump('Has Opened WorkShift!!\n You can`t edit RRO Active attribute');
            }
        }

        return parent::beforeSave($insert);
    }
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            Yii::$app->commandBus->handle(new \common\commands\AddToTimelineCommand([
                'category' => 'cashbox',
                'event' => 'create',
                'data' => [
                    'id' => $this->id,
                    'name' => $this->name,
                ]
            ]));
        } else {

        }
        parent::afterSave($insert, $changedAttributes);
    }
    // Delete
    public function beforeDelete(){
        if(isset($this->openWorkshift)){
            throw new \yii\web\ForbiddenHttpException('Неможливо видалити касу. Існує відкрита зміна');
        }

        if(isset($this->prroCashboxDepartment)){
            throw new \yii\web\ForbiddenHttpException('Неможливо видалити касу. До каси прив`язаний департамент');
        }
    }
}
