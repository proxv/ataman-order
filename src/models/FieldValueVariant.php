<?php

namespace dvizh\order\models;

use yii;

/**
 * Class FieldValueVariant
 * @package dvizh\order\models
 *
 * @property int $id
 * @property int $field_id
 * @property string $value
 */
class FieldValueVariant extends \yii\db\ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_field_value_variant}}';
    }

    /**
     * @return array[]
     */
    public function rules()
    {
        return [
            [['field_id'], 'required'],
            [['value'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => yii::t('order', 'ID'),
            'field_id' => yii::t('order', 'Field'),
            'value' => yii::t('order', 'Value'),
        ];
    }

    /**
     * @param $id
     * @param $name
     * @param $value
     */
    public static function editField($id, $name, $value)
    {
        $setting = FieldValueVariant::findOne($id);
        $setting->$name = $value;
        $setting->save();
    }
}
