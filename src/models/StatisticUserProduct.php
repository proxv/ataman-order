<?php

namespace dvizh\order\models;

use Cassandra\Set;
use yii;


/**
 * Class StatisticABC
 * @package dvizh\order\models
 *
 * @property string $date_range
 * @property int[] $products
 * @property int $user
 *
 * @property string $dateStart
 * @property string $dateStop
 */
class StatisticUserProduct extends Element
{
    public $date_range;
    public $products;
    public $user;

    private $dateStart;
    private $dateStop;


    public function init()
    {
        $this->products = null;

        $this->dateStart = null;
        $this->dateStop = null;
    }

    public function rules()
    {
        return [
            [['user', 'products', 'date_range'], 'required'],
            ['user', 'integer'],
            [['date_range', 'products'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'date_range' => 'Вибірка за дату',
            'products' => 'Продукти',
            'user' => 'Продавець',
        ];
    }


    public function getUserProductQuery()
    {
        $query = (new \yii\db\Query())
            ->from('order_element')
            ->select([
                'order.date',
                'shop_product.id AS productId',
                'shop_product.name AS productName',
                'SUM(order_element.count) AS saleAmount',
                'SUM(order_element.count * order_element.price) AS turnover',
                'SUM(order_element.count * (order_element.price - order_element.base_price)) AS profit',
            ]);

        $query->join('LEFT JOIN', 'order', 'order.id = order_element.order_id');
        $query->join('LEFT JOIN', 'shop_product', 'shop_product.id = order_element.item_id');

        $products = $this->products;
        if (in_array('null', $products)) {
            $products[array_search('null', $products)] = null;
        }
        if (!empty($products)) {
            $query->andWhere(['in', 'shop_product.id', $products]);
        }

        if (isset($this->date_range) && $this->date_range !== '') {
            $date_explode = explode(" - ", $this->date_range);

            $query->andWhere('DATE_FORMAT(order.date,\'%Y-%m-%d\') >= :dateStart AND DATE_FORMAT(order.date,\'%Y-%m-%d\') <= :dateStop',
                [':dateStart' => trim($date_explode[0]), ':dateStop' => trim($date_explode[1])]
            );
        }

        $query->andWhere(['order.user_id' => $this->user]);

        $query->groupBy(['order_element.item_id']);

        return $query;
    }

    private function setStartStopDates()
    {
        if (isset($this->date_range) && $this->date_range !== '') {
            $date_explode = explode(" - ", $this->date_range);

            $this->dateStart = trim($date_explode[0]);
            $this->dateStop = trim($date_explode[1]);
        }
    }

    public function getUserProductQueryForChart()
    {
        $this->setStartStopDates();

        $query = (new \yii\db\Query())
            ->from('order_element')
            ->select([
                'shop_product.name AS productName',

                'order.date AS saleDate',

                'order_element.item_id AS productId',
                'SUM(order_element.count) AS productCount',
                'SUM(order_element.price * order_element.count) AS productTurnover',
                'SUM((order_element.price - order_element.base_price) * order_element.count) AS productProfit',

            ])
            ->join('LEFT JOIN', 'order', 'order.id = order_element.order_id')
            ->join('LEFT JOIN', 'shop_product', 'shop_product.id = order_element.item_id')
            ->where(['in', 'order_element.item_id', $this->products])
            ->andWhere('DATE_FORMAT(order.date,\'%Y-%m-%d\') >= :dateStart AND DATE_FORMAT(order.date,\'%Y-%m-%d\') <= :dateStop',
                [':dateStart' => $this->dateStart, ':dateStop' => $this->dateStop]
            )
            ->andWhere(['order.user_id' => $this->user])
            ->groupBy(['saleDate', 'productId']);

        return $query;
    }

    public function getDateRangeForChart()
    {
        $elements = $this->getUserProductQueryForChart()->all();
        $dates = [];
        foreach ($elements as $element) {
            $date = $element['saleDate'];
            if (!in_array($date, $dates)) {
                $dates[] = $date;
            }
        }
        return $dates;
    }

    public function getSalesQuantityForChart()
    {
        $elements = $this->getUserProductQueryForChart()->all();
        $dates = [];
        foreach ($elements as $element) {
            $date = $element['saleDate'];
            $product = $element['productId'];

            $dates[$date][$product] = [
                'count' => $element['productCount'],
                'turnover' => $element['productTurnover'],
                'profit' => $element['productProfit'],
            ];
        }
        return $dates;
    }

    public function getTurnoverForChart()
    {
        return [];
    }

    public function getProfitForChart()
    {
        return [];
    }
}
