<?php

namespace dvizh\order\models;

use common\modules\openprro\models\Totals;
use common\modules\openprro\models\Excise;
use common\modules\openprro\models\Check;
use common\modules\openprro\models\Pay;
use common\modules\openprro\models\PaySystem;
use common\modules\openprro\models\PrroOrder;
use common\modules\openprro\models\Realization;
use common\modules\openprro\models\Taxse;
use yii;
use dvizh\order\models\tools\OrderQuery;
use dvizh\order\interfaces\Order as OrderInterface;

use common\components\keyStorage\KeyStorage;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property string $client_name
 * @property string $phone
 * @property string $email
 * @property string $promocode
 * @property int $count
 * @property float $cost
 * @property float $base_cost
 * @property int $payment_type_id
 * @property int $shipping_type_id
 *
 * @property string $delivery_time_date
 * @property int $delivery_time_hour
 * @property int $delivery_time_min
 * @property string $delivery_type
 * @property string $status
 * @property string $order_info
 * @property string $time
 * @property int $user_id
 * @property int $seller_user_id
 * @property string $date
 * @property string $payment
 * @property int $timestamp
 * @property string $comment
 * @property string $address
 * @property int $is_assigment
 * @property int $organization_id
 * @property int $is_deleted
 *
 * @property int $workshift_id
 * @property string $cashbox_order_key
 * @property float $cash_amount
 *
 * @property boolean $is_fiscalized
 * @property float $provided
 * @property float $remains
 * @property float $sum
 * @property float $sum_round
 * @property int $department_id
 *
 *
 * @property PaymentType $paymentType
 */
class Order extends \yii\db\ActiveRecord implements OrderInterface
{
    /**
     * @var mixed
     */
    public $sessionId;

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order}}';
    }

    /**
     * @return $this
     */
    public static function find()
    {
        $query = new OrderQuery(get_called_class());

        return $query->with('elementsRelation');
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            //    [['client_name'], 'required'],
            //    [['phone', 'email'], 'emailAndPhoneValidation', 'skipOnEmpty' => false],
            [['date', 'payment', 'comment', 'delivery_time', 'address'], 'string'],
            ['status', 'in', 'range' => array_keys(yii::$app->getModule('order')->orderStatuses)],
            ['email', 'email'],
            [['phone'], 'udokmeci\yii2PhoneValidator\PhoneValidator', 'country' => yii::$app->getModule('order')->countryCode],
            [[
                'status', 'date', 'payment', 'client_name', 'phone', 'email',
                'comment', 'delivery_time_date', 'delivery_type', 'address', 'cashbox_order_key'
            ], 'safe'],
            [[
                'seller_user_id', 'organization_id', 'shipping_type_id',
                'payment_type_id', 'delivery_time_hour', 'delivery_time_min',
                'is_deleted', 'is_assigment', 'workshift_id', 'department_id'
            ], 'integer'],
            [['cash_amount', 'base_cost', 'cost'], 'number'],
            ['cashbox_order_key', 'unique'],

            ['is_fiscalized', 'integer'],
            [['provided', 'remains', 'sum', 'sum_round'], 'number']
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function emailAndPhoneValidation($attribute, $params)
    {
        if (empty($this->phone) && empty($this->email)) {
            $this->addError($attribute, yii::t('order', 'Phone or email is required'));
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => yii::t('order', 'ID'),
            'client_name' => yii::t('order', 'Касир'),
            'shipping_type_id' => yii::t('order', 'Delivery'),
            'delivery_time_date' => yii::t('order', 'Delivery date'),
            'delivery_time_hour' => yii::t('order', 'Delivery hour'),
            'delivery_time_min' => yii::t('order', 'Delivery minute'),
            'delivery_type' => yii::t('order', 'Delivery time'),
            'payment_type_id' => yii::t('order', 'Тип оплати'),
            'comment' => yii::t('order', 'Comment'),
            'phone' => yii::t('order', 'Phone'),
            'promocode' => yii::t('order', 'Promocode'),
            'date' => yii::t('order', 'Date'),
            'email' => yii::t('order', 'Email'),
            'payment' => yii::t('order', 'Paid'),
            'status' => yii::t('order', 'Status'),
            'time' => yii::t('order', 'Time'),
            'user_id' => yii::t('order', 'User ID'),
            'count' => yii::t('order', 'Count'),
            'cost' => yii::t('order', 'Cost'),
            'base_cost' => yii::t('order', 'Base cost'),
            'seller_user_id' => yii::t('order', 'Seller'),
            'address' => yii::t('order', 'Address'),
            'organization_id' => yii::t('order', 'organization'),
            'is_assigment' => yii::t('order', 'Assigment'),
            'is_deleted' => yii::t('order', 'Deleted'),
            'workshift_id' => yii::t('order', 'Work Shift'),
            'cashbox_order_key' => yii::t('order', 'Cashbox order key'),
            'cash_amount' => 'Кількість готівки при суміжній оплаті',
            'is_fiscalized' => 'Фіскалізований',
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            'customer' => ['promocode', 'comment', 'client_name', 'shipping_type_id', 'payment_type_id', 'phone', 'email', 'delivery_time_date', 'delivery_time_hour', 'delivery_time_min', 'delivery_type', 'address'],
            'admin' => array_keys($this->attributeLabels()),
            'default' => array_keys($this->attributeLabels()),
            'return' => array_keys($this->attributeLabels()),
        ];
    }

    /**
     * @param $deleted
     * @return $this
     */
    public function setDeleted($deleted)
    {
        $this->is_deleted = $deleted;

        return $this;
    }

    /**
     * @param string $status
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return mixed
     */
    public function cancel()
    {
        $this->is_deleted = 1;

        return $this->save(false);
    }

    /**
     * @return mixed
     */
    public function restore()
    {
        $this->is_deleted = 0;

        return $this->save(false);
    }

    /**
     * @return mixed
     */
    public function saveData()
    {
        return $this->save(false);
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getCost()
    {
        return $this->cost;
    }

    /**
     * @return float
     */
    public function getBaseCost()
    {
        return $this->base_cost;
    }

    /**
     * @param string $status
     * @return $this
     */
    function setPaymentStatus($status)
    {
        $this->payment = $status;

        return $this;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return floatVal($this->hasMany(Element::className(), ['order_id' => 'id'])->sum('price*count'));
    }

    /**
     * @return string
     */
    public function getTotalFormatted()
    {
        $priceFormat = yii::$app->getModule('order')->priceFormat;
        $price = number_format($this->getPrice(), $priceFormat[0], $priceFormat[1], $priceFormat[2]);
        $currency = yii::$app->getModule('order')->currency;
        if (yii::$app->getModule('order')->currencyPosition == 'after') {
            return "$price $currency";
        } else {
            return "$currency $price";
        }
    }

    /**
     * @param null|int $fieldId
     * @return null|string
     */
    public function getField($fieldId = null)
    {
        if ($field = FieldValue::find()->where(['order_id' => $this->id, 'field_id' => $fieldId])->one()) {
            return $field->value;
        }

        return null;
    }

    /**
     * @param int $fieldId
     * @param string $fieldValue
     * @return mixed
     */
    public function setField($fieldId, $fieldValue)
    {
        if ($field = FieldValue::find()->where(['order_id' => $this->id, 'field_id' => $fieldId])->one()) {
            $field->value = $fieldValue;
            return $field->save();
        }
        $field = new FieldValue();

        $field->field_id = $fieldId;
        $field->value = $fieldValue;
        $field->order_id = $this->id;
        return $field->save();
    }

    /**
     * @return mixed
     */
    public function getPaymentType()
    {
        return $this->hasOne(PaymentType::className(), ['id' => 'payment_type_id']);
    }

    /**
     * @return null|mixed
     */
    public function getUser()
    {
        $userModel = yii::$app->getModule('order')->userModel;
        if ($userModel && class_exists($userModel)) {
            return $this->hasOne($userModel::className(), ['id' => 'seller_user_id']);
        }

        return null;
    }

    /**
     * @return null|mixed
     */
    public function getClient()
    {
        return $this->getUser();
    }

    /**
     * @return null|mixed
     */
    public function getSeller()
    {
        $userModel = yii::$app->getModule('order')->sellerModel;
        if ($userModel && class_exists($userModel)) {
            return $this->hasOne($userModel::className(), ['id' => 'seller_user_id']);
        }

        return null;
    }

    /**
     * @return mixed
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['order_id' => 'id']);
    }

    /**
     * @return mixed
     */
    public function getShipping()
    {
        return $this->hasOne(ShippingType::className(), ['id' => 'shipping_type_id']);
    }

    /**
     * @return int
     */
    public function getCount()
    {
        return intval($this->hasMany(Element::className(), ['order_id' => 'id'])->sum('count'));
    }

    /**
     * @return mixed
     */
    public function getFields()
    {
        return $this->hasMany(FieldValue::className(), ['order_id' => 'id']);
    }

    /**
     * @return mixed
     */
    public function getAllFields()
    {
        return Field::find()->all();
    }

    /**
     * @return mixed
     */
    public function getElementsRelation()
    {
        return $this->hasMany(Element::className(), ['order_id' => 'id'])->where('({{%order_element}}.is_deleted IS NULL OR {{%order_element}}.is_deleted != 1)');
    }

    /**
     * @param bool $withModel
     * @return array
     */
    public function getElements($withModel = true)
    {
        $returnModels = [];
        $elements = $this->getElementsRelation()->all();
        foreach ($elements as $element) {
            if (is_string($element->model) && $withModel && class_exists($element->model)) {
                $model = '\\' . $element->model;
                $productModel = new $model();
                if ($productModel = $productModel::findOne($element->item_id)) {
                    $element->model = $productModel;
                }
            }
            $returnModels[$element->id] = $element;
        }

        return $returnModels;
    }

    /**
     * @param int $id
     * @return Element
     */
    public function getElementById($id)
    {
        return $this->hasMany(Element::className(), ['order_id' => 'id'])->andWhere(['id' => $id])->one();
    }

    /**
     * @param string $modelName
     * @return bool
     */
    public function haveModelElements($modelName)
    {
        if (
        $this->hasMany(Element::className(), ['order_id' => 'id'])
            ->andWhere(['model' => $modelName])
            ->one()
        ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @param $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        // if order field workshift_id is null, you can not save order
        if (is_null($this->workshift_id)) {
            return false;
        }
        // workshift with id dos not exist, you can not save order
        if (!$this->existWorkshift()) {
            return false;
        }
        // workshift with id dos closed, you can not save order
        if ($this->getScenario() !== 'return') {
            if ($this->isClosedWorkshift()) {
                return false;
            }
        }

        if (!isset($this->timestamp)) {
            $this->timestamp = strtotime($this->date);
        }
        if (!isset($this->time)) {
            $this->time = date_format($this->date, 'H:i:s');
        }
        if (!isset($this->email)) {
            $this->email = '';
        }

        if ($this->getScenario() === 'return') {
//            if ($this->status === 'new') {
//                $work_shift = CashboxWorkshift::findOne($this->workshift_id);
//                if (!$work_shift) {
//                    return false;
//                }
//                $payment_type = $this->getPaymentTypeForWorkshift();
//// card or cash
//                $workShiftPaymentTypeAmount = $work_shift[$payment_type . '_amount'];
//                $minusAmountForPayment = $workShiftPaymentTypeAmount - $this->getCost();
//
//                $work_shift->minusAmountTo($payment_type, $minusAmountForPayment);
//            }

        }
        return parent::beforeSave($insert);
    }

    /**
     * @param $insert
     * @param $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            Yii::$app->commandBus->handle(new \common\commands\AddToTimelineCommand([
                'category' => 'order',
                'event' => 'create',
                'data' => [
                    'id' => $this->id,
                    'count' => $this->count,
                    'cost' => $this->cost,
                ]
            ]));
        } else {

        }
        parent::afterSave($insert, $changedAttributes);

        if ($this->getScenario() !== 'return') {
            $this->updateWorkshift();
        }
    }

    /**
     * @return bool
     */
    public function isRelatedPayment()
    {
        return $this->paymentType->slug === 'cash_and_card';
    }

    /**
     * @return float
     */
    public function getRelatedCashAmount()
    {
        return $this->cash_amount;
    }

    /**
     * @return float
     */
    public function getRelatedCardAmount()
    {
        return $this->getCost() - $this->cash_amount;
    }

    /**
     * @return bool
     */
    private function updateWorkshift()
    {
        if ($this->status === 'new') {
            $work_shift = CashboxWorkshift::findOne($this->workshift_id);

            if (!$work_shift) {
                return false;
            }
            if ($this->isRelatedPayment()) {
                $work_shift->plusAmountTo('cash', $this->getRelatedCashAmount());
                $work_shift->plusAmountTo('card', $this->getRelatedCardAmount());
            } else {
                $payment_type = $this->getPaymentTypeForWorkshift();

                $work_shift->plusAmountTo($payment_type, $this->getCost());
            }
//            return true;
        }
    }

    /**
     * @return false|string
     */
    public function getPaymentTypeForWorkshift()
    {
        switch ($this->paymentType->id) {
            case 1:
                return 'cash';
            case 2:
                return 'card';
            default:
                return false;
        }
    }

    /**
     * @return mixed
     */
    public function getWorkshift()
    {
        return $this->hasOne(CashboxWorkshift::className(), ['id' => 'workshift_id']);
    }

    /**
     * @return bool
     */
    public function existWorkshift()
    {
        $ws = $this->getWorkshift()->count();
        return !!$ws;
    }

    /**
     * @return bool
     */
    public function isClosedWorkshift()
    {
        $ws = $this->getWorkshift()->one();
        return !!$ws->is_closed;
    }

    /**
     * @return bool
     */
    public function sendToApi()
    {
        if(!$this->workshift->cashbox->is_rro_active){
            return true;
        }
        $keyStorage = new KeyStorage();
        $pdv_is_active = $keyStorage->get('shop.pdv_is_active');

        $departmentId = $this->department_id;
        $orderElements = $this->getElementsRelation()->all();

        $check = new Check();
        $check->department_id = $departmentId;

        $taxeSum = [];
        foreach ($orderElements as $orderElement) {
            $product = $orderElement->product;
            $realization = new Realization();

            $realization->barcode = (integer)$product->barcode;
            $realization->name = $product->name;
            $realization->unit_num = $product->short_text;

            $realization->amount = round((float)$orderElement->count, 3);
            $realization->price = round((float)$orderElement->price, 2);
            $realization->cost = round((float)($realization->amount * $realization->price), 2);

            if ($pdv_is_active) {
                $realization->letters = 'А';
            }
            $taxeSum['А'] += $realization->cost;

            if ($product->is_excise && $pdv_is_active) {
                $realization->letters .= 'М';
                $taxeSum['М'] += $realization->cost;

                $excise = new Excise($orderElement->excise_barcode);
                $realization->excise_labels[] = $excise->create();
            }

            $check->reals[] = $realization->create();
        }

        if ($pdv_is_active) {
            foreach ($taxeSum as $key => $value) {
                $taxe = new Taxse($key, $value);
                $check->taxes[] = $taxe->create();
            }
        }

        $pay = new Pay();

        $pay->provided = $this->provided;
        $pay->remails = $this->remains;

        if ($this->payment_type_id === 1) {
            $pay->sum = round((float)$taxeSum['А'], 1);
            $check->pays[] = $pay->getCashPay();
        } else if ($this->payment_type_id === 2) {
            $pay->sum = round((float)$taxeSum['А'], 2);
            // If Have info from pay system
            if (isset($this->orderPaySystem->auth_cd)) {
                $paySystem = new PaySystem();
                $paySystem->tax_num = $this->orderPaySystem->tax_num;
                $paySystem->name = $this->orderPaySystem->name;
                $paySystem->acquire_pn = $this->orderPaySystem->acquire_pn;
                $paySystem->acquire_nm = $this->orderPaySystem->acquire_nm;
                $paySystem->acquire_trans_id = $this->orderPaySystem->acquire_trans_id;
                $paySystem->pos_trans_date = $this->orderPaySystem->pos_trans_date;
                $paySystem->pos_trans_num = $this->orderPaySystem->pos_trans_num;
                $paySystem->device_id = $this->orderPaySystem->device_id;
                $paySystem->epz_details = $this->orderPaySystem->epz_details;
                $paySystem->auth_cd = $this->orderPaySystem->auth_cd;
                $paySystem->sum = $this->orderPaySystem->sum;
                $paySystem->commission = $this->orderPaySystem->commission;

                $pay->pay_sys[] = $paySystem->getPaySystem();
            }

            $check->pays[] = $pay->getCardPay();
        }
        $totals = new Totals();
        $totals->no_rnd_sum = (float)$taxeSum['А'];
        $totals->sum = $pay->sum;
        $totals->rnd_sum = round($totals->no_rnd_sum - $totals->sum, 2);

        $check->totals = $totals->create();

        $data = $check->create();

        $response = \Yii::$app->getModule('openprro')->sentToAPI($check->getMethod(), $data);

//var_dump(($data));
//var_dump($response);
        if ($response['status'] === 'success') {
            $prroOrder = new PrroOrder();
            $prroOrder->order_id = $this->id;
            $prroOrder->online = gettype($response['tax_id']) === 'string' ? 0 : 1;
            $prroOrder->shift_tax_id = $response['shift_tax_id'];
            $prroOrder->tax_id = $response['tax_id'];
            $prroOrder->qr = $response['qr'];

            return $prroOrder->save();
//            return true;
        } else {
            $this->addError('openprro', $response['message']);
            return false;
        }

//        return $orderElements;
    }

    public function getFiscalized()
    {
        return $this->hasOne(PrroOrder::className(), ['order_id' => 'id']);
    }

    public function getOrderPaySystem()
    {
        return $this->hasOne(OrderPaySystem::className(), ['order_id' => 'id']);
    }

    public function getPrroCashboxDepartment()
    {
        return $this->hasOne(\common\modules\openprro\models\PrroCashboxDepartment::className(), ['department_id' => $this->department_id]);
    }
}
