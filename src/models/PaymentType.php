<?php

namespace dvizh\order\models;

use yii;

/**
 * Class PaymentType
 * @package dvizh\order\models
 *
 * @property int $id
 * @property string $slug
 * @property string $name
 * @property string $widget
 * @property int $order
 */
class PaymentType extends \yii\db\ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_payment_type}}';
    }

    /**
     * @return array[]
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['order'], 'integer'],
            [['widget'], 'validateWidget'],
        ];
    }

    /**
     * @param $attribute
     * @param $param
     * @return bool
     */
    public function validateWidget($attribute, $param)
    {
        $widget = $this->widget;

        if (trim($widget) == '') {
            return true;
        }

        if (class_exists($widget)) {
            $widgetEx = new $widget();
            if (!$widgetEx instanceof \yii\base\Widget) {
                $this->addError($attribute, 'Widget type error');
            }
        } else {
            $this->addError($attribute, 'Widget not exists');
        }

        return true;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => yii::t('order', 'Name'),
            'order' => yii::t('order', 'Sort'),
            'widget' => yii::t('order', 'Widget'),
        ];
    }
}
