<?php

namespace dvizh\order\models\tools;

use dvizh\order\models\ElementRemoved;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * ElementRemovedSearch represents the model behind the search form about `dvizh\order\models\ElementRemoved`.
 */
class ElementRemovedSearch extends ElementRemoved
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date'], 'safe'],
            [['workshift_id', 'user_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ElementRemoved::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ],
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'user_id' => $this->user_id,
            'workshift_id' => $this->workshift_id,
        ]);

        return $dataProvider;
    }
}
