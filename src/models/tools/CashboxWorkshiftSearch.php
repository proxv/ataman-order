<?php

namespace dvizh\order\models\tools;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use dvizh\order\models\CashboxWorkshift;

/**
 * CashboxWorkshiftSearch represents the model behind the search form about `dvizh\order\models\CashboxWorkshift`.
 */
class CashboxWorkshiftSearch extends CashboxWorkshift
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cashbox_id', 'user_id', 'is_closed'], 'integer'],
            [['crete_at', 'update_at', 'closed_at'], 'safe'],
            [['open_amount', 'cash_amount', 'card_amount', 'close_amount'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CashboxWorkshift::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'cashbox_id' => $this->cashbox_id,
            'user_id' => $this->user_id,
//            'crete_at' => $this->crete_at,
//            'update_at' => $this->update_at,
//            'closed_at' => $this->closed_at,
            'open_amount' => $this->open_amount,
            'cash_amount' => $this->cash_amount,
            'card_amount' => $this->card_amount,
            'close_amount' => $this->close_amount,
            'is_closed' => $this->is_closed,
        ]);
        $query->andFilterWhere(['like', 'crete_at', $this->crete_at]);
        $query->andFilterWhere(['like', 'update_at', $this->update_at]);
        $query->andFilterWhere(['like', 'closed_at', $this->closed_at]);

        return $dataProvider;
    }
}
