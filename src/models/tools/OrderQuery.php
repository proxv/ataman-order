<?php

namespace dvizh\order\models\tools;

use yii;

/**
 * Class OrderQuery
 * @package dvizh\order\models\tools
 */
class OrderQuery extends \yii\db\ActiveQuery
{
    /**
     * @return false
     */
    public function my()
    {
        $userId = yii::$app->user->id;

        if (!$userId) {
            return false;
        }

        return $this->andWhere(['user_id' => $userId]);
    }

    /**
     * @return mixed
     */
    public function unpayment()
    {
        return parent::andWhere(['payment' => 'no']);
    }

    /**
     * @return mixed
     */
    public function byHour()
    {
        return parent::andWhere('timestamp>:time', [':time' => time() - 3600]);
    }
}
