<?php

namespace dvizh\order\models\tools;

use yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use dvizh\order\models\Order;
use dvizh\order\models\FieldValue;

/**
 * Class OrderSearch
 * @package dvizh\order\models\tools
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'workshift_id', 'user_id',
                'is_deleted', 'shipping_type_id',
                'payment_type_id', 'seller_user_id',
                'is_fiscalized'
            ], 'integer'],
            [['payment', 'client_name', 'phone', 'email', 'status', 'time', 'date', 'promocode'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        $query->joinWith('elementsRelation')->groupBy('{{%order}}.id');

        if ($elementTypes = yii::$app->request->get('element_types')) {
            $query->andFilterWhere(['{{%order_element}}.model' => $elementTypes])->groupBy('{{%order}}.id');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'date' => SORT_DESC
                ]
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'user_id' => $this->user_id,
            'payment' => $this->payment,
            'status' => $this->status,
            'promocode' => $this->promocode,
            'seller_user_id' => $this->seller_user_id,
            'workshift_id' => $this->workshift_id,
            'is_fiscalized' => $this->is_fiscalized,
        ]);

        if (isset($this->id)) {
//            $query->andWhere(['{{%order}}.id' => $this->id]);

            $query->andWhere(['like', '{{%order}}.id', $this->id]);
        }

        if (isset($this->is_deleted)) {
            $query->andWhere(['{{%order}}.is_deleted' => $this->is_deleted]);
        }

        $query->andFilterWhere(['like', 'client_name', $this->client_name])
            ->andFilterWhere(['like', 'shipping_type_id', $this->shipping_type_id])
            ->andFilterWhere(['like', 'payment_type_id', $this->payment_type_id])
            ->andFilterWhere(['like', 'phone', $this->phone])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'date', $this->date])
//                ->andFilterWhere(['like', 'workshift_id', $this->workshift_id])
            ->andFilterWhere(['like', 'time', $this->time]);

        return $dataProvider;
    }
}
