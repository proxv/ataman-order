<?php

namespace dvizh\order\models\tools;

use dvizh\order\models\OrderReturnElement;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * OrderReturnSearch represents the model behind the search form about `dvizh\order\models\OrderReturnElement`.
 */
class OrderReturnElementSearch extends OrderReturnElement
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'return_id'], 'integer'],
            [['count'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params, $where = null)
    {
        $query = OrderReturnElement::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if (!($this->load($params) && $this->validate())) {
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'return_id' => $this->return_id,
            'item_id' => $this->item_id,
            'count' => $this->count,
        ]);

        return $dataProvider;
    }
}
