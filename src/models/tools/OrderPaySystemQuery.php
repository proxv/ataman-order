<?php

namespace dvizh\order\models\tools;

/**
 * This is the ActiveQuery class for [[OrderPaySystem]].
 *
 * @see OrderPaySystem
 */
class OrderPaySystemQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return OrderPaySystem[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return OrderPaySystem|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
