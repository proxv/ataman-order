<?php

namespace dvizh\order\models\tools;

use yii;

/**
 * Class OrderReturnQuery
 * @package dvizh\order\models\tools
 */
class OrderReturnQuery extends \yii\db\ActiveQuery
{
    /**
     * @return false|mixed
     */
    public function my()
    {
        $userId = yii::$app->user->id;

        if (!$userId) {
            return false;
        }

        return $this->andWhere(['user_id' => $userId]);
    }

    /**
     * @return mixed
     */
    public function byHour()
    {
        return parent::andWhere('timestamp>:time', [':time' => time() - 3600]);
    }
}
