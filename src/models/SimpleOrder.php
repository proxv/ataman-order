<?php

namespace dvizh\order\models;

use yii;

/**
 * Class SimpleOrder
 * @package dvizh\order\models
 */
class SimpleOrder extends \dvizh\order\models\Order
{

    /**
     * @return array[]
     */
    public function rules()
    {
        return [
            [['phone', 'email'], 'string', 'skipOnEmpty' => true],
        ];
    }

    /**
     * @return mixed
     */
    public function beforeValidate()
    {
        $this->client_name = "";
        $this->email = "";
        $this->phone = "";
        $this->time = time();

        return parent::beforeValidate();
    }
}
