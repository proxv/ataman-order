<?php

namespace dvizh\order\models;

use yii;

/**
 * Class ShippingType
 * @package dvizh\order\models
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property float $cost
 * @property float $free_cost_from
 * @property int $order
 */
class ShippingType extends \yii\db\ActiveRecord
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_shipping_type}}';
    }

    /**
     * @return array[]
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['order'], 'integer'],
            [['cost', 'free_cost_from'], 'double'],
            [['description'], 'string'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'name' => yii::t('order', 'Name'),
            'order' => yii::t('order', 'Sort'),
            'cost' => yii::t('order', 'Cost'),
            'free_cost_from' => yii::t('order', 'Free cost from'),
        ];
    }
}
