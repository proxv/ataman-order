<?php

namespace dvizh\order\models;

use Yii;

/**
 * This is the model class for table "cashbox_deposit_cash".
 *
 * @property int $id
 * @property string $date
 * @property int $workshift_id
 * @property float $amount
 * @property string|null $description
 *
 * @property boolean $is_fiscalized
 */
class DepositCash extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cashbox_deposit_cash';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'workshift_id'], 'required'],
            [['date', 'is_fiscalized'], 'safe'],
            [['workshift_id'], 'integer'],
            [['amount'], 'number'],
            [['description'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Date',
            'workshift_id' => 'Workshift ID',
            'amount' => 'Amount',
            'description' => 'Description',
            'is_fiscalized' => 'Фіскальний',
        ];
    }

    /**
     * @return mixed
     */
    public function getWorkshift()
    {
        return $this->hasOne(CashboxWorkshift::className(), ['id' => 'workshift_id']);
    }

    /**
     * @return mixed
     */
    public function getCashbox()
    {
        $workshift = $this->getWorkshift()->one();
        return $workshift->getCashbox();
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        $workshift = $this->getWorkshift()->one();
        return $workshift->getUser();
    }

    /**
     * @param $insert
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function beforeSave($insert)
    {
        $workshift = $this->getWorkshift()->one();
        if (!$workshift) {
            throw new \yii\web\ForbiddenHttpException('Work shift dos not exist');
        }
        if ($workshift->is_closed === 1) {
            throw new \yii\web\ForbiddenHttpException('Work shift is closed');
        }

        if (!$workshift->plusDepositAmount($this->amount)) {
            throw new \yii\web\ForbiddenHttpException('Can`t plus amount to Work shift');
        }

        // Your code hear
        return parent::beforeSave($insert);
    }


    public function sendToApiDepositeCash(){
        if(!$this->workshift->cashbox->is_rro_active){
            return true;
        }
        // if is_fiscalized === false OR is_fiscalized !== null
        // Dont send to fiskal server
        if(isset($this->is_fiscalized) && !$this->is_fiscalized){
            return true;
        }
        $department = $this->workshift->cashbox->getPrroCashboxDepartment()->andWhere(['is_main' => 1])->one();

        $shift = new \common\modules\openprro\models\WorkShift();
        $shift->department_id = $department->department_id;
        $shift->sum = round((float)$this->amount, 2);
        $data = $shift->addCash();

        $response = \Yii::$app->getModule('openprro')->sentToAPI($shift->getMethod(), $data);
//var_dump($response);
        if ($response['status'] === 'success') {
            $prroWorkshift = new \common\modules\openprro\models\PrroWorkshift();

            $prroWorkshift->workshift_id = $this->workshift->id;
            $prroWorkshift->department_id = $department->department_id;
            $prroWorkshift->type = $shift->getMethod();
            $prroWorkshift->online = gettype($response['tax_id']) === 'string' ? 0 : 1;
            $prroWorkshift->tax_id = $response['tax_id'];
            $prroWorkshift->shift_tax_id = $response['shift_tax_id'];
            $prroWorkshift->qr = $response['qr'];
            $prroWorkshift->save();

            return $prroWorkshift->save();
        } else {
            return false;
        }

    }
}
