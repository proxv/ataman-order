<?php

namespace dvizh\order\models;

use Yii;

/**
 * This is the model class for table "order_return_element".
 *
 * @property int $id
 * @property int $item_id
 * @property int $return_id
 * @property float $count
 */
class OrderReturnElement extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_return_element';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['item_id', 'return_id'], 'required'],
            [['item_id', 'return_id'], 'integer'],
            [['count'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'return_id' => yii::t('order', 'Return'),
            'item_id' => yii::t('order', 'Product'),
            'count' => yii::t('order', 'Count'),
            'product' => yii::t('order', 'Product'),
            'totalPrice' => yii::t('order', 'Total cost'),
        ];
    }

    /**
     * @return float
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @return int
     */
    public function getItemId()
    {
        return $this->item_id;
    }

    /**
     * @return mixed
     */
    public function getReturn()
    {
        return $this->hasOne(\dvizh\order\models\OrderReturn::className(), ['id' => 'return_id']);
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->hasOne(\dvizh\shop\models\Product::className(), ['id' => 'item_id'])->one();
    }

    /**
     * @return Element
     */
    public function getOrderElement()
    {
        $return = $this->getReturn()->one();
        $order = $return->getOrder()->one();

        return Element::find()->where([
            'order_id' => $order->id,
            'item_id' => $this->getItemId()
        ])->one();
    }

    /**
     * @return float
     */
    public function getTotalPrice()
    {
        $orderElement = $this->getOrderElement();
        $price = $orderElement->getprice();

        return $price * $this->getCount();
    }
}
