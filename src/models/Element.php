<?php

namespace dvizh\order\models;

use yii;
use dvizh\order\interfaces\OrderElement as ElementInterface;

/**
 * Class Element
 * @package dvizh\order\models
 *
 * @property integer $id
 * @property string $model
 * @property integer $order_id
 * @property integer $item_id
 * @property float $count
 * @property float $price
 * @property float $base_price
 * @property string $description
 * @property string $options
 * @property boolean $is_assigment
 * @property boolean $is_deleted
 * @property string $name
 * @property string $excise_barcode
 *
 *
 */
class Element extends \yii\db\ActiveRecord implements ElementInterface
{
    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%order_element}}';
    }

    /**
     * @return array[]
     */
    public function rules()
    {
        return [
            [['order_id', 'model', 'item_id'], 'required'],
            [['description', 'model', 'options', 'name', 'excise_barcode'], 'string'],
            [['price'], 'double'],
            [['item_id', 'is_deleted'], 'integer'],
            [['count'], 'number']
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => yii::t('order', 'ID'),
            'name' => yii::t('order', 'Name'),
            'price' => yii::t('order', 'Price'),
            'base_price' => yii::t('order', 'Base price'),
            'description' => yii::t('order', 'Description'),
            'options' => yii::t('order', 'Options'),
            'model' => yii::t('order', 'Model name'),
            'order_id' => yii::t('order', 'Order ID'),
            'item_id' => yii::t('order', 'Product'),
            'count' => yii::t('order', 'Count'),
            'is_assigment' => yii::t('order', 'Assigment'),
            'is_deleted' => yii::t('order', 'Deleted'),
            'excise_barcode' => 'Акцизний код',
        ];
    }

    /**
     * @param integer $orderId
     * @return $this|mixed
     */
    public function setOrderId($orderId)
    {
        $this->order_id = $orderId;

        return $this;
    }

    /**
     * @param boolean $isAssigment
     * @return $this|mixed
     */
    public function setAssigment($isAssigment)
    {
        $this->is_assigment = $isAssigment;

        return $this;
    }

    /**
     * @param string $modelName
     * @return $this|mixed
     */
    public function setModelName($modelName)
    {
        $this->model = $modelName;

        return $this;
    }

    /**
     * @param string $name
     * @return $this|mixed
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @param integer $itemId
     * @return $this|mixed
     */
    public function setItemId($itemId)
    {
        $this->item_id = $itemId;

        return $this;
    }

    /**
     * @param float $count
     * @return mixed|void
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @param float $basePrice
     * @return $this|mixed
     */
    public function setBasePrice($basePrice)
    {
        $this->base_price = $basePrice;

        return $this;
    }

    /**
     * @param float $price
     * @return $this|mixed
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @param string $options
     * @return $this|mixed
     */
    public function setOptions($options)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @param string $description
     * @return $this|mixed
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getMostCount()
    {

    }

    /**
     * @return int|mixed
     */
    public function getOrderId()
    {
        return $this->order_id;
    }

    /**
     * @return bool|mixed
     */
    public function getAssigment()
    {
        return $this->is_assigment;
    }

    /**
     * @return mixed|string
     */
    public function getModelName()
    {
        return $this->model;
    }

    /**
     * @return mixed|string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return int|mixed
     */
    public function getItemId()
    {
        return $this->item_id;
    }

    /**
     * @return float|mixed
     */
    public function getBasePrice()
    {
        return $this->base_price;
    }

    /**
     * @return mixed|string
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return mixed
     */
    public function saveData()
    {
        return $this->save();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float|mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return float|mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @return mixed
     */
    public function getProduct()
    {
        return $this->hasOne(\dvizh\shop\models\Product::className(), ['id' => 'item_id']);

        $modelStr = $this->model;
        $productModel = new $modelStr;

        return $this->hasOne($productModel::className(), ['id' => 'item_id'])->one();
    }

    public function getProductCategory(){
        return $this->product->category;
    }

    /**
     * @return mixed
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @param bool $withCartElementModel
     * @return mixed|string
     */
    public function getModel($withCartElementModel = true)
    {
        if (!$withCartElementModel) {
            return $this->model;
        }

        if (is_string($this->model)) {
            if (class_exists($this->model)) {
                $model = '\\' . $this->model;
                $productModel = new $model();
                if ($productModel = $productModel::findOne($this->item_id)) {
                    $model = $productModel;
                } else {
                    throw new \yii\base\Exception('Element model do not found');
                }
            } else {
                //throw new \yii\base\Exception('Unknow element model');
            }
        } else {
            $model = $this->model;
        }

        return $model;
    }

    /**
     * @param integer $id
     * @param $name
     * @param $value
     */
    public static function editField($id, $name, $value)
    {
        $setting = Element::findOne($id);
        $setting->$name = $value;
        $setting->save();
    }

    /**
     * @return bool
     */
    public function afterDelete()
    {
        parent::afterDelete();

        return true;
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        parent::beforeDelete();

        return true;
    }
}
