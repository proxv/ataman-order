<?php

namespace dvizh\order\models;

use dvizh\order\models\tools\OrderPaySystemQuery;
use Yii;

/**
 * This is the model class for table "order_pay_system".
 *
 * @property int $id
 * @property int $order_id
 * @property string $tax_num
 * @property string $name
 * @property string $acquire_pn
 * @property string $acquire_nm
 * @property string $acquire_trans_id
 * @property string $pos_trans_date
 * @property string $pos_trans_num
 * @property string $device_id
 * @property string $epz_details
 * @property string $auth_cd
 * @property float|null $sum
 * @property float|null $commission
 */
class OrderPaySystem extends \yii\db\ActiveRecord
{

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_pay_system';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
//            [['order_id', 'tax_num', 'name', 'acquire_pn',
//                'acquire_nm', 'acquire_trans_id', 'pos_trans_date',
//                'pos_trans_num', 'device_id', 'epz_details', 'auth_cd'], 'required'],
            [['order_id'], 'integer'],
            [['sum', 'commission'], 'number'],
            [['tax_num', 'acquire_pn', 'auth_cd'], 'string', 'max' => 64],
            [['name', 'acquire_nm', 'pos_trans_date'], 'string', 'max' => 255],
            [['acquire_trans_id', 'pos_trans_num', 'device_id', 'epz_details'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('order', 'ID'),
            'order_id' => Yii::t('order', 'Order ID'),
            'tax_num' => Yii::t('order', 'Tax Num'),
            'name' => Yii::t('order', 'Name'),
            'acquire_pn' => Yii::t('order', 'Acquire Pn'),
            'acquire_nm' => Yii::t('order', 'Acquire Nm'),
            'acquire_trans_id' => Yii::t('order', 'Acquire Trans ID'),
            'pos_trans_date' => Yii::t('order', 'Pos Trans Date'),
            'pos_trans_num' => Yii::t('order', 'Pos Trans Num'),
            'device_id' => Yii::t('order', 'Device ID'),
            'epz_details' => Yii::t('order', 'Epz Details'),
            'auth_cd' => Yii::t('order', 'Auth Cd'),
            'sum' => Yii::t('order', 'Sum'),
            'commission' => Yii::t('order', 'Commission'),
        ];
    }

    /**
     * {@inheritdoc}
     * @return OrderPaySystemQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new OrderPaySystemQuery(get_called_class());
    }

    public function getOrder(){
        return $this->hasOne(Order::className(), ['id'=> 'order_id']);
    }
}
