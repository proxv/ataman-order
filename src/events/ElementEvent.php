<?php

namespace dvizh\order\events;

use dvizh\order\models\Element;
use dvizh\order\models\Order;
use yii\base\Event;

/**
 * Class ElementEvent
 * @package dvizh\order\events
 */
class ElementEvent extends Event
{
    /**
     * @var Element
     */
    public $model;
    /**
     * @var Order
     */
    public $orderModel;
    /**
     * @var Product
     */
    public $productModel;
}