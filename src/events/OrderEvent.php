<?php

namespace dvizh\order\events;

use yii\base\Event;

/**
 * Class OrderEvent
 * @package dvizh\order\events
 */
class OrderEvent extends Event
{
    /**
     * @var
     */
    public $model;
    /**
     * @var
     */
    public $elements;
}