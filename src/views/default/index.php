<?php

use yii\helpers\Url;

$this->title = yii::t('order', 'Orders');

$this->params['breadcrumbs'][] = $this->title;


?>
<div class="model-index">
    <!-- Nav tabs -->
    <ul class="nav nav-tabs">
        <li role="presentation" class="active">
            <a href="#main" data-toggle="tab">
                Головна
            </a>
        </li>
        <li role="presentation">
            <a href="#cashOperations" data-toggle="tab">
                Готівкові операції
            </a>
        </li>
        <li role="presentation">
            <a href="#cashStatistic" data-toggle="tab">
                Статистика
            </a>
        </li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" id="main" class="tab-pane active">
            <table class="table">
                <tr>
                    <th><?= yii::t('order', 'Order statistics'); ?></th>
                    <td>
                        <a href="<?= Url::toRoute('/order/stat/index'); ?>" class="btn btn-default">
                            <i class="glyphicon glyphicon-eye-open"/></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <th>ABC-Аналіз</th>
                    <td>
                        <a href="<?= Url::toRoute('/order/stat/abc'); ?>" class="btn btn-default">
                            <i class="glyphicon glyphicon-eye-open"/></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <th><?= yii::t('order', 'Cash Boxes'); ?></th>
                    <td>
                        <a href="<?= Url::toRoute('/order/cashbox/index'); ?>" class="btn btn-default">
                            <i class="glyphicon glyphicon-eye-open"/></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <th><?= yii::t('order', 'Return'); ?></th>
                    <td>
                        <a href="<?= Url::toRoute('/order/return/index'); ?>" class="btn btn-default">
                            <i class="glyphicon glyphicon-eye-open"/></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <th><?= yii::t('order', 'Removed goods from the order'); ?></th>
                    <td>
                        <a href="<?= Url::toRoute('/order/element-removed/index'); ?>" class="btn btn-default">
                            <i class="glyphicon glyphicon-eye-open"/></i>
                        </a>
                    </td>
                </tr>
            </table>
        </div>
        <div role="tabpanel" id="cashOperations" class="tab-pane">
            <table class="table">
                <tr>
                    <th><?= yii::t('order', 'Deposit cash') ?></th>
                    <td>
                        <a href="<?= Url::toRoute(['/order/deposit-cash/index']); ?>" class="btn btn-default">
                            <i class="glyphicon glyphicon-eye-open"/></i>
                        </a>
                    </td>
                </tr>
                <tr>
                    <th><?= yii::t('order', 'Cash withdrawal') ?></th>
                    <td>
                        <a href="<?= Url::toRoute(['/order/cash-withdrawal/index']); ?>" class="btn btn-default">
                            <i class="glyphicon glyphicon-eye-open"/></i>
                        </a>
                    </td>
                </tr>
            </table>
        </div>
        <div role="tabpanel" id="cashStatistic" class="tab-pane">
            <table class="table">
                <tr>
                    <th>Продавець-Продукт</th>
                    <td>
                        <a href="<?= Url::toRoute(['/order/stat/user-product']); ?>" class="btn btn-default">
                            <i class="glyphicon glyphicon-eye-open"/></i>
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</div>
