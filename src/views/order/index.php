<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use dvizh\order\widgets\Informer;
use kartik\export\ExportMenu;
use nex\datepicker\DatePicker;
use dvizh\order\assets\Asset;
use dvizh\order\assets\OrdersListAsset;
use dvizh\order\widgets\AssigmentToOrder;

/* @var $searchModel dvizh\order\models\tools\OrderSearch */
/* @var $paymentTypes dvizh\order\models\PaymentType */
/* @var $model dvizh\order\models\Order */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $module \dvizh\order\controllers\OrderController */
/* @var $hasAssignments \dvizh\order\controllers\OrderController */
/* @var $tab \dvizh\order\controllers\OrderController */

$this->title = yii::t('order', 'Orders');
$this->params['breadcrumbs'][] = ['label' => yii::t('order', 'Orders'), 'url' => ['/order/default/index']];
$this->params['breadcrumbs'][] = $this->title;


Asset::register($this);
OrdersListAsset::register($this);

if ($dateStart = yii::$app->request->get('date_start')) {
    $dateStart = date('Y-m-d', strtotime($dateStart));
}

if ($dateStop = yii::$app->request->get('date_stop')) {
    $dateStop = date('Y-m-d', strtotime($dateStop));
}

$timeStart = yii::$app->request->get('time_start');
$timeStop = yii::$app->request->get('time_stop');

$columns = [];

$columns[] = [
    'class' => \yii\grid\SerialColumn::className(),
];

$columns[] = [
    'attribute' => 'id',
    'options' => ['style' => 'width: 49px;'],
    'contentOptions' => [
        'class' => 'show-details'
    ],
];
$columns[] = [
    'attribute' => 'workshift_id',
    'options' => ['style' => 'width: 49px;'],
    'contentOptions' => [
        'class' => 'show-details'
    ],
];

$columns[] = [
    'attribute' => 'cashbox_order_key',
    'options' => ['style' => 'width: 49px;'],
    'contentOptions' => [
        'class' => 'show-details'
    ],
];

$columns[] = [
    'attribute' => 'base_cost',
//    'label' => yii::$app->getModule('order')->currency,
    'contentOptions' => [
        'class' => 'show-details'
    ],
];
$columns[] = [
    'attribute' => 'cost',
    'contentOptions' => [
        'class' => 'show-details'
    ],
    'format' => 'raw',
    'value' => function ($model) {
        if ($model->isRelatedPayment()) {
            $cashAmount = $model->getRelatedCashAmount();
            $cardAmount = $model->getRelatedCardAmount();
            return <<<HTML
                <div>Загал.: <b>$model->cost</b></div>
                <div>Готів.: <b>$cashAmount</b></div>
                <div>Карта.: <b>$cardAmount</b></div>
HTML;
        } else {
            return $model->cost;
        }
    }
];


if (Yii::$app->getModule('order')->showPaymentColumn) {
    $columns[] = [
        'attribute' => 'payment',
        'filter' => Html::activeDropDownList(
            $searchModel,
            'payment',
            ['yes' => yii::t('order', 'yes'), 'no' => yii::t('order', 'no')],
            ['class' => 'form-control', 'prompt' => Yii::t('order', 'Paid')]
        ),
        'value' => function ($model) {
            return yii::t('order', $model->payment);
        }
    ];
}

foreach (Yii::$app->getModule('order')->orderColumns as $column) {
    if ($column == 'payment_type_id') {
        $column = [
            'attribute' => 'payment_type_id',
            'filter' => Html::activeDropDownList(
                $searchModel,
                'payment_type_id',
                $paymentTypes,
                ['class' => 'form-control', 'prompt' => Yii::t('order', 'Payment type')]
            ),
            'contentOptions' => [
                'class' => 'show-details'
            ],
            'value' => function ($model) use ($paymentTypes) {
                if (isset($paymentTypes[$model->payment_type_id])) {
                    return $paymentTypes[$model->payment_type_id];
                }
            }
        ];
    }

    if (gettype($column) === 'string') {
        $column = [
            'attribute' => $column,
            'contentOptions' => [
                'class' => 'show-details'
            ],
        ];
    }

    $columns[] = $column;
}

$columns[] = [
    'attribute' => 'date',
    'filter' => false,
    'contentOptions' => [
        'class' => 'show-details'
    ],
    'value' => $model->date,

];

$columns[] = [
    'attribute' => 'status',
    'filter' => Html::activeDropDownList(
        $searchModel,
        'status',
        yii::$app->getModule('order')->orderStatuses,
        ['class' => 'form-control', 'prompt' => Yii::t('order', 'Status')]
    ),
    'format' => 'raw',
    'value' => function ($model) use ($module) {
        // if(!$model->status) {
        //     return null;
        // }
        //
        // $return = Yii::$app->getModule('order')->orderStatuses[$model->status];

        return \dvizh\order\widgets\ChangeStatus::widget(['model' => $model]);
    }
];

if ($module->elementToOrderUrl) {
    $columns[] = [
        'label' => yii::t('order', 'Add to order'),
        'content' => function ($model) use ($module) {
            return '<a href="' . Url::toRoute([$module->elementToOrderUrl, 'order_id' => $model->id]) . '" class="btn btn-success"><i class="glyphicon glyphicon-plus"></i></a>';
        }
    ];
}

$columns[] = ['class' => 'yii\grid\ActionColumn', 'template' => '{view} {delete}'];
$columns[] = [
    'attribute' => 'elementsRelation',
    'label' => 'Елементи чеку',
    'format' => 'raw',
    'value' => function ($model) {
        $elements = $model->getElementsRelation()->all();
        $temp = [];
        $i = 0;
        foreach ($elements as $element) {
            $i++;
            $name = $element->product->name;
            $count = $element->count;
            $price = $element->price;
            $base_price = $element->base_price;

            $temp[] = '#' . $i . $name . ' - ' . $count . ' * ' . $price . ' = ' . ($count * $price);
        }
        return join('&#10;', $temp);
    }
];
$columns[] = [

    'attribute' => 'is_fiscalized',

    'filter' => Html::activeDropDownList(
        $searchModel,
        'is_fiscalized',
        [1=>'Так', 0=>'Ні'],
        ['class' => 'form-control', 'prompt' => 'Оберіть тип...']
    ),
//    'format' => 'raw',
    'value' => function ($model) {
       return $model->is_fiscalized ? 'Так' : 'Ні';
    }
];


$order = yii::$app->order;
?>

<div class="informer-widget">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><a href="#" style="border-bottom: 1px solid #ccc;"
                                       onclick="$('.order-statistics-body').toggle(); return false;"><?= yii::t('order', 'Statistics'); ?>
                    ...</a></h3>
        </div>
        <div class="order-statistics-body" style="display: none;">
            <div class="panel-body">
                <?= Informer::widget(); ?>
            </div>
        </div>
    </div>
</div>

<div class="order-index">
    <div class="box">
        <div class="box-body">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= yii::t('order', 'Search by date'); ?></h3>
                </div>
                <div class="order-search-body">
                    <div class="panel-body">
                        <form action="<?= Url::toRoute(['/order/order/index']); ?>" class="row search">
                            <?php
                            foreach (yii::$app->request->get() as $key => $value) {
                                if (!is_array($value)) {
                                    echo Html::input('hidden', Html::encode($key), Html::encode($value));
                                }
                            }
                            ?>
                            <div class="col-md-4">
                                <label><?= yii::t('order', 'Date'); ?></label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <?= DatePicker::widget([
                                            'name' => 'date_start',
                                            'addon' => false,
                                            'value' => $dateStart,
                                            'size' => 'sm',
                                            'language' => 'ru',
                                            'placeholder' => yii::t('order', 'Date from'),
                                            'options' => [
                                                'autocomplete' => 'off'
                                            ],
                                            'clientOptions' => [
                                                'format' => 'yyyy-MM-DD',
                                                'minDate' => '2015-01-01',
                                            ],
                                            'dropdownItems' => [
                                                ['label' => 'Yesterday', 'url' => '#', 'value' => \Yii::$app->formatter->asDate('-1 day')],
                                                ['label' => 'Tomorrow', 'url' => '#', 'value' => \Yii::$app->formatter->asDate('+1 day')],
                                                ['label' => 'Some value', 'url' => '#', 'value' => 'Special value'],
                                            ],
                                        ]); ?>
                                        <?php if ($timeStart && !yii::$app->request->get('OrderSearch')) { ?>
                                            <input type="hidden" name="time_start"
                                                   value="<?= Html::encode($timeStart); ?>"/>
                                            <p><small><?= yii::t('order', 'Date from'); ?>
                                                    : <?= Html::encode($timeStart); ?></small></p>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= DatePicker::widget([
                                            'name' => 'date_stop',
                                            'addon' => false,
                                            'value' => $dateStop,
                                            'size' => 'sm',
                                            'placeholder' => yii::t('order', 'Date to'),
                                            'language' => 'ru',
                                            'options' => [
                                                'autocomplete' => 'off'
                                            ],
                                            'clientOptions' => [
                                                'format' => 'yyyy-MM-DD',
                                                'minDate' => '2015-01-01',
                                            ],
                                            'dropdownItems' => [
                                                ['label' => yii::t('order', 'Yesterday'), 'url' => '#', 'value' => \Yii::$app->formatter->asDate('-1 day')],
                                                ['label' => yii::t('order', 'Tomorrow'), 'url' => '#', 'value' => \Yii::$app->formatter->asDate('+1 day')],
                                                ['label' => yii::t('order', 'Some value'), 'url' => '#', 'value' => 'Special value'],
                                            ],
                                        ]); ?>
                                        <?php if ($timeStop && !yii::$app->request->get('OrderSearch')) { ?>
                                            <input type="hidden" name="time_stop"
                                                   value="<?= Html::encode($timeStop); ?>"/>
                                            <p><small><?= yii::t('order', 'Date to'); ?>:
                                                    <br/><?= Html::encode($timeStop); ?></small></p>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-3">
<!--                                --><?php
//                                echo '<label class="control-label">'.yii::t('order', 'Element name').'</label>';
//                                echo \kartik\select2\Select2::widget([
//                                    'name' => 'elementName',
//                                    'data' => ['Product 1', 'Product 2'],
//                                    'options' => [
//                                        'placeholder' => 'Виберіть товар ...',
//                                        'multiple' => false
//                                    ],
//                                ]);
//                                ?>
                            </div>

                            <div class="col-md-2">
                                <label>&nbsp;</label>
                                <div>
                                    <input class="btn btn-success" type="submit"
                                           value="<?= Yii::t('order', 'Search'); ?>"/>
                                    <a href="<?= Url::toRoute(['/order/order/index', 'tab' => $tab]); ?>"
                                       class="btn btn-default"><i class="glyphicon glyphicon-remove-sign"></i></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>


            <?php if ($hasAssignments > 0) { ?>
                <div class="tabs row">
                    <div class="col-md-6">
                        <ul class="nav nav-tabs" role="tablist">
                            <li <?php if ($tab == 'orders') { ?>class="active"<?php } ?>><a
                                        href="<?= Url::toRoute(['/order/order/index', 'tab' => 'orders']); ?>"><?= yii::t('order', 'Orders'); ?></a>
                            </li>
                            <li <?php if ($tab == 'assigments') { ?>class="active"<?php } ?>><a
                                        href="<?= Url::toRoute(['/order/order/index', 'tab' => 'assigments']); ?>"><?= yii::t('order', 'Assigments'); ?></a>
                            </li>
                        </ul>
                    </div>
                </div>
            <?php } ?>

            <div class="summary row">
                <div class="col-md-4">
                    <h3>
                        <?= number_format($dataProvider->query->sum('cost'), 2, ',', '.'); ?>
                        <?= $module->currency; ?>
                    </h3>
                </div>
                <div class="col-md-4">
                    <?php
                    if (Yii::$app->getModule('order')->showPaymentColumn) {
                        ?>
                        <h3>
                            <?php
                            echo yii::t('order', 'Paid') . ": ";
                            $query = clone $dataProvider->query;
                            echo number_format($query->where('payment <> \'no\'')->sum('cost'), 2, ',', '.') . $module->currency;
                            ?>
                        </h3>
                        <?php
                    }
                    ?>
                </div>
                <div class="col-md-4 export">
                    <?php
                    $gridColumns = $columns;
                    echo ExportMenu::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => $gridColumns
                    ]);
                    ?>
                </div>
            </div>

            <div class="order-list">
                <?php
                $columnSelectorForGridView = rikcage\grid_column_select\ColumnSelector::widget([
                    'columnSelectorOptions' => [
                        'title' => 'Оберіть колонки для відображення',
                        'label' => 'Колонки',
                        'icon' => '<i class="glyphicon glyphicon-eye-open"></i>',
                    ],
                    'columnBatchToggleSettings' => [
                        'show' => true,
                        'label' => 'Усі/Жодного',
                        'options' => ['class' => 'kv-toggle-all'],
                    ],
//        'showColumnSelector' => false,
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'defaultShowColumns' => [0, 2, 4, 5, 8, 10, 11, 12],
                    'columns' => $columns,
                ]);

                echo \kartik\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'columns' => rikcage\grid_column_select\ColumnSelector::getShowColumns(),
                    'responsiveWrap' => false,
                    'toolbar' => false,
                    'panel' => [
                        'type' => \kartik\grid\GridView::TYPE_DEFAULT,
                        'heading' => false,
                        'before' => $columnSelectorForGridView,
                        'after' => false,
                    ],
                ]);
                //                echo \kartik\grid\GridView::widget([
                //                    'responsiveWrap' => false,
                //                    'tableOptions' => [
                //                        'class' => 'table table-striped',
                //                    ],
                //                    'export' => false,
                //                    'dataProvider' => $dataProvider,
                //                    'filterModel' => $searchModel,
                //                    'columns' => $columns,
                //                ]); ?>
            </div>
        </div>
    </div>
</div>
