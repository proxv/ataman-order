<?php

use yii\helpers\Html;
use yii\grid\GridView;

///* @var $this yii\web\View */
/* @var $searchModel dvizh\order\models\tools\ElementRemovedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $statistic dvizh\order\controllers\ElementRemovedController */

$this->title = yii::t('order', 'Removed goods from the order');
$this->params['breadcrumbs'][] = ['label' => Yii::t('order', 'Orders'), 'url' => ['/order/default/index']];

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="element-removed-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php

    $columns = [
        //0
        ['class' => 'yii\grid\SerialColumn'],
        'id',
        'date',
        [
            'attribute' => 'cashbox',
            'label' => yii::t('order', 'Cash Box'),
            'value' => function ($model) {
                return $model->getCashbox()->name;
            }
        ],
        'workshift_id',
        //5
        [
            'attribute' => 'user_id',
            'label' => yii::t('order', 'User'),
            'value' => function ($model) {
                return $model->getUser()->username;
            }
        ],
        [
            'attribute' => 'product.name',
            'label' => yii::t('order', 'Product')
        ],
        'count'
    ];
    $columnSelectorForGridView = rikcage\grid_column_select\ColumnSelector::widget([
        'columnSelectorOptions' => [
            'title' => 'Оберіть колонки для відображення',
            'label' => 'Колонки',
            'icon' => '<i class="glyphicon glyphicon-eye-open"></i>',
        ],
        'columnBatchToggleSettings' => [
            'show' => true,
            'label' => 'Усі/Жодного',
            'options' => ['class' => 'kv-toggle-all'],
        ],
//        'showColumnSelector' => false,
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'defaultShowColumns' => [0, 2, 3, 4, 5, 6, 7],
        'columns' => $columns,
    ]);

    echo \kartik\grid\GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => rikcage\grid_column_select\ColumnSelector::getShowColumns(),
        'responsiveWrap' => false,
        'toolbar' => false,
        'panel' => [
            'type' => \kartik\grid\GridView::TYPE_DEFAULT,
            'heading' => false,
            'before' => $columnSelectorForGridView,
            'after' => false,
        ],
    ]);
    ?>

</div>
