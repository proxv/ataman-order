<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

///* @var $this yii\web\View */
/* @var $model dvizh\order\models\CashboxWorkshift */
/* @var $showCloseAmount dvizh\order\models\CashboxWorkshift */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="cashbox-workshift-form">

    <div class="row">
        <div class="col-xs-12">
            <?php if (Yii::$app->session->hasFlash('warning')) { ?>
                <div class="alert alert-warning" role="alert">
                    <?= Yii::$app->session->getFlash('warning') ?>
                </div>
            <?php } ?>
        </div>
    </div>

    <?php $form = ActiveForm::begin(); ?>

    <?php echo $form->errorSummary($model); ?>

    <?php if ($showCloseAmount) {
        echo $form->field($model, 'close_amount')->textInput();
    } else {
        echo $form->field($model, 'open_amount')->textInput();
    }
    ?>
    <!--    --><?php //echo $form->field($model, 'open_amount')->textInput() ?>

    <div class="form-group">
        <?php echo Html::submitButton($model->isNewRecord ? 'Відкрти зміну' : 'Закрити зміну', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
