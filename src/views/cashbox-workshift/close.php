<?php

use yii\helpers\Html;


///* @var $this yii\web\View */
/* @var $model dvizh\order\models\CashboxWorkshift */
/* @var $canSwitchCashBox dvizh\order\models\CashboxWorkshift */
/* @var $showCloseAmount dvizh\order\models\CashboxWorkshift */

$cashbox = $model->getCashbox();

$this->title = 'Закриття зміни';
$this->params['breadcrumbs'][] = ['label' => 'Каси', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $cashbox->name, 'url' => ['cashbox/view', 'id' => $cashbox->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cashbox-workshift-close">

    <?php echo $this->render('_form', [
        'model' => $model,
        'canSwitchCashBox' => $canSwitchCashBox,
        'showCloseAmount' => $showCloseAmount,
    ]) ?>

</div>
