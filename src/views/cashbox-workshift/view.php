<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

///* @var $this yii\web\View */
/* @var $model dvizh\order\models\CashboxWorkshift */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cashbox Workshifts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cashbox-workshift-view">
    <div class="row">
        <div class="col-xs-12">
            <?php if (Yii::$app->session->hasFlash('success')) { ?>
                <div class="alert alert-success" role="alert">
                    <?= Yii::$app->session->getFlash('success') ?>
                </div>
            <?php } ?>
            <?php if (Yii::$app->session->hasFlash('error')) { ?>
                <div class="alert alert-warning" role="alert">
                    <?= Yii::$app->session->getFlash('error') ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <p>
        <?php echo Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?php echo Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>
    <p>
        <?php echo Html::a('Close', ['close', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
    </p>

    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'cashbox_id',
            'user_id',
            'crete_at',
            'update_at',
            'closed_at',
            'open_amount',
            'cash_amount',
            'card_amount',
            'close_amount',
            'is_closed',
        ],
    ]) ?>

</div>
