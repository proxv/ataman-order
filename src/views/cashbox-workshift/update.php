<?php

use yii\helpers\Html;

///* @var $this yii\web\View */
/* @var $model dvizh\order\models\CashboxWorkshift */
/* @var $canSwitchCashBox dvizh\order\models\CashboxWorkshift */
/* @var $showCloseAmount dvizh\order\models\CashboxWorkshift */

$this->title = 'Update Cashbox Workshift: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cashbox Workshifts', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cashbox-workshift-update">

    <?php echo $this->render('_form', [
        'model' => $model,
        'showCloseAmount' => $showCloseAmount,
    ]) ?>

</div>
