<?php

use yii\helpers\Html;

///* @var $this yii\web\View */
/* @var $model dvizh\order\models\CashboxWorkshift */
/* @var $canSwitchCashBox dvizh\order\models\CashboxWorkshift */
/* @var $showCloseAmount dvizh\order\models\CashboxWorkshift */

$cashbox = $model->getCashbox();

$this->title = 'Відкрити зміну';
$this->params['breadcrumbs'][] = ['label' => 'Каса', 'url' => ['cashbox/index']];
$this->params['breadcrumbs'][] = ['label' => $cashbox->name, 'url' => ['cashbox/view', 'id' => $cashbox->id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cashbox-workshift-create">

    <?php echo $this->render('_form', [
        'model' => $model,
        'showCloseAmount' => $showCloseAmount,
    ]) ?>

</div>
