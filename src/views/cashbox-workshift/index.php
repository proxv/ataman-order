<?php

use yii\helpers\Html;
use yii\grid\GridView;

///* @var $this yii\web\View */
/* @var $searchModel dvizh\order\models\cashbox\CashboxWorkshiftSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Workshift';
$this->params['breadcrumbs'][] = ['label' => 'Cashboxes', 'url' => ['cashbox/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cashbox-workshift-index">
    <div class="row">
        <div class="col-xs-12">
            <?php if (Yii::$app->session->hasFlash('success')) { ?>
                <div class="alert alert-success" role="alert">
                    <?= Yii::$app->session->getFlash('success') ?>
                </div>
            <?php } ?>
            <?php if (Yii::$app->session->hasFlash('error')) { ?>
                <div class="alert alert-warning" role="alert">
                    <?= Yii::$app->session->getFlash('error') ?>
                </div>
            <?php } ?>
        </div>
    </div>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?php echo Html::a('Create Cashbox Workshift', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php echo GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
//            'cashbox_id',
            [
                'attribute' => 'cashbox_id',
                'value' => 'cashbox.name'
            ],
//            'user_id',
            [
                'attribute' => 'user_id',
                'value' => 'user.username'
            ],
            'crete_at',
            'update_at',
            // 'closed_at',
            // 'amount',
            // 'cash_amount',
            // 'card_amount',
            'is_closed',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
