<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

///* @var $this yii\web\View */
/* @var $model dvizh\order\models\cashbox\CashboxWorkshiftSearch */
/* @var $form yii\bootstrap\ActiveForm */
?>

<div class="cashbox-workshift-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?php echo $form->field($model, 'id') ?>

    <?php echo $form->field($model, 'cashbox_id') ?>

    <?php echo $form->field($model, 'user_id') ?>

    <?php echo $form->field($model, 'crete_at') ?>

    <?php echo $form->field($model, 'update_at') ?>

    <?php // echo $form->field($model, 'closed_at') ?>

    <?php // echo $form->field($model, 'amount') ?>

    <?php // echo $form->field($model, 'cash_amount') ?>

    <?php // echo $form->field($model, 'card_amount') ?>

    <div class="form-group">
        <?php echo Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?php echo Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
