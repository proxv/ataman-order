<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\grid\GridView;

use nex\datepicker\DatePicker;

///* @var $this yii\web\View */
/* @var $searchModel dvizh\order\models\tools\OrderReturnSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $statistic dvizh\order\controllers\ReturnController */

$this->title = 'Повернення';
$this->params['breadcrumbs'][] = ['label' => Yii::t('order', 'Orders'), 'url' => ['/order/default/index']];

$this->params['breadcrumbs'][] = $this->title;


if ($dateStart = yii::$app->request->get('date_start')) {
    $dateStart = date('Y-m-d', strtotime($dateStart));
}

if ($dateStop = yii::$app->request->get('date_stop')) {
    $dateStop = date('Y-m-d', strtotime($dateStop));
}

$timeStart = yii::$app->request->get('time_start');
$timeStop = yii::$app->request->get('time_stop');


?>
<div class="return-index">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title"><?= yii::t('order', 'Search by date'); ?></h3>
                </div>
                <div class="order-search-body">
                    <div class="panel-body">
                        <form action="<?= Url::toRoute(['/order/return/index']); ?>" class="row search">
                            <?php
                            foreach (yii::$app->request->get() as $key => $value) {
                                if (!is_array($value)) {
                                    echo Html::input('hidden', Html::encode($key), Html::encode($value));
                                }
                            }
                            ?>
                            <div class="col-md-4">
                                <label><?= yii::t('order', 'Date'); ?></label>
                                <div class="row">
                                    <div class="col-md-6">
                                        <?= DatePicker::widget([
                                            'name' => 'date_start',
                                            'addon' => false,
                                            'value' => $dateStart,
                                            'size' => 'sm',
                                            'language' => 'ru',
                                            'placeholder' => yii::t('order', 'Date from'),
                                            'options' => [
                                                'autocomplete' => 'off'
                                            ],
                                            'clientOptions' => [
                                                'format' => 'yyyy-MM-DD',
                                                'minDate' => '2015-01-01',
                                            ],
                                            'dropdownItems' => [
                                                ['label' => 'Yesterday', 'url' => '#', 'value' => \Yii::$app->formatter->asDate('-1 day')],
                                                ['label' => 'Tomorrow', 'url' => '#', 'value' => \Yii::$app->formatter->asDate('+1 day')],
                                                ['label' => 'Some value', 'url' => '#', 'value' => 'Special value'],
                                            ],
                                        ]); ?>
                                        <?php if ($timeStart && !yii::$app->request->get('OrderSearch')) { ?>
                                            <input type="hidden" name="time_start"
                                                   value="<?= Html::encode($timeStart); ?>"/>
                                            <p><small><?= yii::t('order', 'Date from'); ?>
                                                    : <?= Html::encode($timeStart); ?></small></p>
                                        <?php } ?>
                                    </div>
                                    <div class="col-md-6">
                                        <?= DatePicker::widget([
                                            'name' => 'date_stop',
                                            'addon' => false,
                                            'value' => $dateStop,
                                            'size' => 'sm',
                                            'placeholder' => yii::t('order', 'Date to'),
                                            'language' => 'ru',
                                            'options' => [
                                                'autocomplete' => 'off'
                                            ],
                                            'clientOptions' => [
                                                'format' => 'yyyy-MM-DD',
                                                'minDate' => '2015-01-01',
                                            ],
                                            'dropdownItems' => [
                                                ['label' => yii::t('order', 'Yesterday'), 'url' => '#', 'value' => \Yii::$app->formatter->asDate('-1 day')],
                                                ['label' => yii::t('order', 'Tomorrow'), 'url' => '#', 'value' => \Yii::$app->formatter->asDate('+1 day')],
                                                ['label' => yii::t('order', 'Some value'), 'url' => '#', 'value' => 'Special value'],
                                            ],
                                        ]); ?>
                                    </div>
                                </div>
                            </div>


                            <div class="col-md-2">
                                <label>&nbsp;</label>
                                <div>
                                    <input class="btn btn-success" type="submit"
                                           value="<?= Yii::t('order', 'Search'); ?>"/>
                                    <a href="<?= Url::toRoute(['/order/return/index']); ?>"
                                       class="btn btn-default"><i class="glyphicon glyphicon-remove-sign"></i></a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12">
            <?php
            $columns = [
                //0
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                [
                    'attribute' => 'cashbox_id',
                    'label' => 'Каса',
                    'value' => function ($model) {
                        $cashbox = $model->cashbox;
                        return '#' . $cashbox->id . ' (' . $cashbox->name . ')';
                    }
                ],
                'workshift_id',
                'order_id',
                //5
                [
                    'attribute' => 'order_id',
                    'label' => 'Чек з посу №',
                    'value' => function ($model) {
                        return $model->order->cashbox_order_key;
                    }
                ],
                [
                    'attribute' => 'user_id',
                    'label' => 'Користувач',
                    'value' => 'user.username'
                ],

                'date',
                'description',
                [
                    'label' => 'Кількість товарів',
                    'value' => 'elementCount'
                ],
                //10
                [
                    'attribute' => 'totalPrice',
                    'value' => function ($model) {
                        $totalPrice = $model->getTotalPrice();
                        return number_format($totalPrice, 2);
                    }
                ],

                ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
            ];
            $columnSelectorForGridView = rikcage\grid_column_select\ColumnSelector::widget([
                'columnSelectorOptions' => [
                    'title' => 'Оберіть колонки для відображення',
                    'label' => 'Колонки',
                    'icon' => '<i class="glyphicon glyphicon-eye-open"></i>',
                ],
                'columnBatchToggleSettings' => [
                    'show' => true,
                    'label' => 'Усі/Жодного',
                    'options' => ['class' => 'kv-toggle-all'],
                ],
//        'showColumnSelector' => false,
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'defaultShowColumns' => [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11],
                'columns' => $columns,
            ]);

            echo \kartik\grid\GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => rikcage\grid_column_select\ColumnSelector::getShowColumns(),
                'responsiveWrap' => false,
                'toolbar' => false,
                'panel' => [
                    'type' => \kartik\grid\GridView::TYPE_DEFAULT,
                    'heading' => false,
                    'before' => $columnSelectorForGridView,
                    'after' => false,
                ],
            ]);
            ?>

        </div>
    </div>
</div>
