<?php

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use nex\datepicker\DatePicker;
use yii\grid\GridView;

use yii\helpers\ArrayHelper;

///* @var $this yii\web\View */
/* @var $model dvizh\order\models\OrderReturn */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Повернення #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('order', 'Orders'), 'url' => ['/order/default/index']];

$this->params['breadcrumbs'][] = ['label' => 'Повернення', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->id;
?>
<div class="cashbox-view">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <h3>Інформація повернення</h3>
            <?php
            $attributes = [
                'id',
                'order_id',
                [
                    'attribute' => 'order_id',
                    'label' => 'Чек з посу №',
                    'value' => function ($model) {
                        return $model->order->cashbox_order_key;
                    }
                ],
                'date',
                'description',
                [
                    'attribute' => 'user_id',
                    'label' => 'Користувач',
                    'value' => $model->user->username
                ],
                [
                    'attribute' => 'cashbox_id',
                    'label' => 'Каса',
                    'value' => function ($model) {
                        $cashbox = $model->cashbox;
                        return '#' . $cashbox->id . ' (' . $cashbox->name . ')';
                    }
                ],
                'workshift_id',
                [
                    'attribute' => 'totalPrice',
                    'value' => function ($model) {
                        $totalPrice = $model->getTotalPrice();
                        return number_format($totalPrice, 2);
                    }
                ],
                'order.is_fiscalized'
            ];

            if($model->order->is_fiscalized){
                $attributes[] = [
                    'label' => 'Локальний номер',
                    'value' => $model->fiscalized->tax_id
                ];
                $attributes[] = [
                    'label' => 'Url',
                    'format' => 'raw',
                    'value' => Html::a("Переглянути", $model->fiscalized->qr, ['target'=>'_blank']),
                ];
            }

            echo DetailView::widget([
                'model' => $model,
                'attributes' => $attributes,
            ]) ?>
        </div>
        <div class="col-xs-12 col-sm-6">
            <h5><strong>Товари</strong></h5>
            <?= \kartik\grid\GridView::widget([
                'export' => false,
                'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
                'columns' => [
                    'id',
                    [
                        'attribute' => 'item_id',
                        'label' => 'Продукт',
                        'value' => function ($model) {
                            $product = $model->getProduct();
                            return '#' . $product->id . '. ' . $product->name;
                        }
                    ],
                    'count',
                    [
                        'attribute' => 'totalPrice',
                        'value' => function ($model) {
                            $totalPrice = $model->getTotalPrice();
                            return number_format($totalPrice, 2);
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
