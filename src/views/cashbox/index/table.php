<?php

use yii\grid\GridView;

/* @var $searchModel dvizh\order\models\cashbox\CashboxSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$columns = [
    //0
    ['class' => 'yii\grid\SerialColumn'],
    'id',
    'name',
    'description',
    [
        'attribute' => 'has_open_workshift',
        'label' => 'Статус зміни',
        'contentOptions' => function ($model) {
            $status = $model->getOpenWorkshift() ? 'bg-success' : 'bg-danger';
            return ['class' => $status];
        },
        'value' => function ($model) {
            $hasOpen = $model->getOpenWorkshift();
            return $hasOpen ? 'Активна' : 'Не активна';
        }
    ],
    //5
    [
        'attribute' => 'workshift_open_at',
        'label' => 'Зміну відкрито',
        'value' => function ($model) {
            $openWorksihft = $model->getOpenWorkshift();
            $closedWorksihft = $model->getLastClosedWorkshift();
            return $openWorksihft
                ? $openWorksihft->crete_at
                : $closedWorksihft->crete_at;
        }
    ],
    [
        'attribute' => 'workshift_open_at',
        'label' => 'Зміну Закрито',
        'value' => function ($model) {
            $openWorksihft = $model->getOpenWorkshift();
            $closedWorksihft = $model->getLastClosedWorkshift();
            return $openWorksihft
                ? $openWorksihft->closed_at
                : $closedWorksihft->closed_at;
        }
    ],
    'updated_at',
    [
        'attribute' => 'currentCashAmount',
        'label' => yii::t('order', 'Cash on cash box')
    ],
    [
        'attribute' => 'currentCashAmountLuck',
        'contentOptions' => function ($model) {
            $status = $model->currentCashAmountLuck < 0 ? 'bg-danger' : 'bg-success';
            return ['class' => $status];
        },
        'label' => yii::t('order', 'Lack')
    ],
    //10
    [
      'label' => 'Ключ зміни',
      'value' => function($model){
            return $model->openWorkshift->key;
      }
    ],
    [
        'label' => 'RRO',
        'value' => function($model){
            $modelDepartment = new \common\modules\openprro\models\PrroCashboxDepartment();
            $modelDepartment->cashbox_id = $model->id;

            $departments = $modelDepartment->getDepartmentsFromApi();
            $temp = [];
            foreach ($departments as $department) {
                $depData = $modelDepartment->find()
                    ->where(['department_id' => $department['department_id']])
                    ->one();
                $category = isset($depData->category) ? '(' . $depData->category->name . ')' : '';
                $temp[] = $department['rro_id'] . $category;
            }

            return join(', ', $temp);
        }
    ],
    ['class' => 'yii\grid\ActionColumn', 'template' => '{view}'],
];
$defaultShowColumns = [0, 2, 4, 5, 6, 8, 9, 12];
$columnSelectorForGridView = rikcage\grid_column_select\ColumnSelector::widget([
    'columnSelectorOptions' => [
        'title' => 'Оберіть колонки для відображення',
        'label' => 'Колонки',
        'icon' => '<i class="glyphicon glyphicon-eye-open"></i>',
    ],
    'columnBatchToggleSettings' => [
        'show' => true,
        'label' => 'Усі/Жодного',
        'options' => ['class' => 'kv-toggle-all'],
    ],
//        'showColumnSelector' => false,
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'defaultShowColumns' => $defaultShowColumns,
    'columns' => $columns,
]);

echo \kartik\grid\GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => rikcage\grid_column_select\ColumnSelector::getShowColumns(),
    'responsiveWrap' => false,
    'toolbar' => false,
    'panel' => [
        'type' => \kartik\grid\GridView::TYPE_DEFAULT,
        'heading' => false,
        'before' => $columnSelectorForGridView,
        'after' => false,
    ],
]); ?>