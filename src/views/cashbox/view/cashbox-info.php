<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\widgets\DetailView;

/* @var $model dvizh\order\models\Cashbox */

?>
<div class="col-xs-12">
    <h4><?= yii::t('order', 'Information about the cash box'); ?></h4>
    <?php echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'label' => 'RRO',
                'value' => function ($model) {
                    $modelDepartment = new \common\modules\openprro\models\PrroCashboxDepartment();
                    $modelDepartment->cashbox_id = $model->id;

                    $departments = $modelDepartment->getDepartmentsFromApi();
                    $temp = [];
                    foreach ($departments as $department) {
                        $depData = $modelDepartment->find()
                            ->where(['department_id' => $department['department_id']])
                            ->one();
                        $category = isset($depData->category) ? '(' . $depData->category->name . ')' : '';
                        $temp[] = $department['rro_id'] . $category;
                    }

                    return join(', ', $temp);
                }
            ],
//                            'id',
            'name',
//                            [
//                                'attribute' => 'user.username',
//                                'label' => yii::t('order', 'Default user')
//                            ],
            'description',
//                            'created_at',
//                            'updated_at',
            [
                'attribute' => 'currentCashAmount',
                'label' => yii::t('order', 'Cash on cash box')
            ],
            [
                'attribute' => 'currentCashAmountLuck',
                'label' => yii::t('order', 'Lack')
            ],
        ],
    ]) ?>
</div>