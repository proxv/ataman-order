<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

use yii\widgets\DetailView;

/* @var $model dvizh\order\models\Cashbox */
/* @var $dataProvider dvizh\order\controllers\CashboxController */
/* @var $searchModel dvizh\order\controllers\CashboxController */
/* @var $active_work_shift */
?>
<div class="row">
    <div class="col-xs-12">
        <p>
            <?php
            if ($active_work_shift) {
//                echo Html::a(yii::t('order', 'Close the work shift'), ['cashbox-workshift/close', 'id' => $active_work_shift->id, 'cashbox' => $model->id], ['class' => 'btn btn-primary']);
                echo Html::a('Переглянути інформацію', ['workshift', 'id' => $active_work_shift->id, 'cashbox' => $model->id], ['class' => 'btn btn-primary']);
            } else {
//                echo Html::a(yii::t('order', 'Open work shift'), ['cashbox-workshift/create', 'cashbox' => $model->id], ['class' => 'btn btn-success']);
            }
            ?>
        </p>
    </div>
    <div class="col-xs-12">
        <?php
        if ($active_work_shift) {
            echo '<h4>' . yii::t('order', 'Open change') . '</h4>';
            echo DetailView::widget([
                'model' => $active_work_shift,
                'attributes' => [
//                                'id',
                    [
                        'attribute' => 'user.username',
                        'label' => yii::t('order', 'Seller')
                    ],
//                                'crete_at',
//                                'update_at',
                    'open_amount',
                    'cash_amount',
                    'card_amount',
                    'return_amount',
                    'withdrawal_amount',
                    'deposit_amount',
//                                'key'
                ],
            ]);
        } else {
            echo '<h4>' . yii::t('order', 'No open changes') . '</h4>';
        }
        ?>
    </div>
</div>