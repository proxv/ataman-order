<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/* @var $model dvizh\order\models\Cashbox */
/* @var $dataProvider dvizh\order\controllers\CashboxController */
/* @var $searchModel dvizh\order\controllers\CashboxController */
?>

<div class="row">
    <div class="col-xs-12">

        <h4><?= yii::t('order', 'Closed changes'); ?></h4>
        <?php
        $columns = [
            //0
            ['class' => 'yii\grid\SerialColumn'],
            'id',
            [
                'attribute' => 'user_id',
                'filter' => Html::activeDropDownList(
                    $searchModel,
                    'user_id',
                    ArrayHelper::map(
                        \dvizh\order\models\CashboxWorkshift::find()->where(['cashbox_id' => $model->id])->all(),
                        'user_id',
                        'user.username'
                    ),
                    ['prompt' => 'Вибрати варіант ...', 'class' => 'form-control form-control-sm']),
                'value' => 'user.username'
            ],
            'open_amount',
            'cash_amount',
            //5
            'card_amount',
            'close_amount',
            'crete_at',
//                    [
//                        'attribute' => 'crete_at',
//                        'filter' => nex\datepicker\DatePicker::widget([
//                            'model' => $searchModel,
//                            'attribute' => 'crete_at',
//                            'language' => 'ru',
//                            'size' => 'sm',
//                            'readonly' => true,
//                            'placeholder' => 'Choose date',
//                            'options' => [
//                                'autocomplete' => 'off',
//                            ],
//                            'clientOptions' => [
//                                'format' => 'yyyy-MM-DD',
//                                'minDate' => '2015-01-01',
//                                'toolbarPlacement'=> 'bottom',
//                                'widgetPositioning'=> [
//                                        'horizontal' => 'auto',
//                                        'vertical' => 'bottom',
//                                ],
//                                'allowInputToggle' => true
//                            ],
//                        ]),
//                    ],
            'closed_at',
            'cash',
            //10
            [
                'attribute' => 'cashLack',
                'contentOptions' => function ($model) {
                    $status = $model->cashLack < 0 ? 'bg-danger' : 'bg-success';
                    return ['class' => $status];
                },
                'value' => 'cashLack'
            ],

            [
                'attribute' => 'closed_by_user_id',
                'label' => yii::t('order', 'Change is closed by'),
                'value' => 'user.username'
            ],
            'return_amount',
            'withdrawal_amount',
            'deposit_amount',
            //15
            'key',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url,$model) {
//                return $model->id;
                        return Html::a('<span class="glyphicon glyphicon-screenshot"></span>', ['workshift', 'id' => $model->id]);
                    },
                ]
            ],
        ];
        $defaultShowColumns = [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 13, 14, 16];
        $columnSelectorForGridView = rikcage\grid_column_select\ColumnSelector::widget([
            'columnSelectorOptions' => [
                'title' => 'Оберіть колонки для відображення',
                'label' => 'Колонки',
                'icon' => '<i class="glyphicon glyphicon-eye-open"></i>',
            ],
            'columnBatchToggleSettings' => [
                'show' => true,
                'label' => 'Усі/Жодного',
                'options' => ['class' => 'kv-toggle-all'],
            ],
//        'showColumnSelector' => false,
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'defaultShowColumns' => $defaultShowColumns,
            'columns' => $columns,
        ]);

        echo \kartik\grid\GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => rikcage\grid_column_select\ColumnSelector::getShowColumns(),
            'responsiveWrap' => false,
            'toolbar' => false,
            'panel' => [
                'type' => \kartik\grid\GridView::TYPE_DEFAULT,
                'heading' => false,
                'before' => $columnSelectorForGridView,
                'after' => false,
            ],
        ]);
        ?>
    </div>
</div>