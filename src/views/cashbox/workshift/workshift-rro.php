<?php

use yii\grid\GridView;
use yii\helpers\Html;


/* @var $model dvizh\order\models\Cashbox */

?>
<div class="col-xs-6">
    <?php

    $dataProvider = new \yii\data\ActiveDataProvider([
        'query' => $model->getPrroWorkshift(),
    ]);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
//            'online',
            'type',
//
//            'tax_id',
//            'shift_tax_id',
            'department_id',
//            [
//                'attribute' => 'department_id',
//                'format' => 'raw',
//                'value' => function ($model) {
//        var_dump($model->);
//                    return false;
//                }
//            ],
            [
                'attribute' => 'qr',
                'format' => 'raw',
                'value' => function ($model) {
                    return $model->type === 'close_shift'
                        ? ''
                        : Html::a("Переглянути", $model->qr, ['target' => '_blank']);
                }
            ],
            [
                'label' => 'Visual',
                'format' => 'raw',
                'value' => function ($model) {
                    $department = new \common\modules\openprro\models\PrroCashboxDepartment();
                    $department->cashbox_id = $model->workshift->cashbox_id;
                    $temp = $department->getDepartmentsFromApi();
                    $rro_id = $temp[$model->department_id]['rro_id'];
                    $isZrep = $model->type === 'close_shift';
                    $action = $isZrep ? 'zrep' : 'check';
                    $tax = $isZrep ? $model->shift_tax_id : $model->tax_id;

                    return Html::a("Переглянути", [
                        '/openprro/default/view-visual',
                        'type' => $action,
                        'rro' => $rro_id,
                        'tax' => $tax,
                    ], [ 'target' => '_blank',
                        'class' => 'btn',
                    ]);
                }
            ],
        ],
    ]);
    ?>
</div>