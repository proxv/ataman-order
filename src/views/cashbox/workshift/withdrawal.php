<?php

use yii\grid\GridView;

/* @var $model dvizh\order\models\Cashbox */

?>
<div class="col-xs-6">
    <?php
    $fiscalizeded = \dvizh\order\models\CashWithdrawal::find()
        ->select('SUM(amount) AS amount')
        ->where(['workshift_id' => $model->id, 'is_fiscalized' => 1])->one();
    $notFiscalizeded = \dvizh\order\models\CashWithdrawal::find()
        ->select('SUM(amount) AS amount')
        ->where(['workshift_id' => $model->id, 'is_fiscalized' => 0])->one();
    $fCost = $fiscalizeded->amount ? $fiscalizeded->amount : 0;
    $nfCost = $notFiscalizeded->amount ? $notFiscalizeded->amount : 0;
    ?>
    <h3>Зняття готівки</h3>
    <h4>Сума(Фіскал./Не Фіскал.): <?= $fCost + $nfCost ?>(<?= $fCost ?> / <?= $nfCost ?>)</h4>
    <?php
    $dataProvider = new \yii\data\ActiveDataProvider([
        'query' => \dvizh\order\models\CashWithdrawal::find()->where(['workshift_id' => $model->id]),
    ]);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'workshift_id',
            'id',
            'date',
            [
                'attribute' => 'cashbox',
                'label' => yii::t('order', 'Cash Box'),
                'value' => function ($model) {
                    return $model->getCashbox()->name;
                }
            ],
            //5
            [
                'attribute' => 'user',
                'label' => yii::t('order', 'Seller'),
                'value' => function ($model) {
                    return $model->getUser()->username;
                }
            ],
            'amount',
            'description',
            [
                'attribute' => 'is_fiscalized',
                'value' => function ($model) {
                    return $model->is_fiscalized ? 'Так' : 'Ні';
                }
            ],
        ],
    ]);
    ?>
</div>
