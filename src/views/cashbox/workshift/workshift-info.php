<?php

use yii\widgets\DetailView;


/* @var $model dvizh\order\models\CashboxWorkshift */

?>
<div class="col-xs-6">
    <?php
    echo DetailView::widget([
        'model' => $model,
        'attributes' => [
            'crete_at',
            'closed_at',

            'open_amount',
            'cash_amount',
            'card_amount',
            'close_amount',

            'return_amount',
            'withdrawal_amount',
            'deposit_amount',
            [
                'label' => 'RRO',
                'format'=>'raw',
                'value' => function ($model) {
                    $modelDepartment = new \common\modules\openprro\models\PrroCashboxDepartment();
                    $modelDepartment->cashbox_id = $model->cashbox->id;
                    $departmentsInfo = $modelDepartment->getDepartmentsFromApi();

                    $departments = $model->cashbox->prroCashboxDepartment;

                    $temp = [];
                    foreach ($departments as $department){
//                        is_main
//                        category_id
//                        department_id
//                        var_dump($departmentsInfo[$department->department_id]);
                        $departmentInfo = $departmentsInfo[$department->department_id];

                        $temp[] = 'Name:' . $departmentInfo['name']
                            . '<br> Dep#: ' . $department->department_id
                            . '<br> RRO#: ' . $departmentInfo['rro_id']
                            . '<br> Category: ' . $department->category->name
                        ;
                    }

                    return join('<hr> ', $temp);

//                    var_dump($model->cashbox->prroCashboxDepartment);
//                    $modelDepartment = new \common\modules\openprro\models\PrroCashboxDepartment();
//                    $modelDepartment->cashbox_id = $model->id;
//
//                    $departments = $modelDepartment->getDepartmentsFromApi();
//                    $temp = [];
//                    foreach ($departments as $department) {
//                        $depData = $modelDepartment->find()
//                            ->where(['department_id' => $department['department_id']])
//                            ->one();
//                        $category = isset($depData->category) ? '(' . $depData->category->name . ')' : '';
//                        $temp[] = $department['rro_id'] . $category;
//                    }
//
//                    return join(', ', $temp);
                }
            ],
        ],
    ]);
    ?>
</div>
