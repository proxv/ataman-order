<?php

use yii\grid\GridView;
use yii\helpers\Html;

/* @var $model dvizh\order\models\Cashbox */

/* @var $searchModel dvizh\order\models\cashbox\CashboxSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="row">
    <div class="col-xs-6">
        <?php
        $fiscalizeded = \dvizh\order\models\Order::find()->select('SUM(cost) AS cost')
            ->where(['workshift_id' => $model->id, 'is_fiscalized' => 1])->one();
        $notFiscalizeded = \dvizh\order\models\Order::find()->select('SUM(cost) AS cost')
            ->where(['workshift_id' => $model->id, 'is_fiscalized' => 0])->one();
        $fCost = $fiscalizeded->cost ? $fiscalizeded->cost : 0;
        $nfCost = $notFiscalizeded->cost ? $notFiscalizeded->cost : 0;
        ?>
        <h2>Чеки</h2>
        <h3>Сума(Фіскал./Не Фіскал.): <?= $fCost + $nfCost ?>(<?= $fCost ?> / <?= $nfCost ?>)</h3>
        <div class="table-responsive">
            <?php
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//                    'workshift_id',
                    'id',
//                    'cashbox_order_key',
                    'base_cost',
                    'cost',
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{view}',
                        'buttons' => [
                            'view' => function ($url, $model) {
                                return Html::a('<span class="glyphicon glyphicon-eye-open"></span>',
                                    ['/order/order/view', 'id' => $model->id], ['target' => '_blank']
                                );
                            },
                        ]
                    ],
                    [
                        'attribute' => 'is_fiscalized',
                        'label' => 'Фіскальний',
                        'value' => function ($model) {
                            return $model->is_fiscalized ? 'Так' : 'Ні';
                        }
                    ],
                    [
                        'attribute' => 'department_id',
                        'label' => 'Департамент',
                        'value' => function ($model) {
                            return $model->department_id;
                        }
                    ],
                    [
                        'label' => 'Url',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a("Переглянути", $model->fiscalized->qr, ['target' => '_blank']);
                        }
                    ]
                ],
            ]);
            ?>
        </div>
    </div>
    <div class="col-xs-6">
        <?php
        $orderReturns = \dvizh\order\models\OrderReturn::find()
            ->where(['workshift_id' => $model->id])
            ->all();
        $fCost = 0;
        $nfCost = 0;
        foreach ($orderReturns as $orderReturn) {
            if ($orderReturn->order->is_fiscalized) {
                $fCost += $orderReturn->getTotalPrice();
            } else {
                $nfCost += $orderReturn->getTotalPrice();
            }
        }
        ?>
        <h2>Повернення</h2>
        <h3>Сума(Фіскал./Не Фіскал.): <?= $fCost + $nfCost ?>(<?= $fCost ?> / <?= $nfCost ?>)</h3>
        <div class="table-responsive">
            <?php
            $dataProvider = new \yii\data\ActiveDataProvider([
                'query' => \dvizh\order\models\OrderReturn::find()->where(['workshift_id' => $model->id]),
            ]);
            echo GridView::widget([
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
//                    'workshift_id',
                    'id',
                    'date',
//                    [
//                        'attribute' => 'cashbox',
//                        'label' => yii::t('order', 'Cash Box'),
//                        'value' => function ($model) {
//                            return $model->getCashbox()->name;
//                        }
//                    ],
                    //5
                    [
                        'attribute' => 'user',
                        'label' => yii::t('order', 'Seller'),
                        'value' => function ($model) {
                            return $model->getUser()->username;
                        }
                    ],
                    'elementCount',
                    'description',

                    [
                        'label' => 'Фіскальний',
                        'value' => function ($model) {
                            return $model->order->is_fiscalized ? 'Так' : 'Ні';
                        }
                    ],
                    [
                        'label' => 'Url',
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::a("Переглянути", $model->fiscalized->qr, ['target' => '_blank']);
                        }
                    ]
                ],
            ]);
            ?>
        </div>
    </div>
</div>
