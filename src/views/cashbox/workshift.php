<?php

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use yii\grid\GridView;
use nex\datepicker\DatePicker;

use yii\helpers\ArrayHelper;

/* @var $model dvizh\order\models\Cashbox */
/* @var $tab dvizh\order\controllers\CashboxController */

$this->title = 'Робоча зміна #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('order', 'Orders'), 'url' => ['/order/default/index']];
$this->params['breadcrumbs'][] = ['label' => yii::t('order', 'Cash Boxes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cashbox->name, 'url' => ['view', 'id' => $model->cashbox->id]];
$this->params['breadcrumbs'][] = $this->title;

if(isset($tab)){
    $tab = 'main';
}
?>
<div class="cashbox-workshift">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="<?=($tab === 'main' ? 'active' : '')?>">
            <a href="#main" aria-controls="main" role="tab" data-toggle="tab">Головна</a>
        </li>
        <li role="presentation" class="<?=($tab === 'order' ? 'active' : '')?>">
            <a href="#order" aria-controls="order" role="tab" data-toggle="tab">Чеки</a>
        </li>
    </ul>

    <div class="tab-content">
        <div role="tabpanel" class="tab-pane <?=($tab === 'main' ? 'active' : '')?>" id="main">
            <div class="row">
                <?php echo $this->render('workshift/workshift-info', ['model' => $model]); ?>
                <?php echo $this->render('workshift/workshift-rro', ['model' => $model,]); ?>
            </div>
            <div class="row">
                <?php echo $this->render('workshift/deposit', ['model' => $model]); ?>
                <?php echo $this->render('workshift/withdrawal', ['model' => $model,]); ?>
            </div>
        </div>
        <div role="tabpanel" class="tab-pane <?=($tab === 'order' ? 'active' : '')?>" id="order">
            <?php
            $dataProvider = new \yii\data\ActiveDataProvider([
                'query' => \dvizh\order\models\Order::find()->where(['workshift_id' => $model->id]),
            ]);
            echo $this->render('workshift/order', [
                'model' => $model,
                'dataProvider' => $dataProvider,
            ]); ?>
        </div>
    </div>
</div>
