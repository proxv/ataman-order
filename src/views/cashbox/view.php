<?php

use Yii;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;
use nex\datepicker\DatePicker;

use yii\helpers\ArrayHelper;

///* @var $this yii\web\View */
/* @var $model dvizh\order\models\Cashbox */
/* @var $active_work_shift dvizh\order\models\Cashbox */
/* @var $statistic dvizh\order\controllers\CashboxController */
/* @var $dataProvider dvizh\order\controllers\CashboxController */
/* @var $searchModel dvizh\order\controllers\CashboxController */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('order', 'Orders'), 'url' => ['/order/default/index']];

$this->params['breadcrumbs'][] = ['label' => yii::t('order', 'Cash Boxes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;


if ($dateStart = yii::$app->request->get('date_start')) {
    $dateStart = date('Y-m-d', strtotime($dateStart));
}

if ($dateStop = yii::$app->request->get('date_stop')) {
    $dateStop = date('Y-m-d', strtotime($dateStop));
}

$timeStart = yii::$app->request->get('time_start');
$timeStop = yii::$app->request->get('time_stop');
?>
<div class="cashbox-view">
    <!-- Begin Flasher-->
    <div class="row">
        <div class="col-xs-12">
            <?php if (Yii::$app->session->hasFlash('success')) { ?>
                <div class="alert alert-success" role="alert">
                    <?= Yii::$app->session->getFlash('success') ?>
                </div>
            <?php } ?>
            <?php if (Yii::$app->session->hasFlash('warning')) { ?>
                <div class="alert alert-warning" role="alert">
                    <?= Yii::$app->session->getFlash('warning') ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <!-- End Flasher-->
    <!-- Begin Statistic-->
    <div class="statistic-widget">
        <?= \dvizh\order\widgets\CashboxStatistic::widget([
            'cashbox' => $model
        ]); ?>
    </div>
    <!-- End Statistic-->
    <!-- Start Date Search-->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><?= yii::t('order', 'Search by date'); ?></h3>
        </div>
        <div class="order-search-body">
            <div class="panel-body">
                <form action="<?= Url::toRoute(['/order/cashbox/view', 'id' => $model->id]); ?>" class="row search">
                    <?php
                    foreach (yii::$app->request->get() as $key => $value) {
                        if (!is_array($value)) {
                            echo Html::input('hidden', Html::encode($key), Html::encode($value));
                        }
                    }
                    ?>
                    <div class="col-md-4">
                        <label><?= yii::t('order', 'Date'); ?></label>
                        <div class="row">
                            <div class="col-md-6">
                                <?= DatePicker::widget([
                                    'name' => 'date_start',
                                    'addon' => false,
                                    'value' => $dateStart,
                                    'size' => 'sm',
                                    'language' => 'ru',
                                    'placeholder' => yii::t('order', 'Date from'),
                                    'options' => [
                                        'autocomplete' => 'off'
                                    ],
                                    'clientOptions' => [
                                        'format' => 'yyyy-MM-DD',
                                        'minDate' => '2015-01-01',
                                    ],
                                    'dropdownItems' => [
                                        ['label' => 'Yesterday', 'url' => '#', 'value' => \Yii::$app->formatter->asDate('-1 day')],
                                        ['label' => 'Tomorrow', 'url' => '#', 'value' => \Yii::$app->formatter->asDate('+1 day')],
                                        ['label' => 'Some value', 'url' => '#', 'value' => 'Special value'],
                                    ],
                                ]); ?>
                                <?php if ($timeStart && !yii::$app->request->get('OrderSearch')) { ?>
                                    <input type="hidden" name="time_start" value="<?= Html::encode($timeStart); ?>"/>
                                    <p><small><?= yii::t('order', 'Date from'); ?>
                                            : <?= Html::encode($timeStart); ?></small></p>
                                <?php } ?>
                            </div>
                            <div class="col-md-6">
                                <?= DatePicker::widget([
                                    'name' => 'date_stop',
                                    'addon' => false,
                                    'value' => $dateStop,
                                    'size' => 'sm',
                                    'placeholder' => yii::t('order', 'Date to'),
                                    'language' => 'ru',
                                    'options' => [
                                        'autocomplete' => 'off'
                                    ],
                                    'clientOptions' => [
                                        'format' => 'yyyy-MM-DD',
                                        'minDate' => '2015-01-01',
                                    ],
                                    'dropdownItems' => [
                                        ['label' => yii::t('order', 'Yesterday'), 'url' => '#', 'value' => \Yii::$app->formatter->asDate('-1 day')],
                                        ['label' => yii::t('order', 'Tomorrow'), 'url' => '#', 'value' => \Yii::$app->formatter->asDate('+1 day')],
                                        ['label' => yii::t('order', 'Some value'), 'url' => '#', 'value' => 'Special value'],
                                    ],
                                ]); ?>
                                <?php if ($timeStop && !yii::$app->request->get('OrderSearch')) { ?>
                                    <input type="hidden" name="time_stop" value="<?= Html::encode($timeStop); ?>"/>
                                    <p><small><?= yii::t('order', 'Date to'); ?>: <br/><?= Html::encode($timeStop); ?>
                                        </small></p>
                                <?php } ?>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <label><?= yii::t('order', 'Element name'); ?></label>
                        <?php if ($elementPossibleNames = \Yii::$app->getModule('order')->searchByElementNameArray) { ?>
                            <?= Html::dropDownList(
                                'elementName',
                                \Yii::$app->request->get('elementName'),
                                $elementPossibleNames,
                                [
                                    'class' => 'form-control',
                                    'prompt' => yii::t('order', 'Element name'),
                                ]);
                            ?>
                        <?php } ?>
                    </div>

                    <div class="col-md-2">
                        <label>&nbsp;</label>
                        <div>
                            <input class="btn btn-success" type="submit" value="<?= Yii::t('order', 'Search'); ?>"/>
                            <a href="<?= Url::toRoute(['/order/cashbox/view', 'id' => $model->id]); ?>"
                               class="btn btn-default">
                                <i class="glyphicon glyphicon-remove-sign"></i>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- End Date Search-->
    <div class="row">
        <div class="col-xs-6">
            <div class="row">
                <!--                Buttons-->
                <div class="col-xs-12">
                    <p>
                         <span>
            <?php echo Html::a(yii::t('order', 'Sold goods per day'), ['order-products', 'id' => $model->id], ['class' => 'btn btn-success']) ?>
        </span>
                        <!--                        --><?php
                        //                        echo Html::a('Заумовчанням', ['set-default', 'id' => $model->id], [
                        //                            'class' => 'btn btn-primary',
                        //                            'disabled' => ($model->user_id === yii::$app->user->id)
                        //                        ]);
                        //                        ?>
                        <?php echo Html::a(yii::t('order', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
                        <?php echo Html::a(yii::t('order', 'Delete'), ['delete', 'id' => $model->id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </p>
                </div>
                <?php echo $this->render('view/cashbox-info', [
                    'model' => $model
                ]); ?>
            </div>
        </div>
        <div class="col-xs-6">
            <?php echo $this->render('view/workshift-info', [
                'model' => $model,
                'dataProvider' => $dataProvider,
                'searchModel' => $searchModel,

                'active_work_shift' => $active_work_shift,
            ]); ?>
        </div>
    </div>
    <!--Table-->
    <?php echo $this->render('view/table', [
        'model' => $model,
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
    ]); ?>
</div>
