<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

///* @var $this yii\web\View */
/* @var $model common\modules\dvizh\order\models\Cashbox */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $can_set_as_default boolean */
?>

<div class="cashbox-form">
    <ul class="nav nav-tabs" role="tablist">
        <li role="presentation" class="active">
            <a href="#home" aria-controls="home" role="tab" data-toggle="tab">Головна</a>
        </li>
        <?php if (!$model->isNewRecord): ?>
            <li role="presentation">
                <?php echo Html::a('Департаменти', ['/openprro/prro-cashbox-department/index', 'cashbox' => $_GET['id']]); ?>
            </li>
        <?php endif; ?>
    </ul><!-- Tab panes -->
    <div class="tab-content">
        <div role="tabpanel" class="tab-pane active" id="home">
            <?php $form = ActiveForm::begin(); ?>

            <?php echo $form->errorSummary($model); ?>

            <?php echo $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'description')->textInput(['maxlength' => true]) ?>

            <?php echo $form->field($model, 'created_at')->hiddenInput()->label(false) ?>

            <?php echo $form->field($model, 'updated_at')->hiddenInput()->label(false) ?>

            <?php echo $form->field($model, 'is_rro_active')->checkbox([
                    'disabled' => isset($model->openWorkshift)
            ]); ?>

            <div class="form-group">
                <?php echo Html::submitButton($model->isNewRecord ? yii::t('order', 'Create') : yii::t('order', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>

</div>
