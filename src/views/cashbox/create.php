<?php

use yii\helpers\Html;


///* @var $this yii\web\View */
/* @var $model dvizh\order\models\Cashbox */
/* @var $can_set_as_default boolean */

$this->title = yii::t('order', 'Add Cash Box');
$this->params['breadcrumbs'][] = ['label' => Yii::t('order', 'Orders'), 'url' => ['/order/default/index']];

$this->params['breadcrumbs'][] = ['label' => yii::t('order', 'Cash Boxes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cashbox-create">

    <?php echo $this->render('_form', [
        'model' => $model,
        'can_set_as_default' => $can_set_as_default,
    ]) ?>

</div>
