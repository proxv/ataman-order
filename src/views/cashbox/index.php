<?php

use yii\helpers\Html;
use yii\grid\GridView;

///* @var $this yii\web\View */
/* @var $searchModel dvizh\order\models\cashbox\CashboxSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $statistic dvizh\order\controllers\CashboxController */

$this->title = yii::t('order', 'Cash Boxes');
$this->params['breadcrumbs'][] = ['label' => Yii::t('order', 'Orders'), 'url' => ['/order/default/index']];

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="cashbox-index">
    <!-- Begin Flasher-->
    <div class="row">
        <div class="col-xs-12">
            <?php if (Yii::$app->session->hasFlash('success')) { ?>
                <div class="alert alert-success" role="alert">
                    <?= Yii::$app->session->getFlash('success') ?>
                </div>
            <?php } ?>
            <?php if (Yii::$app->session->hasFlash('warning')) { ?>
                <div class="alert alert-warning" role="alert">
                    <?= Yii::$app->session->getFlash('warning') ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <!-- End Flasher-->
    <!-- Begin Statistic-->
    <div class="statistic-widget">
        <?= \dvizh\order\widgets\CashboxStatistic::widget([
            'cashbox' => null
        ]); ?>
    </div>
    <!-- End Statistic-->

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <span>
            
            <?php echo Html::a(yii::t('order', 'Add Cash Box'), ['create'], ['class' => 'btn btn-success']) ?>
        </span>
        <span>
            <?php echo Html::a(yii::t('order', 'Sold goods per day'), ['order-products'], ['class' => 'btn btn-success']) ?>
        </span>
    </p>

    <?php echo $this->render('index/table', [
        'dataProvider' => $dataProvider,
        'searchModel' => $searchModel,
    ]); ?>

</div>
