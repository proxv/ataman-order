<?php

use yii\helpers\Html;

///* @var $this yii\web\View */
/* @var $model dvizh\order\models\Cashbox */
/* @var $can_set_as_default boolean */


$this->title = yii::t('order', 'Update the cash box data') . ': ' . ' #' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('order', 'Orders'), 'url' => ['/order/default/index']];

$this->params['breadcrumbs'][] = ['label' => yii::t('order', 'Cash Boxes'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = yii::t('order', 'Update');
?>
<div class="cashbox-update">

    <?php echo $this->render('_form', [
        'model' => $model,
        'can_set_as_default' => $can_set_as_default,
    ]) ?>

</div>
