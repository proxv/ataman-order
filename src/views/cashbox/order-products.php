<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\GridView;
use nex\datepicker\DatePicker;
use kartik\export\ExportMenu;

///* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


if ($date = yii::$app->request->get('date')) {
    $date = date('Y-m-d', strtotime($date));
}

$this->title = yii::t('order', 'Sold goods per day');
$this->params['breadcrumbs'][] = ['label' => Yii::t('order', 'Orders'), 'url' => ['/order/default/index']];

$this->params['breadcrumbs'][] = ['label' => yii::t('order', 'Cash Boxes'), 'url' => ['index']];
if ($casbox_id = yii::$app->request->get('id')) {

    $this->params['breadcrumbs'][] = ['label' => '#' . $casbox_id, 'url' => ['view', 'id' => $casbox_id]];
}

$this->params['breadcrumbs'][] = $this->title;

?>
<div class="cashbox-order-products">
    <!--    <h2>--><? //='('.(!!$date ? $date : date('Y-m-d')) .')';?><!--</h2>-->
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title"><?= yii::t('order', 'Search by date'); ?></h3>
        </div>
        <div class="order-search-body">
            <div class="panel-body">
                <form action="<?= Url::toRoute(['/order/cashbox/order-products']); ?>" class="row search">
                    <?php
//                    foreach (yii::$app->request->get() as $key => $value) {
//                        if (!is_array($value)) {
//                            echo Html::input('hidden', Html::encode($key), Html::encode($value));
//                        }
//                    }
//                    var_dump($date);
                    ?>
                    <div class="col-md-4">
                        <label><?= yii::t('order', 'Date'); ?></label>
                        <div class="row">
                            <div class="col-md-6">
                                <?= DatePicker::widget([
                                    'name' => 'date',
                                    'addon' => false,
                                    'value' => isset($date) ? $date : date('Y-m-d'),
                                    'size' => 'sm',
                                    'language' => 'ru',
                                    'placeholder' => yii::t('order', 'Date from'),
                                    'options' => [
                                        'autocomplete' => 'off'
                                    ],
                                    'clientOptions' => [
                                        'format' => 'yyyy-MM-DD',
                                        'minDate' => '2015-01-01',
                                    ],
                                    'dropdownItems' => [
                                        ['label' => 'Yesterday', 'url' => '#', 'value' => \Yii::$app->formatter->asDate('-1 day')],
                                        ['label' => 'Tomorrow', 'url' => '#', 'value' => \Yii::$app->formatter->asDate('+1 day')],
                                        ['label' => 'Some value', 'url' => '#', 'value' => 'Special value'],
                                    ],
                                ]); ?>
                            </div>
                            <div class="col-md-6">
                                <div class="checkbox">
                                    <label>
                                        <?php
                                        $is_fiscalized = yii::$app->request->get('is_fiscalized') !== null;
//                                        var_dump($is_fiscalized);
                                        ?>
                                        <input type="checkbox"
                                               name="is_fiscalized" <?= $is_fiscalized ? 'checked' : '';?>> Фіскалізовані
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <label>&nbsp;</label>
                        <div>
                            <input class="btn btn-success" type="submit" value="<?= Yii::t('order', 'Search'); ?>"/>
                            <a href="<?= Url::toRoute(['/order/cashbox/order-products']); ?>" class="btn btn-default">
                                <i class="glyphicon glyphicon-remove-sign"></i>
                            </a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <?php
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'item_id',
            'value' => function ($model) {
                return $model->product->name;
            }
        ],
        [
            'attribute' => 'count',
            'value' => function ($model) {
                return round($model->count, 3);
            }
        ],
        'base_price',
        'price',
        [
            'label'=> 'Сума (Прихідна)',
            'attribute' => 'base_sum',
            'value' => function ($model) {
                return round($model->base_price * round($model->count, 3), 2);
            }
        ],
        [
            'label' => 'Сумма',
            'attribute' => 'sum',
            'value' => function ($model) {
                return round($model->price * round($model->count, 3), 2);
            }
        ],
    ];
    $fileName = (isset($date) && $date !== '' && count($date) > 0) ? $date: 'All';
    echo ExportMenu::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'filename' => 'ProductByDay-'.$fileName
    ]);
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns
    ]); ?>

</div>
