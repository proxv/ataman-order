<?php

use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

use kartik\date\DatePicker;
use kartik\range\RangeInput;
use yii\helpers\Html;

/* @var $this yii\web\View */
/** @var $model \dvizh\order\models\StatisticABC */
/** @var $dataProvider \yii\data\ActiveDataProvider */

?>
<div class="stat-abc">

    <?php

    $form = ActiveForm::begin(); ?>

    <div class="row">
        <div class="col-xs-12 col-md-4">
            <?php
            echo '<label class="form-label">Вибірка за дату</label>';

            echo \kartik\daterange\DateRangePicker::widget([
                'model' => $model,
                'attribute' => 'date_range',
                'convertFormat' => true,
                'readonly' => true,
                'includeMonthsFilter' => true,
                'initRangeExpr' => true,
                'pluginOptions' => [
                    'singleDatePicker' => false,
                    'showDropdowns' => true,
                    'locale' => ['format' => 'Y-m-d'],
                    'ranges' => [
                        'Сьогодні' => ["moment().startOf('day')", "moment()"],
                        'Учора' => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                        'Цей місяць' => ["moment().startOf('month')", "moment().endOf('month')"],
                        'Попередній місяць' => ["moment().subtract(1, 'month').startOf('month')", "moment().subtract(1, 'month').endOf('month')"],
                    ]
                ]
            ])
            ?>
        </div>
        <div class="col-xs-12 col-md-4">
            <?php
            echo $form->field($model, 'categories')
                ->label('Категорія')
                ->widget(\kartik\select2\Select2::classname(), [
                    'data' => $model->getAllCategories(),
                    'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                    'options' => [
                        'placeholder' => 'оберіть категорії ...',

                        'multiple' => true],
                    'pluginOptions' => [//                    'allowClear' => true
                    ],]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-4">
            <?php
            echo $form->field($model, 'range_ab')
                ->label('AB-Діапазон')
                ->widget(RangeInput::classname(), ['data' => $model->getAllCategories(),
                    'html5Container' => ['style' => 'width:70%'],
                    'html5Options' => ['min' => 0, 'max' => 100],
                    'addon' => ['append' => ['content' => '%']]]);
            ?>
        </div>
        <div class="col-xs-12 col-sm-6 col-md-4">
            <?php

            echo $form->field($model, 'range_bc')
                ->label('BC-Діапазон')
                ->widget(RangeInput::classname(), ['data' => $model->getAllCategories(),
                    'html5Container' => ['style' => 'width:70%'],
                    'html5Options' => ['min' => 0, 'max' => 100],
                    'addon' => ['append' => ['content' => '%']]]);
            ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-xs-6 col-md-4">
            <?php
            echo $form->field($model, 'parameter')
                ->label('Аналіз по параметру')
                ->widget(\kartik\select2\Select2::classname(), ['data' => $model->getAllParameters(),
                    'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                    'options' => ['placeholder' => 'оберіть параметр ...',
                        'multiple' => false],
                    'pluginOptions' => [//                    'allowClear' => true
                    ],]);
            ?>
        </div>
        <div class="col-xs-6 col-md-4">
            <?php
            echo $form->field($model, 'with_returned')
                ->label('Обрахунок з поверненнями')
                ->widget(\kartik\select2\Select2::classname(), ['data' => [1 => 'Так', 0 => 'Ні'],
                    'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                    'options' => ['placeholder' => 'оберіть параметр ...',
                        'multiple' => false],
                    'pluginOptions' => [//                    'allowClear' => true
                    ],]);
            ?>
        </div>
        <div class="col-xs-6 col-md-4">
            <br>
            <?php echo Html::submitButton('Згенерувати', ['class' => 'btn btn-primary',
                'onClick' => new \yii\web\JsExpression('(function(){
                $(this).html(`Оновлення...`);
//                $(this).prop(`disabled`,`disabled`)
                }).call(this)')]) ?>
        </div>
    </div>
    <?php
    ActiveForm::end();
    ?>
    <?php if ($model->validate()): ?>
        <div class="row">
            <div class="col-xs-12">
                <h3>Загальна сума: <strong><?= number_format($model->getTotalPrice(), 2, ',', ' '); ?></strong></h3>
            </div>
            <div class="col-xs-12">
                <?php
                static $prevProductPrecentage = 0;
                $totalPrice = $model->getTotalPrice();

                $columns = [//0
                    ['class' => 'yii\grid\SerialColumn'],
                    ['attribute' => 'productName',
                        'label' => 'Товар'
                    ],
//                    'productPriceAvg',
//                    'productCount',
//                    'productReturned',
                    ['attribute' => 'totalPrice',
                        'label' => 'Вартість(середня)'
                    ],
                    ['attribute' => 'precent',
                        'label' => '%(Загальної суми)',
                        'value' =>
                            function ($model) use ($totalPrice) {
                                $precent = ($model['totalPrice'] / $totalPrice) * 100;
                                return number_format($precent, 2);
                            }],
                    ['attribute' => 'precentTotal',
                        'label' => '%',
                        'value' =>
                            function ($mod) use ($totalPrice, $model) {
                                $precentTotal = $model->abc_temp += ($mod['totalPrice'] / $totalPrice) * 100;
                                return number_format($precentTotal, 2);
                            }
                    ],
                    ['attribute' => 'rang',
                        'label' => 'Ранг',
                        'format' => 'raw',
                        'value' =>
                            function ($mod) use ($totalPrice, $model) {
                                switch ($model->abc_temp) {
                                    case $model->abc_temp <= $model->range_ab:
                                        return 'A';
                                    //                                case $model->abc_temp > $model->range_ab && $model->abc_temp <= ((100 - $model->range_ab) / 100) * $model->range_bc:
                                    case $model->abc_temp > $model->range_ab && $model->abc_temp <= $model->range_ab + $model->range_bc:
                                        return 'B';
                                    default:
                                        return 'C';
                                }
                            }
                    ]
                ];
                echo \kartik\grid\GridView::widget(['dataProvider' => $dataProvider,
                    //                'filterModel' => $searchModel,
                    'columns' => $columns,
                    'responsiveWrap' => false,
                    'toolbar' => false,]);
                ?>
            </div>
        </div>
    <?php endif; ?>
</div>