<?php

use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;

use kartik\date\DatePicker;
use kartik\range\RangeInput;
use yii\helpers\Html;


/* @var $this yii\web\View */
/** @var $model \dvizh\order\models\StatisticUserProduct */
/** @var $dataProvider \yii\data\ActiveDataProvider */

?>
<div class="stat-abc">

    <?php
    $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-xs-12 col-md-4">
            <?= $form->field($model, 'user')
                ->widget(\kartik\select2\Select2::classname(), [
                    'data' => ArrayHelper::map(\common\models\User::find()->all(), 'id', 'username'),
                    'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                    'options' => [
                        'placeholder' => 'оберіть користувача ...',
                        'multiple' => false
                    ],
                    'pluginOptions' => [
                        //                    'allowClear' => true
                    ]
                ]); ?>
        </div>
        <div class="col-xs-12 col-md-4">
            <?php
            echo $form->field($model, 'products')
                ->widget(\kartik\select2\Select2::classname(), ['data' => ArrayHelper::map(\dvizh\shop\models\Product::find()->all(), 'id', 'name'),
                    'theme' => \kartik\select2\Select2::THEME_DEFAULT,
                    'options' => ['placeholder' => 'оберіть продукт ...',

                        'multiple' => true],
                    'pluginOptions' => [//                    'allowClear' => true
                    ],]);
            ?>
        </div>
        <div class="col-xs-12 col-md-4">
            <?php
            echo $form->field($model, 'date_range')->widget(\kartik\daterange\DateRangePicker::classname(), [
                'convertFormat' => true,
                'readonly' => true,
                'includeMonthsFilter' => true,
                'initRangeExpr' => true,
                'pluginOptions' => [
                    'singleDatePicker' => false,
                    'showDropdowns' => true,
                    'locale' => ['format' => 'Y-m-d'],
                    'ranges' => [
                        'Сьогодні' => ["moment().startOf('day')", "moment()"],
                        'Учора' => ["moment().startOf('day').subtract(1,'days')", "moment().endOf('day').subtract(1,'days')"],
                        'Цей місяць' => ["moment().startOf('month')", "moment().endOf('month')"],
                        'Попередній місяць' => ["moment().subtract(1, 'month').startOf('month')", "moment().subtract(1, 'month').endOf('month')"],
                    ]
                ],
                'pluginEvents' => [
                    'apply.daterangepicker' => 'function() {  }',
                    'cancel.daterangepicker' => 'function() {  }',
                ]
            ]);
            ?>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-xs-6 col-md-4">
            <br>
            <?php echo Html::submitButton('Згенерувати', ['class' => 'btn btn-primary']) ?>
        </div>
    </div>
    <?php
    ActiveForm::end();
    ?>
    <div class="row">
        <div class="col-xs-12">
            <?php
            if ($model->validate()) {
                $chartStatistic = $model->getUserProductQueryForChart()->all();
                $temp = [];
                foreach ($chartStatistic as $key => $value) {
                    $temp[$value['productId']][$value['saleDate']] = [
                        'name' => $value['productName'],
                        'count' => $value['productCount'],
                        'turnover' => $value['productTurnover'],
                        'profit' => $value['productProfit'],
                    ];

                }
                $tabs = [];
                foreach ($temp as $key => $dates) {
                    $tab = [
                        'id' => $key,
                        'dates' => [],
                        'count' => [],
                        'turnovers' => [],
                        'profits' => [],
                    ];

                    foreach ($dates as $date => $content) {
                        $tab['name'] = $content['name'];
                        $tab['dates'][] = $date;
                        $tab['count'][] = $content['count'];
                        $tab['turnover'][] = $content['turnover'];
                        $tab['profit'][] = $content['profit'];
                    }
                    $tabs[] = $tab;
                }
                ?>

                <div class="panel panel-primary">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <div class="row">
                            <div class="col-xs-12">
                                <h4 class="panel-title" style="line-height: 2em;">
                                    <a role="button" data-toggle="collapse" data-parent=".cashbox-statistic"
                                       href="#collapseOne"
                                       aria-expanded="false" aria-controls="collapseOne">
                                        Графік ...
                                    </a>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                        <div class="panel-body">
                            <ul class="nav nav-tabs" role="tablist">
                                <?php $active = true;
                                foreach ($tabs as $tab): ?>
                                    <li role="presentation" class="<?= $active ? 'active' : '' ?>">
                                        <a href="#statistic-tab_<?= ($tab['id']) ?>"
                                           role="tab"
                                           data-toggle="tab">
                                            <?= ($tab['name']) ?>
                                        </a>
                                    </li>
                                    <?php $active = false; endforeach; ?>
                            </ul>
                            <div class="tab-content">
                                <?php $active = true;
                                foreach ($tabs as $tab): ?>
                                    <div role="tabpanel" class="tab-pane <?= $active ? 'active' : '' ?>"
                                         id="statistic-tab_<?= ($tab['id']) ?>">
                                        <?= dosamigos\chartjs\ChartJs::widget([
                                            'type' => 'line',
                                            'clientOptions' => ['responsive' => true],
                                            'options' => [
                                                'height' => '100'
                                            ],
                                            'data' => [
                                                'labels' => $tab['dates'],
                                                'datasets' => [
                                                    [
                                                        'label' => "Кількість продаж",
                                                        'backgroundColor' => "rgba(179,181,198,0.2)",
                                                        'borderColor' => "rgba(179,181,198,1)",
                                                        'pointBackgroundColor' => "rgba(179,181,198,1)",
                                                        'pointBorderColor' => "#fff",
                                                        'pointHoverBackgroundColor' => "#fff",
                                                        'pointHoverBorderColor' => "rgba(179,181,198,1)",
                                                        'data' => $tab['count'],
                                                    ],
                                                    [
                                                        'label' => "Оборот",
                                                        'backgroundColor' => "rgba(255,99,132,0.2)",
                                                        'borderColor' => "rgba(255,99,132,1)",
                                                        'pointBackgroundColor' => "rgba(255,99,132,1)",
                                                        'pointBorderColor' => "#fff",
                                                        'pointHoverBackgroundColor' => "#fff",
                                                        'pointHoverBorderColor' => "rgba(255,99,132,1)",
                                                        'data' => $tab['turnover'],
                                                    ],
                                                    [
                                                        'label' => "Прибуток",
                                                        'backgroundColor' => "rgba(92, 99, 255,0.2)",
                                                        'borderColor' => "rgba(192, 99, 255, 1)",
                                                        'pointBackgroundColor' => "rgba(192, 99, 255, 1)",
                                                        'pointBorderColor' => "#fff",
                                                        'pointHoverBackgroundColor' => "#fff",
                                                        'pointHoverBorderColor' => "rgba(192, 99, 255, 1)",
                                                        'data' => $tab['profit'],
                                                    ]
                                                ]
                                            ]
                                        ]); ?>
                                    </div>
                                    <?php $active = false; endforeach; ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                $columns = [//0
                    ['class' => 'yii\grid\SerialColumn'],
                    [
                        'attribute' => 'productName',
                        'label' => 'Продукт',
                    ],
                    [
                        'attribute' => 'saleAmount',
                        'label' => 'Кількість проданого',
                    ],
                    [
                        'attribute' => 'turnover',
                        'label' => 'Оборот',
                    ],
                    [
                        'attribute' => 'profit',
                        'label' => 'Прибуток',
                    ],
                    [
                        'class' => 'yii\grid\ActionColumn',
                        'template' => '{link}',
                        'buttons' => [
                            'link' => function ($url, $model, $key) {
                                return \yii\bootstrap\Button::widget([
                                    'label' => 'Графік',
                                    'options' => [
                                        'type' => 'button',
                                        'class' => 'btn btn-primary',
                                        'data-toggle' => 'modal',
                                        'data-target' => '#myModal',
                                    ],
                                ]);
                            },
                        ],
                    ],
                ];
                echo \kartik\grid\GridView::widget([
                    'dataProvider' => $dataProvider,
                    //                'filterModel' => $searchModel,
                    'columns' => $columns,
                    'responsiveWrap' => false,
                    'toolbar' => false,
                ]);

            }
            ?>
        </div>
    </div>


    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Modal title</h4>
                </div>
                <div class="modal-body">
                    <?php
                    echo dosamigos\chartjs\ChartJs::widget([
                        'type' => 'bar',
                        'clientOptions' => ['responsive' => true],
                        'options' => [
                            'height' => '100'
                        ],
                        'data' => [
                            'labels' => ['qwe', 'qweee'],
                            'datasets' => [
                                [
                                    'label' => "Оборот",
                                    'backgroundColor' => "rgba(179,181,198,0.8)",
                                    'borderColor' => "rgba(179,181,198,1)",
                                    'pointBackgroundColor' => "rgba(179,181,198,1)",
                                    'pointBorderColor' => "#fff",
                                    'pointHoverBackgroundColor' => "#fff",
                                    'pointHoverBorderColor' => "rgba(179,181,198,1)",
                                    'data' => [10, 20, 30, 40, 50, 10, 50]
                                ],
                            ]
                        ]
                    ]); ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
</div>