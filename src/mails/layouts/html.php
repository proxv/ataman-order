<?php

/**
 * @var $this yii\web\View
 * @var $content \yii\mail\BaseMessage
 */
use yii\helpers\Html;

$this->beginPage();
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml" lang="uk">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>"/>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head(); ?>
        <style type="text/css">
            body, td, th {
                font-family: Arial;
            }
        </style>
    </head>
    <body>
    <?php $this->beginBody(); ?>
    <?= $content ?>
    <?php $this->endBody(); ?>
    </body>
    </html>
<?php $this->endPage(); ?>