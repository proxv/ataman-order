<?php

namespace dvizh\order;

use yii\base\BootstrapInterface;
use yii;

/**
 * Class Bootstrap
 * @package dvizh\order
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * @param $app
     */
    public function bootstrap($app)
    {
        if (!$app->has('order')) {
            $app->set('order', ['class' => 'dvizh\order\Order']);
        }

        if (empty($app->modules['gridview'])) {
            $app->setModule('gridview', [
                'class' => '\kartik\grid\Module',
            ]);
        }

        if (!isset($app->i18n->translations['order']) && !isset($app->i18n->translations['order*'])) {
            $app->i18n->translations['order'] = [
                'class' => 'yii\i18n\PhpMessageSource',
                'basePath' => __DIR__ . '/messages',
                'forceTranslation' => true
            ];
        }
    }
}
