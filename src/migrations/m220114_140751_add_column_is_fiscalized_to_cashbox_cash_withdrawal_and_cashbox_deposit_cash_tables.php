<?php

use yii\db\Migration;

/**
 * Class m220114_140751_add_column_is_fiscalized_to_cashbox_cash_withdrawal_and_cashbox_deposit_cash_tables
 */
class m220114_140751_add_column_is_fiscalized_to_cashbox_cash_withdrawal_and_cashbox_deposit_cash_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%cashbox_cash_withdrawal}}', 'is_fiscalized', $this->boolean());
        $this->addColumn('{{%cashbox_deposit_cash}}', 'is_fiscalized', $this->boolean());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%cashbox_deposit_cash}}', 'is_fiscalized');
        $this->dropColumn('{{%cashbox_cash_withdrawal}}', 'is_fiscalized');
    }
}
