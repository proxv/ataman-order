<?php

use yii\db\Migration;

/**
 * Class m210820_113910_add_cashbox_order_key_field_to_order_table
 */
class m210820_113910_add_cashbox_order_key_field_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'cashbox_order_key', $this->string(255)->notNull()->defaultvalue(''));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'cashbox_order_key');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210820_113910_add_cashbox_order_key_field_to_order_table cannot be reverted.\n";

        return false;
    }
    */
}
