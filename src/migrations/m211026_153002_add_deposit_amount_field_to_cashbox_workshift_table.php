<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m211026_153002_add_deposit_amount_field_to_cashbox_workshift_table
 */
class m211026_153002_add_deposit_amount_field_to_cashbox_workshift_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cashbox_workshift}}', 'deposit_amount', Schema::TYPE_DECIMAL . "(11, 2)");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%cashbox_workshift}}', 'deposit_amount');

        return true;
    }
}
