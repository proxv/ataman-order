<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cashbox_workshift}}`.
 */
class m210708_125555_create_cashbox_workshift_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cashbox_workshift}}', [
            'id' => $this->primaryKey(),
            'cashbox_id' => $this->integer()->notNull(),
            'user_id' => $this->integer()->notNull(),

            'crete_at' => $this->timestamp()->notNull(),
            'update_at' => $this->timestamp(),
            'closed_at' => $this->timestamp(),

            'open_amount' => $this->float()->notNull()->defaultValue(0),
            'cash_amount' => $this->float()->notNull()->defaultValue(0),
            'card_amount' => $this->float()->notNull()->defaultValue(0),
            'close_amount' => $this->float()->notNull()->defaultValue(0),

            'is_closed' => $this->boolean()->notNull()->defaultValue(0)
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cashbox_workshift}}');
    }
}
