<?php

use yii\db\Migration;
use yii\db\Schema;


/**
 * Class m210726_111828_create_cashbox_cash_withdrawal
 */
class m210726_111828_create_cashbox_cash_withdrawal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cashbox_cash_withdrawal}}', [
            'id' => $this->primaryKey(),
            'date' => $this->dateTime()->notNull(),
            'workshift_id' => $this->integer(11)->notNull(),
            'amount' => Schema::TYPE_DECIMAL . "(11, 2) NOT NULL DEFAULT 0",
            'description' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cashbox_cash_withdrawal}}');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210726_111828_create_cashbox_cash_withdrawal cannot be reverted.\n";

        return false;
    }
    */
}
