<?php

use yii\db\Migration;

/**
 * Class m210722_140133_add_field_workshift_id_to_order_return_table
 */
class m210722_140133_add_field_workshift_id_to_order_return_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%order_return}}', 'workshift_id', $this->integer(11)->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order_return}}', 'workshift_id');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210722_140133_add_field_workshift_id_to_order_return_table cannot be reverted.\n";

        return false;
    }
    */
}
