<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `{{%order_pay_system}}`.
 */
class m211229_150503_add_is_send_advance_to_cashbox_workshift_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%cashbox_workshift}}', 'is_send_advance', $this->boolean()->notNull());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%cashbox_workshift}}', 'is_send_advance');
    }
}
