<?php

use yii\db\Migration;

/**
 * Class m211026_132053_add_new_payment_to_payment_type_table
 */
class m211026_132053_add_new_payment_to_payment_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%order_payment_type}}', [
            'id' => '4',
            'name' => 'Суміжний',
            'slug' => 'cash_and_card',
            'widget' => '',
            'order' => NULL,
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%order_payment_type}}', ['id' => 4]);

    }
}
