<?php

use yii\db\Migration;

/**
 * Class m210709_080912_create_workshift_id_field
 */
class m210709_080912_create_workshift_id_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'workshift_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'workshift_id');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210709_080912_create_workshift_id_field cannot be reverted.\n";

        return false;
    }
    */
}
