<?php

use yii\db\Migration;

/**
 * Class m210719_124316_add_closed_by_user_id_to_cashbox_workshift_table
 */
class m210719_124316_add_closed_by_user_id_to_cashbox_workshift_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%cashbox_workshift}}', 'closed_by_user_id', $this->integer());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%cashbox_workshift}}', 'closed_by_user_id');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210719_124316_add_closed_by_user_id_to_cashbox_workshift_table cannot be reverted.\n";

        return false;
    }
    */
}
