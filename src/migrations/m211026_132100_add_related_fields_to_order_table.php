<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m211026_132100_add_related_fields_to_order_table
 */
class m211026_132100_add_related_fields_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'cash_amount', Schema::TYPE_DECIMAL . "(11, 2) DEFAULT 0");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'cash_amount');
    }
}
