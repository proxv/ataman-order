<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m210726_082115_change_float_fields_as_decimal
 */
class m210726_082115_change_float_fields_as_decimal extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%cashbox_workshift}}', 'open_amount', Schema::TYPE_DECIMAL . "(11, 2) NOT NULL DEFAULT 0");
        $this->alterColumn('{{%cashbox_workshift}}', 'cash_amount', Schema::TYPE_DECIMAL . "(11, 2) NOT NULL DEFAULT 0");
        $this->alterColumn('{{%cashbox_workshift}}', 'card_amount', Schema::TYPE_DECIMAL . "(11, 2) NOT NULL DEFAULT 0");
        $this->alterColumn('{{%cashbox_workshift}}', 'close_amount', Schema::TYPE_DECIMAL . "(11, 2) NOT NULL DEFAULT 0");
        $this->alterColumn('{{%cashbox_workshift}}', 'return_amount', Schema::TYPE_DECIMAL . "(11, 2) DEFAULT 0");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%cashbox_workshift}}', 'open_amount', $this->float()->notNull()->defaultValue(0));
        $this->alterColumn('{{%cashbox_workshift}}', 'cash_amount', $this->float()->notNull()->defaultValue(0));
        $this->alterColumn('{{%cashbox_workshift}}', 'card_amount', $this->float()->notNull()->defaultValue(0));
        $this->alterColumn('{{%cashbox_workshift}}', 'close_amount', $this->float()->notNull()->defaultValue(0));
        $this->alterColumn('{{%cashbox_workshift}}', 'return_amount', $this->float()->defaultValue(0));

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210726_082115_change_float_fields_as_decimal cannot be reverted.\n";

        return false;
    }
    */
}
