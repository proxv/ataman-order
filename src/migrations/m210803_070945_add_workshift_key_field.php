<?php

use yii\db\Migration;

/**
 * Class m210803_070945_add_workshift_key_field
 */
class m210803_070945_add_workshift_key_field extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%cashbox_workshift}}', 'key', $this->string(255)->notNull());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%cashbox_workshift}}', 'key');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210803_070945_add_workshift_key_field cannot be reverted.\n";

        return false;
    }
    */
}
