<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles adding columns to table `{{%order_pay_system}}`.
 */
class m220104_131234_change_columns_on_order_pay_system_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('order_pay_system', 'tax_num', $this->string(64));
        $this->alterColumn('order_pay_system', 'name', $this->string(255));
        $this->alterColumn('order_pay_system', 'acquire_pn', $this->string(64));
        $this->alterColumn('order_pay_system', 'acquire_nm', $this->string(255));
        $this->alterColumn('order_pay_system', 'acquire_trans_id', $this->string(128));
        $this->alterColumn('order_pay_system', 'pos_trans_date', $this->string(255));
        $this->alterColumn('order_pay_system', 'pos_trans_num', $this->string(128));
        $this->alterColumn('order_pay_system', 'device_id', $this->string(128));
        $this->alterColumn('order_pay_system', 'epz_details', $this->string(128));
        $this->alterColumn('order_pay_system', 'auth_cd', $this->string(64));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('order_pay_system', 'tax_num', $this->string(64)->notNull());
        $this->alterColumn('order_pay_system', 'name', $this->string(255)->notNull());
        $this->alterColumn('order_pay_system', 'acquire_pn', $this->string(64)->notNull());
        $this->alterColumn('order_pay_system', 'acquire_nm', $this->string(255)->notNull());
        $this->alterColumn('order_pay_system', 'acquire_trans_id', $this->string(128)->notNull());
        $this->alterColumn('order_pay_system', 'pos_trans_date', $this->string(255)->notNull());
        $this->alterColumn('order_pay_system', 'pos_trans_num', $this->string(128)->notNull());
        $this->alterColumn('order_pay_system', 'device_id', $this->string(128)->notNull());
        $this->alterColumn('order_pay_system', 'epz_details', $this->string(128)->notNull());
        $this->alterColumn('order_pay_system', 'auth_cd', $this->string(64)->notNull());
    }
}
