<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m211229_150023_add_fiskalization_and_payment_fields_to_order_table
 */
class m211229_150023_add_fiskalization_and_payment_fields_to_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%order}}', 'is_fiscalized', $this->boolean()->notNull());
        $this->addColumn('{{%order}}', 'provided', Schema::TYPE_DECIMAL . "(11, 2)");
        $this->addColumn('{{%order}}', 'remains', Schema::TYPE_DECIMAL . "(11, 2)");
        $this->addColumn('{{%order}}', 'sum', Schema::TYPE_DECIMAL . "(11, 2)");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'is_fiscalized');
        $this->dropColumn('{{%order}}', 'provided');
        $this->dropColumn('{{%order}}', 'remains');
        $this->dropColumn('{{%order}}', 'sum');
    }
}
