<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order_return_element}}`.
 */
class m210721_102322_create_order_return_element_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order_return_element}}', [
            'id' => $this->primaryKey(),
            'return_id' => $this->integer(11)->notNull(),
            'item_id' => $this->integer(11)->notNull(),
            'count' => $this->float()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_return_element}}');
    }
}
