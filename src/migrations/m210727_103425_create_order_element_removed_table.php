<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order_element_removed}}`.
 */
class m210727_103425_create_order_element_removed_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order_element_removed}}', [
            'id' => $this->primaryKey(),
            'date' => $this->dateTime()->notNull(),
            'workshift_id' => $this->integer(11)->notNull(),
            'user_id' => $this->integer(11)->notNull(),
            'item_id' => $this->integer(11)->notNull(),
            'count' => $this->float()->notNull()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_element_removed}}');
    }
}
