<?php

use yii\db\Migration;

/**
 * Class m220316_122349_add_is_rro_active_column_to_cashbox_table
 */
class m220316_122349_add_is_rro_active_column_to_cashbox_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%cashbox}}', 'is_rro_active', $this->boolean()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%cashbox}}', 'is_rro_active');

    }
}
