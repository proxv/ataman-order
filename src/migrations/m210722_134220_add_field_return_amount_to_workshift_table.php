<?php

use yii\db\Migration;

/**
 * Class m210722_134220_add_field_return_amount_to_workshift_table
 */
class m210722_134220_add_field_return_amount_to_workshift_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%cashbox_workshift}}', 'return_amount', $this->float());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%cashbox_workshift}}', 'return_amount');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210722_134220_add_field_return_amount_to_workshift_table cannot be reverted.\n";

        return false;
    }
    */
}
