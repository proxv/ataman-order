<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Class m210726_132600_add_withdrawal_amount_field_to_cashbox_workshift_table
 */
class m210726_132600_add_withdrawal_amount_field_to_cashbox_workshift_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%cashbox_workshift}}', 'withdrawal_amount', Schema::TYPE_DECIMAL . "(11, 2)");
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%cashbox_workshift}}', 'withdrawal_amount');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210726_132600_add_withdrawal_amount_field_to_cashbox_workshift_table cannot be reverted.\n";

        return false;
    }
    */
}
