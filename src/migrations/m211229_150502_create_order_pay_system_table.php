<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `{{%order_pay_system}}`.
 */
class m211229_150502_create_order_pay_system_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order_pay_system}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(11)->notNull(),

            'tax_num' => $this->string(64)->notNull(),
            'name' => $this->string(255)->notNull(),
            'acquire_pn' => $this->string(64)->notNull(),
            'acquire_nm' => $this->string(255)->notNull(),
            'acquire_trans_id' => $this->string(128)->notNull(),
            'pos_trans_date' => $this->string(255)->notNull(),
            'pos_trans_num' => $this->string(128)->notNull(),
            'device_id' => $this->string(128)->notNull(),
            'epz_details' => $this->string(128)->notNull(),
            'auth_cd' => $this->string(64)->notNull(),
            'sum' => Schema::TYPE_DECIMAL . "(11, 2)",
            'commission' => Schema::TYPE_DECIMAL . "(11, 2)",
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_pay_system}}');
    }
}
