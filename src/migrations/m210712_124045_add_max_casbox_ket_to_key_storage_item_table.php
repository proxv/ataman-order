<?php

use yii\db\Migration;

/**
 * Class m210712_124045_add_max_casbox_ket_to_key_storage_item_table
 */
class m210712_124045_add_max_casbox_ket_to_key_storage_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->insert('{{%key_storage_item}}', [
            'key' => 'backend.order-max-cashbox',
            'value' => '1',
            'comment' => 'Max Cash Box quantity for shop'
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%key_storage_item}}', [
            'key' => 'backend.order-max-cashbox'
        ]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210712_124045_add_max_casbox_ket_to_key_storage_item_table cannot be reverted.\n";

        return false;
    }
    */
}
