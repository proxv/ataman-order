<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cashbox}}`.
 */
class m210708_092924_create_cashbox_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cashbox}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->string(255),
            'user_id' => $this->integer(),
//            'work_shift_is_open' => $this->boolean()->notNull()->defaultValue(0),
//            'work_shift_amount' => $this->float()->notNull()->defaultValue(0),
//            'work_shift_start_at' => $this->timestamp()->notNull(),
//            'work_shift_end_at' => $this->timestamp(),
//            'work_shift_cash_amount' => $this->float()->defaultValue(0),
//            'work_shift_card_amount' => $this->float()->defaultValue(0),
            'created_at' => $this->timestamp()->notNull(),
            'updated_at' => $this->timestamp()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cashbox}}');
    }
}
