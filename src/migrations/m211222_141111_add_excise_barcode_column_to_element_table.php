<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%element}}`.
 */
class m211222_141111_add_excise_barcode_column_to_element_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%order_element}}', 'excise_barcode', $this->string(255));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%order_element}}', 'excise_barcode');
    }
}
