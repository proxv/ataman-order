<?php

use yii\db\Migration;

/**
 * Class m210820_093255_change_count_field_to_float_in_order_element_table
 */
class m210820_093255_change_count_field_to_float_in_order_element_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('{{%order_element}}', 'count', $this->float()->notNull()->defaultValue(0));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->alterColumn('{{%order_element}}', 'count', $this->integer(11)->notNull());
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210820_093255_change_count_field_to_float_in_order_element_table cannot be reverted.\n";

        return false;
    }
    */
}
