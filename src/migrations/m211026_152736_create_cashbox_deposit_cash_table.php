<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `{{%cashbox_deposit_cash}}`.
 */
class m211026_152736_create_cashbox_deposit_cash_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cashbox_deposit_cash}}', [
            'id' => $this->primaryKey(),
            'date' => $this->dateTime()->notNull(),
            'workshift_id' => $this->integer(11)->notNull(),
            'amount' => Schema::TYPE_DECIMAL . "(11, 2) NOT NULL DEFAULT 0",
            'description' => $this->text(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cashbox_deposit_cash}}');
    }
}
