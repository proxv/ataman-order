<?php

namespace dvizh\order\interfaces;

/**
 * Interface OrderElement
 * @package dvizh\order\interfaces
 */
interface OrderElement
{
    /**
     * @return mixed
     */
    public function getOrderId();

    /**
     * @return mixed
     */
    public function getAssigment();

    /**
     * @return mixed
     */
    public function getModelName();

    /**
     * @return mixed
     */
    public function getName();

    /**
     * @return mixed
     */
    public function getItemId();

    /**
     * @return mixed
     */
    public function getCount();

    /**
     * @return mixed
     */
    public function getBasePrice();

    /**
     * @return mixed
     */
    public function getPrice();

    /**
     * @return mixed
     */
    public function getOptions();

    /**
     * @return mixed
     */
    public function getDescription();

    /**
     * @param $orderId
     * @return mixed
     */
    public function setOrderId($orderId);

    /**
     * @param $isAssigment
     * @return mixed
     */
    public function setAssigment($isAssigment);

    /**
     * @param $modelName
     * @return mixed
     */
    public function setModelName($modelName);

    /**
     * @param $name
     * @return mixed
     */
    public function setName($name);

    /**
     * @param $itemId
     * @return mixed
     */
    public function setItemId($itemId);

    /**
     * @param $count
     * @return mixed
     */
    public function setCount($count);

    /**
     * @param $basePrice
     * @return mixed
     */
    public function setBasePrice($basePrice);

    /**
     * @param $price
     * @return mixed
     */
    public function setPrice($price);

    /**
     * @param $options
     * @return mixed
     */
    public function setOptions($options);

    /**
     * @param $description
     * @return mixed
     */
    public function setDescription($description);

    /**
     * @return mixed
     */
    public function saveData();
}
