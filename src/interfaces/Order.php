<?php

namespace dvizh\order\interfaces;

/**
 * Interface Order
 * @package dvizh\order\interfaces
 */
interface Order
{
    /**
     * @param $deleted
     * @return mixed
     */
    public function setDeleted($deleted);

    /**
     * @param $status
     * @return mixed
     */
    public function setStatus($status);

    /**
     * @return mixed
     */
    public function saveData();
}
