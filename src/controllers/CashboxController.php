<?php

namespace dvizh\order\controllers;

use dvizh\order\models\CashboxWorkshift;
use dvizh\order\models\Element;
use dvizh\order\models\tools\CashboxWorkshiftSearch;
use Yii;
use dvizh\order\models\Cashbox;
use dvizh\order\models\tools\CashboxSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * CashboxController implements the CRUD actions for Cashbox model.
 */
class CashboxController extends Controller
{
    /**
     * @return array[]
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @param $keys
     * @param $datas
     * @param string[] $fields
     * @return array
     */
    private function fillData($keys, $datas, $fields = ['total', 'profit'])
    {
        $result = [];

        foreach ($keys as $keyIndex => $keyValue) {
            foreach ($fields as $f) {
                $result[$f][$keyIndex] = $datas[$keyValue][$f] ? (integer)$datas[$keyValue][$f] : 0;
            }
        }
        return $result;
    }

    public function actionWorkshift($id,$tab = 'main'){
        $model = CashboxWorkshift::find()->where(['id' => $id])->one();
//        var_dump($model);
        return $this->render('workshift', [
            'model' => $model,
            'tab' => $tab,
        ]);
    }
    /**
     * Lists all Cashbox models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CashboxSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cashbox model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        $searchModel = new CashboxWorkshiftSearch();

        $params = yii::$app->request->queryParams;

        if (empty($params['CashboxWorkshiftSearch'])) {
            $params = [
                'CashboxWorkshiftSearch' => [
                    'cashbox_id' => $model->id,
                    'is_closed' => 1
                ]
            ];
        }

        $dataProvider = $searchModel->search($params);

        $this->setCustomQueryParams($dataProvider->query);
        $dataProvider->query->orderBy('update_at DESC');

        $active_work_shift = $model->getWorkshift()->where(['is_closed' => 0])->one();

        return $this->render('view', [
            'model' => $model,
            'active_work_shift' => $active_work_shift,

            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * @return null
     */
    private function getStatHours()
    {
        $keyStorage = new \common\components\keyStorage\KeyStorage();
        $key = 'backend.order-statistic-hours';

        if (!$keyStorage->has($key)) {
            return null;
        }

        $hours = $keyStorage->get($key);

        if ($hours) {
            return $hours;
        } else {
            return null;
        }
    }

    /**
     * Creates a new Cashbox model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Cashbox();

        $total = $model->getCashboxesCount();
        $max = $model->getMaxCashboxes();

        if ($total >= $max) {
            yii::$app->session->setFlash('warning', 'Ваш ліміт ' . $max . ' Каси!');
            return $this->redirect(['index']);
        }

        $post = Yii::$app->request->post();

        if ($model->load($post) && $model->save()) {
            yii::$app->session->setFlash('success', 'Каса створена!');

            return $this->redirect(['index']);
        } else {
            $model->created_at = $model->updated_at = date("Y-m-d H:m:s");

            return $this->render('create', [
                'model' => $model,
                'can_set_as_default' => false,
            ]);
        }
    }

    /**
     * Updates an existing Cashbox model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'can_set_as_default' => true,
            ]);
        }
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionSetDefault($id)
    {
        $model = $this->findModel($id);
        $currentUserId = yii::$app->user->id;

        if ($model && is_null($model->user_id)) {
            $cash_box = Cashbox::find()->where(['user_id' => $currentUserId])->one();
            if ($cash_box) {
                $cash_box->user_id = null;
                $cash_box->save();
            }

            $model->user_id = $currentUserId;
            $model->save();
            yii::$app->session->setFlash('success', 'Success');

        } else {
            yii::$app->session->setFlash('warning', 'Error');
        }
        return $this->redirect(['view', 'id' => $model->id]);
    }

    /**
     * Deletes an existing Cashbox model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cashbox model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Cashbox the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Cashbox::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $query
     */
    protected function setCustomQueryParams($query)
    {

        //По дате заказа
        if ($dateStart = yii::$app->request->get('date_start')) {
            $dateStop = yii::$app->request->get('date_stop');
            if ($dateStop) {
                $dateStop = date('Y-m-d', strtotime($dateStop));
            }

            $dateStart = date('Y-m-d', strtotime($dateStart));

            if ($dateStart == $dateStop) {
                $query->andWhere(['DATE_FORMAT(crete_at, "%Y-%m-%d")' => $dateStart]);
            } else {
                if (!$dateStop) {
                    $query->andWhere('DATE_FORMAT(crete_at, "%Y-%m-%d") = :dateStart', [':dateStart' => $dateStart]);
                } else {
                    $query->andWhere('crete_at >= :dateStart', [':dateStart' => $dateStart]);
                    $query->andWhere('crete_at <= :dateStop', [':dateStop' => $dateStop]);
                }
            }
        }

        //По времени заказа
        if ($timeStart = yii::$app->request->get('time_start')) {
            $query->andWhere('crete_at >= :timeStart', [':timeStart' => $timeStart]);
        }

        if ($timeStop = yii::$app->request->get('time_stop')) {
            if (urldecode($timeStop) == '0000-00-00 00:00:00') {
                $timeStop = date('Y-m-d H:i:s');
            }
            $query->andWhere('crete_at <= :timeStop', [':timeStop' => $timeStop]);
        }
    }

    /**
     * @return mixed
     */
    public function actionOrderProducts()
    {
        // date
        $query = Element::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        if ($cashbox_id = yii::$app->request->get('id')) {
            $cashbox = Cashbox::findOne($cashbox_id);
            $workshift = $cashbox->getWorkshift()->select('id')->all();

            $workShiftIds = array_map(function ($a) {
                return $a->id;
            }, $workshift);

            $query->andWhere(['in', '{{%order}}.workshift_id', $workShiftIds]);
        }

        if ($date = yii::$app->request->get('date')) {
            $query->andWhere(['DATE_FORMAT(date, "%Y-%m-%d")' => $date]);
        } else {
            $query->andWhere('WEEKDAY({{%order}}.date) = WEEKDAY(NOW())');
        }

        if($is_fiscalized = yii::$app->request->get('is_fiscalized')){
            $query->andWhere('{{%order}}.is_fiscalized = 1');
        }

        $query->orderBy('{{%order}}.date DESC');

        $query->leftJoin('{{%order}}', '{{%order_element}}.order_id = {{%order}}.id');

        $query->addSelect('{{%order_element}}.item_id');
        $query->addSelect('{{%order_element}}.model');
        $query->addSelect('SUM({{%order_element}}.count) as count');
        $query->addSelect('({{%order_element}}.price) as price');
        $query->addSelect('({{%order_element}}.base_price) as base_price');
        $query->groupBy('item_id');


        return $this->render('order-products', [
            'dataProvider' => $dataProvider,
        ]);
    }
}
