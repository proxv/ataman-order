<?php

namespace dvizh\order\controllers;

use yii;
use dvizh\order\models\FieldValueVariant;
use yii\web\Controller;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\NotFoundHttpException;

/**
 * Class FieldValueVariantController
 * @package dvizh\order\controllers
 */
class FieldValueVariantController extends Controller
{
    /**
     * @return array[]
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => $this->module->adminRoles,
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                    'edittable' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FieldValueVariant();

        $model->load(yii::$app->request->post());
        $model->save();

        return $this->redirect(yii::$app->request->referrer);
    }

    /**
     * @param intager $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(yii::$app->request->referrer);
    }

    /**
     *
     */
    public function actionEditable()
    {
        $name = yii::$app->request->post('name');
        $value = yii::$app->request->post('value');
        $pk = unserialize(base64_decode(yii::$app->request->post('pk')));
        FieldValueVariant::editField($pk, $name, $value);
    }

    /**
     * @param integer $id
     * @return FieldValueVariant
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        if (($model = FieldValueVariant::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested variant does not exist.');
        }
    }
}
