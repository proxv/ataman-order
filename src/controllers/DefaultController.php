<?php

namespace dvizh\order\controllers;

use yii;
use yii\web\Controller;
use yii\filters\AccessControl;

/**
 * Class DefaultController
 * @package dvizh\order\controllers
 */
class DefaultController extends Controller
{
    /**
     * @return array[]
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => $this->module->adminRoles,
                    ]
                ]
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
}
