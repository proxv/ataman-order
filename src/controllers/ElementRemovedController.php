<?php

namespace dvizh\order\controllers;

use dvizh\order\models\CashWithdrawal;
use dvizh\order\models\ElementRemoved;
use dvizh\order\models\tools\CashWithdrawalsSearch;
use dvizh\order\models\tools\ElementRemovedSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;


/**
 * Class ElementRemovedController
 * @package dvizh\order\controllers
 */
class ElementRemovedController extends Controller
{
    /**
     * @return array[]
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * Lists all ElementRemoved models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ElementRemovedSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the ElementRemoved model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ElementRemoved the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ElementRemoved::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

}
