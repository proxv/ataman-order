<?php

namespace dvizh\order\controllers;

use dvizh\order\models\OrderReturn;
use dvizh\order\models\OrderReturnElement;
use dvizh\order\models\tools\OrderReturnSearch;
use dvizh\order\models\tools\OrderReturnElementSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\data\ActiveDataProvider;


/**
 * ReturnController implements the CRUD actions for OrderReturn model.
 */
class ReturnController extends Controller
{
    /**
     * @return array[]
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all OrderReturn models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new OrderReturnSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $this->setCustomQueryParams($dataProvider->query);
        $dataProvider->query->orderBy('date DESC');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single OrderReturn model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);
        $searchModel = new OrderReturnElementSearch();
        $params = yii::$app->request->queryParams;
        $params['OrderReturnElementSearch']['return_id'] = $model->id;
        $dataProvider = $searchModel->search($params);

        return $this->render('view', [
            'model' => $model,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Finds the OrderReturn model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrderReturn the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrderReturn::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * @param $query
     */
    protected function setCustomQueryParams($query)
    {
        //По дате заказа
        if ($dateStart = yii::$app->request->get('date_start')) {
            $dateStop = yii::$app->request->get('date_stop');
            if ($dateStop) {
                $dateStop = date('Y-m-d', strtotime($dateStop));
            }

            $dateStart = date('Y-m-d', strtotime($dateStart));

            if ($dateStart == $dateStop) {
                $query->andWhere(['DATE_FORMAT(date, "%Y-%m-%d")' => $dateStart]);
            } else {
                if (!$dateStop) {
                    $query->andWhere('DATE_FORMAT(date, "%Y-%m-%d") = :dateStart', [':dateStart' => $dateStart]);
                } else {
                    $query->andWhere('date >= :dateStart', [':dateStart' => $dateStart]);
                    $query->andWhere('date <= :dateStop', [':dateStop' => $dateStop]);
                }
            }
        }

        //По времени заказа
        if ($timeStart = yii::$app->request->get('time_start')) {
            $query->andWhere('date >= :timeStart', [':timeStart' => $timeStart]);
        }

        if ($timeStop = yii::$app->request->get('time_stop')) {
            if (urldecode($timeStop) == '0000-00-00 00:00:00') {
                $timeStop = date('Y-m-d H:i:s');
            }
            $query->andWhere('date <= :timeStop', [':timeStop' => $timeStop]);
        }
    }
}
