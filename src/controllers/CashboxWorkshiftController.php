<?php

namespace dvizh\order\controllers;

use dvizh\order\models\Cashbox;
use Yii;
use dvizh\order\models\CashboxWorkshift;
use dvizh\order\models\tools\CashboxWorkshiftSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CashboxWorkshiftController implements the CRUD actions for CashboxWorkshift model.
 */
class CashboxWorkshiftController extends Controller
{
    /**
     * @return array[]
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CashboxWorkshift models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CashboxWorkshiftSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CashboxWorkshift model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionClose($id)
    {
        $model = $this->findModel($id);
        $post = Yii::$app->request->post();

        if ($post) {
            $close_amount = $post['CashboxWorkshift']['close_amount'];
            $model->setCloseWorkShift($close_amount, Yii::$app->user->identity->id);

            yii::$app->session->setFlash('success', 'Зміна #' . $model->id . ' закрита успішно');
            return $this->redirect(['cashbox/view', 'id' => $model->getCashbox()->id]);
        } else {
            return $this->render('close', [
                'model' => $model,
                'canSwitchCashBox' => false,
                'showCloseAmount' => true,
            ]);
        }
    }

    /**
     * Creates a new CashboxWorkshift model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate($cashbox)
    {
        $model = new CashboxWorkshift();

        $canOpen = $model->canOpenWorkshift(Yii::$app->user->identity->id);
        if (!$canOpen) {
            yii::$app->session->setFlash('warning', 'У вас вже відкрита зміна');
            return $this->redirect([
                'cashbox/view',
                'id' => $cashbox
            ]);
        }

        $model->cashbox_id = $cashbox;

        $openWorkShift = $model->hasOpenWorkShift();

        if ($openWorkShift) {
            return $this->redirect([
                'cashbox-workshift/close',
                'id' => $openWorkShift->id,
            ]);
        }

        $post = Yii::$app->request->post();
        if ($post) {
            $workshift = $post['CashboxWorkshift'];

//            $model->cashbox_id = (integer)$workshift['cashbox_id'];

            $model->open_amount = (float)$workshift['open_amount'];

            $model->user_id = Yii::$app->user->identity->id;
            $model->crete_at = $model->update_at = date("Y-m-d H:m:s");
            $model->cash_amount = $model->card_amount = 0;
            $hasOpenWorkShift = $model->hasOpenWorkShift();
            if (!$hasOpenWorkShift) {
                $model->save();
                return $this->redirect(['cashbox/view', 'id' => $cashbox]);
            } else {
                yii::$app->session->setFlash('warning', 'User #' . $hasOpenWorkShift->user_id . ' dont close his Work Shift');
            }
        }

        $model->open_amount = 0;

        return $this->render('create', [
            'model' => $model,
            'showCloseAmount' => false,
        ]);
    }

    /**
     * Updates an existing CashboxWorkshift model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if (
            $model->load(Yii::$app->request->post()) &&
            $model->save()
        ) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'showCloseAmount' => false,
            ]);
        }
    }

    /**
     * Deletes an existing CashboxWorkshift model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CashboxWorkshift model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CashboxWorkshift the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CashboxWorkshift::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
