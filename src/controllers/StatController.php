<?php

namespace dvizh\order\controllers;

use dvizh\order\interfaces\OrderElement;
use dvizh\order\models\Element;
use dvizh\order\models\StatisticABC;
use dvizh\order\models\StatisticUserProduct;
use yii;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use dvizh\order\models\Order;
use yii\data\ActiveDataProvider;


/**
 * Class StatController
 * @package dvizh\order\controllers
 */
class StatController extends Controller
{
    /**
     * @return array[]
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => $this->module->adminRoles,
                    ]
                ]
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }


    /**
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Order;

        return $this->render('index', [
            'model' => $model,
        ]);
    }

    /**
     * @param null|string|integer $y
     * @param null|string|integer $m
     * @return mixed
     */
    public function actionMonth($y = null, $m = null)
    {
        $m = Html::encode($m);
        $y = Html::encode($y);

        $model = new Order;

        return $this->render('month', [
            'm' => $m,
            'y' => $y,
            'month' => yii::t('order', "month_$m"),
            'model' => $model,
        ]);
    }

    /**
     * @return mixed
     */
    public function actionAbc()
    {
        $model = new StatisticABC();

        $model->load(Yii::$app->request->post());

        $filtered = array_filter($model->categories);
        $model->categories = empty($filtered) ? null : $filtered;

        $dataProvider = new ActiveDataProvider([
            'query' => $model->getAbcProductsQuery(),
            'sort' => [
                'defaultOrder' => 'price DESC'
            ],
            'pagination' => false
//            'pagination' => [
//                'pageSize' => 5,
//            ],
        ]);


        return $this->render('abc', [
            'model' => $model,
            'dataProvider' => $dataProvider
        ]);
    }

    public function actionUserProduct(){
        $model = new StatisticUserProduct();

        $model->load(Yii::$app->request->post());

        $filtered = array_filter($model->products);
        $model->products = empty($filtered) ? null : $filtered;

        $dataProvider = new ActiveDataProvider([
            'query' => $model->getUserProductQuery(),
//            'sort' => [
//                'defaultOrder' => 'price DESC'
//            ],
//            'pagination' => false
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);
        return $this->render('user-product', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

}
